@Regression
Feature: Verify ATM Branch location in the Angular Post-login pages 


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@Regression
Scenario Outline: Angular Post login page display with ATM Branch location on black bulter bar : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And User scroll down to the footer of the page
And I click on contact us
And I click on ATM Branch on the black butler bar
Then I see the ATM Branch page

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@ATM
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|ag|
