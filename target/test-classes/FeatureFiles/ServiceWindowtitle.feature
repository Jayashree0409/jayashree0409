@Regression
Feature: Verify UI Design for the Banking Servicing links


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@US_GCB-425 @Z_Auto @Sprint02 @Regression
Scenario Outline: Banking service title of page : GCB-425; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Banking service title of page


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|ag|

@US_GCB-425 @Z_Auto @Sprint02 @Regression
Scenario Outline: Credit card service title of page : GCB-425; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Credit card service
Then I validate the Credit card service title of page


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL UIDES|ag|

@US_GCB-425 @Z_Auto @Sprint02 @Regression
Scenario Outline: Travel service title of page : GCB-425; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Travel service
Then I validate the Travel service title of page


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL UIDES|ag|


@US_GCB-425 @Z_Auto @Sprint02 @Regression
Scenario Outline: Statements and Documents service title of page : GCB-425; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Statements and Documents service
Then I validate the Statements and Documents service title of page


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL UIDES|ag|


@US_GCB-425 @Z_Auto @Sprint02 @Regression
Scenario Outline: More settings service title of page : GCB-425; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the More settings service
Then I validate the More settings service title of page


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL UIDES|ag|

@US_GCB-425 @Z_Auto @Sprint02 @Regression
Scenario Outline: Balance Transfer service title of page : GCB-425; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Payments and transfers
And I click the Balance Transfer service
Then I validate the Balance Transfer service title of page

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL UIDES|ag|
