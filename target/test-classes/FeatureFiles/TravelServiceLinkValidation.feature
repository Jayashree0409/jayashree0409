@Regression
Feature: Verify Link Validation for the Travel Servicing


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Manage Travel Notices link validation for the Travel Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Travel service
Then I validate the Manage Travel Notices link in the Travel services

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL SERVICES|ag|
