@Regression
Feature: Verify product explorer icon on the Angular page
Provided all the correct User ID and Password, user should be able to successfully login into Citibank Online.
In case the info provided is incorrect and/or eligibility criteria are not met, user is blocked from Login into Citibank Online. 


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@Regression
Scenario Outline: Navigate to the Product Explorer Icon for Angular page : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as new "<URL>" 
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Statements and Documents menu
And I see Statements and Documents page
And click on product explorer icon
Then I see product explorer dropdown page

Examples:
|URL|UserID|Password|AccountNumber|CustomerType|PageType|Ind_PayeeName|Ind_PayeeNName|OneTimeRef#|Merch_PayeeName|Merch_PayeeNName|
##@data@src/test/java/dataProvider/TestData.xlsx@PE
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123||CBOL PE|ag||||||

@Regression
Scenario Outline: Navigate to the Credit Card marketing page for Angular page : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as new "<URL>" 
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Statements and Documents menu
And I see Statements and Documents page
And click on product explorer icon
And click on credit card icon
Then I see credit card marketing page

Examples:
|URL|UserID|Password|AccountNumber|CustomerType|PageType|Ind_PayeeName|Ind_PayeeNName|OneTimeRef#|Merch_PayeeName|Merch_PayeeNName|
##@data@src/test/java/dataProvider/TestData.xlsx@PE
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123||CBOL PE|ag||||||

@Regression
Scenario Outline: Navigate to the Lending marketing page for Angular page : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as new "<URL>" 
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Statements and Documents menu
And I see Statements and Documents page
And click on product explorer icon
And click on Lending icon
Then I see Lending marketing page

Examples:
|URL|UserID|Password|AccountNumber|CustomerType|PageType|Ind_PayeeName|Ind_PayeeNName|OneTimeRef#|Merch_PayeeName|Merch_PayeeNName|
##@data@src/test/java/dataProvider/TestData.xlsx@PE
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123||CBOL PE|ag||||||

@Regression
Scenario Outline: Navigate to the Citigold marketing page for Angular page : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as new "<URL>" 
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Statements and Documents menu
And I see Statements and Documents page
And click on product explorer icon
And click on citigold icon
Then I see citigold marketing page

Examples:
|URL|UserID|Password|AccountNumber|CustomerType|PageType|Ind_PayeeName|Ind_PayeeNName|OneTimeRef#|Merch_PayeeName|Merch_PayeeNName|
##@data@src/test/java/dataProvider/TestData.xlsx@PE
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123||CBOL PE|ag||||||

@Regression
Scenario Outline: Navigate to the Banking marketing page for Angular page : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as new "<URL>" 
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Statements and Documents menu
And I see Statements and Documents page
And click on product explorer icon
And click on Banking icon
Then I see Banking marketing page

Examples:
|URL|UserID|Password|AccountNumber|CustomerType|PageType|Ind_PayeeName|Ind_PayeeNName|OneTimeRef#|Merch_PayeeName|Merch_PayeeNName|
##@data@src/test/java/dataProvider/TestData.xlsx@PE
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123||CBOL PE|ag||||||

@Regression
Scenario Outline: Navigate to the Investing marketing page for Angular page : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as new "<URL>" 
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Statements and Documents menu
And I see Statements and Documents page
And click on product explorer icon
And click on Investing icon
Then I see Investing marketing page

Examples:
|URL|UserID|Password|AccountNumber|CustomerType|PageType|Ind_PayeeName|Ind_PayeeNName|OneTimeRef#|Merch_PayeeName|Merch_PayeeNName|
##@data@src/test/java/dataProvider/TestData.xlsx@PE
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123||CBOL PE|ag||||||
