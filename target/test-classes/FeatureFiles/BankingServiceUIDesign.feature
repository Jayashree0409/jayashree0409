@Regression
Feature: Verify UI Design for the Banking Servicing links


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@US_GCB-218 @Z_Auto @Sprint01 @Regression
Scenario Outline: Font size UI Design validation for the Banking Servicing links : GCB-218; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the font size for the banking services link


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|ag|

@US_GCB-218 @Z_Auto @Sprint01 @Regression
Scenario Outline: Font Family UI Design validation for the Banking Servicing links : GCB-218; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the font family for the banking services link

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|ag|

@US_GCB-218 @Z_Auto @Sprint01 @Regression
Scenario Outline: Color UI Design validation for the Banking Servicing links : GCB-218; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the color for the banking services link

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|ag|

@US_GCB-218 @Z_Auto @Sprint01 @Regression
Scenario Outline: Line height UI Design validation for the Banking Servicing links : GCB-218; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Line height for the banking services link

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|ag|

@US_GCB-218 @Z_Auto @Sprint01 @Regression
Scenario Outline: LetterSpace UI Design validation for the Banking Servicing links : GCB-218; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Letterspace for the banking services link

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|ag|
