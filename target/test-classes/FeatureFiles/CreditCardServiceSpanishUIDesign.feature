@Regression
Feature: Verify UI Design for the Spanish Credit Card Servicing links


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------
@US_GCB-217 @Z_Auto @Sprint01 @Regression
Scenario Outline: Font size UI Design validation for the Spanish Credit Card Servicing links : GCB-217; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
And I click on Espanol
Then I see the page with spanish content
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Spanish Credit card service
Then I validate the font size for the Spanish Credit card services link

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL UIDES|ag|

@US_GCB-217 @Z_Auto @Sprint01 @Regression
Scenario Outline: Font Family UI Design validation for the Spanish Credit Card Servicing links : GCB-217; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
And I click on Espanol
Then I see the page with spanish content
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Spanish Credit card service
Then I validate the font family for the Spanish Credit card services link

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL UIDES|ag|

@US_GCB-217 @Z_Auto @Sprint01 @Regression
Scenario Outline: Color UI Design validation for the Spanish Credit Card Servicing links : GCB-217; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
And I click on Espanol
Then I see the page with spanish content
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Spanish Credit card service
Then I validate the color for the Spanish Credit card services link

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL UIDES|ag|

@US_GCB-217 @Z_Auto @Sprint01 @Regression
Scenario Outline: Line height UI Design validation for the Spanish Credit Card Servicing links : GCB-217; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
And I click on Espanol
Then I see the page with spanish content
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Spanish Credit card service
Then I validate the Line height for the Spanish Credit card services link

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL UIDES|ag|

@US_GCB-217 @Z_Auto @Sprint01 @Regression
Scenario Outline: LetterSpace UI Design validation for the Spanish Credit Card Servicing links : GCB-217; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
And I click on Espanol
Then I see the page with spanish content
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Spanish Credit card service
Then I validate the Letterspace for the Spanish Credit card services link

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL UIDES|ag|
