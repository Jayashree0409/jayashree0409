@Regression
Feature: Verify ATM Branch location in the Spanish Marketing Post-login pages 


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@Regression
Scenario Outline: Spanish Marketing Post login page display with ATM Branch location on black bulter bar : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as "<URL>"
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click on Open an account on the black butler bar
And I click on ATM Branch on the black butler bar
Then I see the ATM Branch page

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@ATM
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|ag|
