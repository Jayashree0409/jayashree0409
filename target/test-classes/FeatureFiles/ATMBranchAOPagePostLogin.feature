@Regression
Feature: Verify ATM Branch location in the AO Post-login pages 


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@Regression
Scenario Outline: AO Post login page display with ATM Branch location on black bulter bar : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Banking Service menu
And I see Banking Service page
And Click on UpgradeCitiPriority link
And I See UpgradeCitiPriority Page
And I click on ATM Branch on the black butler bar
Then I see the ATM Branch page

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@ATM
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|lgc|
