@Regression
Feature: Verify ATM Branch location in the Spanish 3rd Party Post-login pages 


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@Regression
Scenario Outline: Spanish 3rd Party Post login page display with ATM Branch location on black bulter bar : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as "<URL>"
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Rewards and Benefits menu
And I see Rewards and Benefits drop down menu
And Click on Card benefits menu
And I see Card benefits page
And click on close button on the popup window
And I click on ATM Branch on the black butler bar
Then I see the ATM Branch page

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@ATM
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL UIDES|lgc|
