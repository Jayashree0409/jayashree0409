@Regression
Feature: Verify New feedback icon display on the page
Provided all the correct User ID and Password, user should be able to successfully login into Citibank Online.
In case the info provided is incorrect and/or eligibility criteria are not met, user is blocked from Login into Citibank Online. 


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@US_GCB-346 @Z_Auto @Sprint02 @Regression
Scenario Outline: Display of New Feedback icon on the dashboard page : GCB-346; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And click on feedback icon
Then I see medallia page

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@MEDA
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL MEDA|lgc|

@US_GCB-346 @Z_Auto @Sprint02 @Regression
Scenario Outline: Display of New Feedback icon on the Banking service page : GCB-346; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
And click on feedback icon
Then I see medallia page

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@MEDA
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL MEDA|lgc|

@US_GCB-346 @Z_Auto @Sprint02 @Regression
Scenario Outline: Display of New Feedback icon on the Credit Card service page : GCB-346; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Credit card service
And click on feedback icon
Then I see medallia page

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@MEDA
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL MEDA|lgc|

@US_GCB-346 @Z_Auto @Sprint02 @Regression
Scenario Outline: Display of New Feedback icon on the Travel Service page : GCB-346; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Travel service
And click on feedback icon
Then I see medallia page

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@MEDA
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL MEDA|lgc|

@US_GCB-346 @Z_Auto @Sprint02 @Regression
Scenario Outline: Display of New Feedback icon on the Statements and Documents page : GCB-346; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Statements and Documents service
And click on feedback icon
Then I see medallia page

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@MEDA
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL MEDA|lgc|

@US_GCB-346 @Z_Auto @Sprint02 @Regression
Scenario Outline: Display of New Feedback icon on the More settings page : GCB-346; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Profiles
And I click the More settings service
And click on feedback icon
Then I see medallia page

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@MEDA
|https://uat3.online.citi.com/US/login.do|user_5061|test123|CBOL MEDA|lgc|
