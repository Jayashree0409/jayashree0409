@Regression
Feature: Verify Link Validation for the Banking Servicing


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Reset ATM link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Reset ATM link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Lock card link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Lock card link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Replace ATM Card link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Replace ATM Card link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Change ATM Card to Debit Card link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Change ATM Card to Debit Card link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Stop Check Payment link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Stop Check Payment link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Reorder Personal Checks link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Reorder Personal Checks link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Text Banking link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Text Banking link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|


@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Telephone Access Code link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Telephone Access Code link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Link Unlink Citi Accounts link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Link Unlink Citi Accounts link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Link Unlink Savings for Overdraft Protection link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Link Unlink Savings for Overdraft Protection link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Request Upgrade to CitiPriority link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Request Upgrade to CitiPriority link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Request Upgrade to Citigold link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Request Upgrade to Citigold link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Link Citi Accounts from Other Countries link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Link Citi Accounts from Other Countries link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Financial Tools link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Financial Tools link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Investments link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Investments link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Schedule an Appointment link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Schedule an Appointment link in the banking services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Manage Appointments link validation for the Banking Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Banking service
Then I validate the Manage Appointments link in the banking services

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|
