@Regression
Feature: Verify New IA Branding Menu display for spanish page


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------


@US_GCB-310 @Z_Auto @Sprint01 @Regression
Scenario Outline: Accounts submenu display for spanish : GCB-310; WebContainer; Regression
Given I am on the Home Page with Iteration as "<URL>"
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Accounts
Then I see the submenu for Accounts menu

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@BRAND
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|lgc|

@US_GCB-310 @Z_Auto @Sprint01 @Regression
Scenario Outline: Payments and Transfer submenu display for spanish : GCB-310; WebContainer; Regression
Given I am on the Home Page with Iteration as "<URL>"
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Payments and Transfer
Then I see the submenu for Payments and Transfer menu

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@BRAND
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|lgc|

@US_GCB-310 @Z_Auto @Sprint01 @Regression
Scenario Outline: Services submenu display for spanish : GCB-310; WebContainer; Regression
Given I am on the Home Page with Iteration as "<URL>"
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
Then I see the submenu for services menu


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@BRAND
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|lgc|

@US_GCB-310 @Z_Auto @Sprint01 @Regression
Scenario Outline: Rewards and Benefits submenu display for spanish : GCB-310; WebContainer; Regression
Given I am on the Home Page with Iteration as "<URL>"
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Rewards and Benefits menu
Then I see the submenu for Rewards and Benefits menu

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@BRAND
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|lgc|

@US_GCB-310 @Z_Auto @Sprint01 @Regression
Scenario Outline: Profile submenu display for spanish : GCB-310; WebContainer; Regression
Given I am on the Home Page with Iteration as "<URL>"
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on profile menu
Then I see the submenu for profiles menu

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@BRAND
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|lgc|

