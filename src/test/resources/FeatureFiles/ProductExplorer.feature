@Sanity
Feature: Verify product explorer icon on the legacy page
Provided all the correct User ID and Password, user should be able to successfully login into Citibank Online.
In case the info provided is incorrect and/or eligibility criteria are not met, user is blocked from Login into Citibank Online. 


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@US_GCB-310 @Z_Auto @Sprint01 @Regression
Scenario Outline: Navigate to the Product Explorer Icon : GCB-310; WebContainer; Sanity
Given I am on the Home Page with Iteration as new "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And click on product explorer icon
Then I see product explorer dropdown page

Examples:
|URL|UserID|Password|AccountNumber|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@PE
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123||CBOL PE|lgc|

