@Regression
Feature: Verify product explorer icon on the AO page for Spanish Content
Provided all the correct User ID and Password, user should be able to successfully login into Citibank Online.
In case the info provided is incorrect and/or eligibility criteria are not met, user is blocked from Login into Citibank Online. 


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@Regression
Scenario Outline: Toggle to Spanish and navigate to the Product Explorer Icon in the AO Page : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as new "<URL>"
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Banking Service menu
And I see Banking Service page
And Click on UpgradeCitiPriority link
And I See UpgradeCitiPriority Page
And click on product explorer icon
Then I see product explorer dropdown page

Examples:
|URL|UserID|Password|AccountNumber|CustomerType|PageType|Ind_PayeeName|Ind_PayeeNName|OneTimeRef#|Merch_PayeeName|Merch_PayeeNName|
##@data@src/test/java/dataProvider/TestData.xlsx@PE
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123||CBOL PE|ag||||||

@Regression
Scenario Outline: Toggle to Spanish and navigate to the Credit Card marketing page via Product Explorer Icon in AO Page : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as new "<URL>" 
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Banking Service menu
And I see Banking Service page
And Click on UpgradeCitiPriority link
And I See UpgradeCitiPriority Page
And click on product explorer icon
And click on credit card icon
Then I see credit card marketing page

Examples:
|URL|UserID|Password|AccountNumber|CustomerType|PageType|Ind_PayeeName|Ind_PayeeNName|OneTimeRef#|Merch_PayeeName|Merch_PayeeNName|
##@data@src/test/java/dataProvider/TestData.xlsx@PE
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123||CBOL PE|ag||||||

@Regression
Scenario Outline: Toggle to Spanish and navigate to the Lending marketing page via Product Explorer Icon in AO Page : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as new "<URL>"
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Banking Service menu
And I see Banking Service page
And Click on UpgradeCitiPriority link
And I See UpgradeCitiPriority Page
And click on product explorer icon
And click on Lending icon
Then I see Lending marketing page

Examples:
|URL|UserID|Password|AccountNumber|CustomerType|PageType|Ind_PayeeName|Ind_PayeeNName|OneTimeRef#|Merch_PayeeName|Merch_PayeeNName|
##@data@src/test/java/dataProvider/TestData.xlsx@PE
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123||CBOL PE|ag||||||

@Regression
Scenario Outline: Toggle to Spanish and navigate to the Citigold marketing page via Product Explorer Icon in AO Page : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as new "<URL>" 
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Banking Service menu
And I see Banking Service page
And Click on UpgradeCitiPriority link
And I See UpgradeCitiPriority Page
And click on product explorer icon
And click on citigold icon
Then I see citigold marketing page

Examples:
|URL|UserID|Password|AccountNumber|CustomerType|PageType|Ind_PayeeName|Ind_PayeeNName|OneTimeRef#|Merch_PayeeName|Merch_PayeeNName|
##@data@src/test/java/dataProvider/TestData.xlsx@PE
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123||CBOL PE|ag||||||

@Regression
Scenario Outline: Toggle to Spanish and navigate to the Banking marketing page via Product Explorer Icon in AO Page : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as new "<URL>" 
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Banking Service menu
And I see Banking Service page
And Click on UpgradeCitiPriority link
And I See UpgradeCitiPriority Page
And click on product explorer icon
And click on Banking icon
Then I see Banking marketing page

Examples:
|URL|UserID|Password|AccountNumber|CustomerType|PageType|Ind_PayeeName|Ind_PayeeNName|OneTimeRef#|Merch_PayeeName|Merch_PayeeNName|
##@data@src/test/java/dataProvider/TestData.xlsx@PE
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123||CBOL PE|ag||||||

@Regression
Scenario Outline: Toggle to Spanish and navigate to the Investing marketing page via Product Explorer Icon in AO Page : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as new "<URL>" 
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Banking Service menu
And I see Banking Service page
And Click on UpgradeCitiPriority link
And I See UpgradeCitiPriority Page
And click on product explorer icon
And click on Investing icon
Then I see Investing marketing page

Examples:
|URL|UserID|Password|AccountNumber|CustomerType|PageType|Ind_PayeeName|Ind_PayeeNName|OneTimeRef#|Merch_PayeeName|Merch_PayeeNName|
##@data@src/test/java/dataProvider/TestData.xlsx@PE
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123||CBOL PE|ag||||||
