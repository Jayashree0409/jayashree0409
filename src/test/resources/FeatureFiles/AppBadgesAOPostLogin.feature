@Regression
Feature: Verify Appbadges in the AOPage Post-login pages 


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@Regression
Scenario Outline: AOPage Post login page display with the New icon to download the app badges for iOS : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Banking Service menu
And I see Banking Service page
And Click on UpgradeCitiPriority link
And I See UpgradeCitiPriority Page
And User scroll down to the footer of the page
And I click on post login Apple App badges
And I see the post login speedbump display before navigate to app store 3rd party page
And I click on post login continue button to page open in new tab
Then I see the 3rd party page in new tab

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@APPBADGES
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL APPBADGES|lgc|

@Regression
Scenario Outline: AOPage Post login page display with the New icon to download the app badges for Android : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And Click on Service menu
And I see Services drop down menu
And Click on Banking Service menu
And I see Banking Service page
And Click on UpgradeCitiPriority link
And I See UpgradeCitiPriority Page
And User scroll down to the footer of the page
And I click on post login Google App badges
And I see the post login speedbump display before navigate to app store 3rd party page
And I click on post login continue button to page open in new tab
Then I see the 3rd party page in new tab

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@APPBADGES
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL APPBADGES|lgc|
