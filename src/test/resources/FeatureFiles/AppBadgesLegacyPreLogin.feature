@Regression
Feature: Verify Appbadges in the Pre-login pages 


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@Regression
Scenario Outline: Legacy Home page display with the New icon to download the app badges for iOS : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as "<URL>"
When User scroll down to the footer of the page
And I click on Apple App badges
And I see the speedbump display before navigate to app store 3rd party page
And I click on continue button to page open in new tab
Then I see the 3rd party page in new tab

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@APPBADGES
|https://uat3.online.citi.com/US/login.do|||CBOL APPBADGES|lgc|

@Regression
Scenario Outline: Legacy Home page display with the New icon to download the app badges for Android : RCE101; BrowserContainer; Regression
Given I am on the Home Page with Iteration as "<URL>"
When User scroll down to the footer of the page
And I click on Google App badges
And I see the speedbump display before navigate to app store 3rd party page
And I click on continue button to page open in new tab
Then I see the 3rd party page in new tab

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@APPBADGES
|https://uat3.online.citi.com/US/login.do|||CBOL APPBADGES|lgc|
