@Regression
Feature: Verify Link Validation for the Statements and Documents Servicing


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Account Snapshot link validation for the Statements and Documents Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Statements and Documents service
Then I validate the Account Snapshot link in the Statements and Documents services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Account statement link validation for the Statements and Documents Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Statements and Documents service
Then I validate the Account statement link in the Statements and Documents services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Ecommunications link validation for the Statements and Documents Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Statements and Documents service
Then I validate the Ecommunications link in the Statements and Documents services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Secure Message Center link validation for the Statements and Documents Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Statements and Documents service
Then I validate the Secure Message Center link in the Statements and Documents services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Tax Documents link validation for the Statements and Documents Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Statements and Documents service
Then I validate the Tax Documents link in the Statements and Documents services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Manage Paperless Settings link validation for the Statements and Documents Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Statements and Documents service
Then I validate the Manage Paperless Settings link in the Statements and Documents services


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|

@US_GCB-486 @Z_Auto @Sprint03 @Regression
Scenario Outline: Language Preference link validation for the Statements and Documents Servicing links : GCB-486; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Services
And I click the Statements and Documents service
Then I validate the Language Preference link in the Statements and Documents services

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@SERVICES
|https://uat3.online.citi.com/US/login.do|npc6100c|ist123|CBOL SERVICES|ag|
