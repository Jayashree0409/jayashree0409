@Regression
Feature: Verify UI Design for the More Settings Servicing links


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------

@US_GCB-255 @Z_Auto @Sprint01 @Regression
Scenario Outline: Font size UI Design validation for the More Settings Servicing links : GCB-255; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Profiles
And I click the More settings service
Then I validate the font size for the More settings services link

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|ag|

@US_GCB-255 @Z_Auto @Sprint01 @Regression
Scenario Outline: Font Family UI Design validation for the More Settings Servicing links : GCB-255; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Profiles
And I click the More settings service
Then I validate the font family for the More settings services link

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|ag|

@US_GCB-255 @Z_Auto @Sprint01 @Regression
Scenario Outline: Color UI Design validation for the More Settings Servicing links : GCB-255; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Profiles
And I click the More settings service
Then I validate the color for the More settings services link

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|ag|

@US_GCB-255 @Z_Auto @Sprint01 @Regression
Scenario Outline: Line height UI Design validation for the More Settings Servicing links : GCB-255; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Profiles
And I click the More settings service
Then I validate the Line height for the More settings services link

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|ag|

@US_GCB-255 @Z_Auto @Sprint01 @Regression
Scenario Outline: LetterSpace UI Design validation for the More Settings Servicing links : GCB-255; WebContainer; Sprint
Given I am on the Home Page with Iteration as "<URL>"
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Profiles
And I click the More settings service
Then I validate the Letterspace for the More settings services link

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@UIDES
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|ag|
