@Regression
Feature: Verify old account management hub decommission in the Post login pages.


#-------------------------------------------------------------------------------------------
#------------------------------------Positive Flow------------------------------------------
#-------------------------------------------------------------------------------------------



@US_GCB-384 @Z_Auto @Sprint03 
Scenario Outline: Spanish Decommission Old Account Management Hub Upon Click on Cancel : GCB-384; WebContainer; Sprint03
Given I am on the Home Page with Iteration as "<URL>"
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Profiles
And I click the User ID link
And I click Cancel link
Then I see CBOL dashboard Page


Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@BRAND
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|lgc|


@US_GCB-384 @Z_Auto @Sprint03 @Regression
Scenario Outline: Spanish Decommission Old Account Management Hub Upon Click on Continue : GCB-384; WebContainer; Sprint03
Given I am on the Home Page with Iteration as "<URL>"
Then click the Language to spanish
When I enter a valid User ID and Password "<UserID>" and "<Password>"
And If I click on Sign on button
And I see CBOL dashboard Page
And I click the Profiles
And I click the User ID link
And I enter new User ID 
And I enter Current password
And I click Change button
And I click the Profiles
And I click the User ID link
And I enter revert User ID
And I enter Current password
And I click Change button
And I click Continue button
Then I see CBOL dashboard Page

Examples:
|URL|UserID|Password|CustomerType|PageType|
##@data@src/test/java/dataProvider/TestData.xlsx@BRAND
|https://uat3.online.citi.com/US/login.do|bank_9059|ist123|CBOL UIDES|lgc|


