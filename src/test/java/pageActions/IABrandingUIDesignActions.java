package pageActions;

import static org.testng.Assert.assertTrue;

import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.Color;
import pageLocators.IABrandingUIDesignLocator;
import utils.SeleniumDriver;
import utils.redlinesConfigFileReader;
import pageLocators.IABrandingLocators;
//import utils.ExcelRedlinesutils;


public class IABrandingUIDesignActions {
		
		CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
		IABrandingUIDesignLocator iaBrandinguidesignLocators=null;
		redlinesConfigFileReader redlinesconfigfilereader=null;
		IABrandingLocators iaBrandinglocators=null;
		final Log log = LogFactory.getLog(IABrandingUIDesignActions.class.getName());
		
		//public XSSFRow row;
		
		//Constructor
		public  IABrandingUIDesignActions()
		{
			this.iaBrandinguidesignLocators = new IABrandingUIDesignLocator();
			this.redlinesconfigfilereader = new redlinesConfigFileReader();
			this.iaBrandinglocators = new IABrandingLocators();
			PageFactory.initElements(SeleniumDriver.getDriver(), iaBrandinguidesignLocators);
			PageFactory.initElements(SeleniumDriver.getDriver(), redlinesconfigfilereader);
			PageFactory.initElements(SeleniumDriver.getDriver(), iaBrandinglocators);
		}
		
		public void CreditCardwindowtitle()
		{
			String windowtitle = SeleniumDriver.driver.getTitle();
			System.out.println("Window title of current page:" +windowtitle);
			String Expected_windowtitle = "Credit Card Services - Citibank";
				Assert.assertEquals(Expected_windowtitle,windowtitle);
					
		}
		
		public void Bankingwindowtitle()
		{
			String windowtitle = SeleniumDriver.driver.getTitle();
			System.out.println("Window title of current page:" +windowtitle);
			String Expected_windowtitle = "Banking Services - Citibank";
				Assert.assertEquals(Expected_windowtitle,windowtitle);
					
		}
		
		public void Travelwindowtitle()
		{
			String windowtitle = SeleniumDriver.driver.getTitle();
			System.out.println("Window title of current page:" +windowtitle);
			String Expected_windowtitle = "Travel Services - Citibank";
				Assert.assertEquals(Expected_windowtitle,windowtitle);
					
		}
		
		public void Stmtdocswindowtitle()
		{
			String windowtitle = SeleniumDriver.driver.getTitle();
			System.out.println("Window title of current page:" +windowtitle);
			String Expected_windowtitle = "Statements & Communication Preferences - Citibank";
				Assert.assertEquals(Expected_windowtitle,windowtitle);					
		}
		
		public void Moresettingswindowtitle()
		{
			String windowtitle = SeleniumDriver.driver.getTitle();
			System.out.println("Window title of current page:" +windowtitle);
			String Expected_windowtitle = "Settings - Citibank";
				Assert.assertEquals(Expected_windowtitle,windowtitle);					
		}
		
		public void Balancetransferwindowtitle()
		{
			String windowtitle = SeleniumDriver.driver.getTitle();
			System.out.println("Window title of current page:" +windowtitle);
			String Expected_windowtitle = "Balance Transfers - Citibank";
				Assert.assertEquals(Expected_windowtitle,windowtitle);					
		}
		
		public void services()
		{
			
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("/ag")) 
			{
			cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.AGServices);
			}
			else
			{
				cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.Services);
			}
			
		}
		
		public void profiles()
		{
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("/ag")) 
			{
			cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.AGprofiles);
			System.out.println("Angular profile icon clicked");
			}
			else
			{
				cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.profiles);
				System.out.println("Legacy profile icon clicked");
			}
			
		}
		
			public void user_id()
			{
			
				if (useridislinkpresent())
				{
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					cbolCommonActions.CBOLPerformClick(iaBrandinglocators.SpanishUserid);
					System.out.println("Spanish user id Element is present");
				}
		
			else
				{
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
					cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Userid);
					System.out.println("English user id Element is present");
				}
			}
		
		
		public void useridinputfield()
		{
			
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
				
				cbolCommonActions.CBOLPerformClick(iaBrandinglocators.NewUsername);
				cbolCommonActions.CBOLPerformSendKeys(iaBrandinglocators.NewUsernamemasked, "bank_3585_");
				System.out.println("User id entered");
	
		}
		
		public void revertuseridinputfield()
		{
			
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
				
				cbolCommonActions.CBOLPerformClick(iaBrandinglocators.NewUsername);
				cbolCommonActions.CBOLPerformSendKeys(iaBrandinglocators.NewUsernamemasked, "bank_3585");
				System.out.println("User id entered");
	
		}
		
		public void passwordinputfield()
		{
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
				
				cbolCommonActions.CBOLPerformClick(iaBrandinglocators.CurrPassword);
				cbolCommonActions.CBOLPerformSendKeys(iaBrandinglocators.CurrPassword, "ist123");
				System.out.println("password entered");
			
		}
		
		public void decommission_acct_mgmt_hub_Change()
		{
				
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.ChangeButton);
		}
		
		public void decommission_acct_mgmt_hub_Continue()
		{
			
			String currenturl="";
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Continuebutton);
			
			String Old_hub_URL = "https://sit33.online.citi.com/US/JRS/portal/menu.do?ID=Support";
			String CurrentURL = SeleniumDriver.getCurrentURL();

			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(CurrentURL,currenturl);
				System.out.println("Links match as per the requirement for enter");
				
				Assert.assertNotEquals("Actual is not Matching with expected", currenturl, Old_hub_URL);
				System.out.println("Current URL of the page:" +currenturl);
				System.out.println("Old Hub URL:" +Old_hub_URL);
				
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(CurrentURL,currenturl);
				System.out.println("Links match as per the requirement for enter1");
				
				Assert.assertNotEquals("Actual is not Matching with expected", currenturl, Old_hub_URL);
				System.out.println("Current URL of the page:" +currenturl);
				System.out.println("Old Hub URL:" +Old_hub_URL);
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(CurrentURL,CurrentURL);
				System.out.println("Links match as per the requirement for end");
				
				Assert.assertNotEquals("Actual is not Matching with expected", CurrentURL, Old_hub_URL);
				System.out.println("Current URL of the page:" +CurrentURL);
				System.out.println("Old Hub URL:" +Old_hub_URL);
			}

			
		}
		
		public void decommission_acct_mgmt_hub_Cancel()
		{
			if (Cancelislinkpresent())
			{
				String currenturl="";
						
				cbolCommonActions.CBOLPerformClick(iaBrandinglocators.SpanishCancelbutton);
				System.out.println("Spanish cancel button Element is present");
				
			
				String Old_hub_URL = "https://sit33.online.citi.com/US/JRS/portal/menu.do?ID=Support";
				String CurrentURL = SeleniumDriver.getCurrentURL();
			
				if(CurrentURL.contains("&JFP_TOKEN"))
					{
						System.out.println("enter");
						currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
						Assert.assertEquals(CurrentURL,currenturl);
						System.out.println("Links match as per the requirement for enter");
				
						Assert.assertNotEquals("Actual is not Matching with expected", currenturl, Old_hub_URL);
						System.out.println("Current URL of the page:" +currenturl);
						System.out.println("Old Hub URL:" +Old_hub_URL);
				
					}
				else if(CurrentURL.contains("?JFP_TOKEN"))
					{
						System.out.println("enter1");
						currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
						Assert.assertEquals(CurrentURL,currenturl);
						System.out.println("Links match as per the requirement for enter1");
				
						Assert.assertNotEquals("Actual is not Matching with expected", currenturl, Old_hub_URL);
						System.out.println("Current URL of the page:" +currenturl);
						System.out.println("Old Hub URL:" +Old_hub_URL);
					}
				else
					{
						System.out.println("end");
						Assert.assertEquals(CurrentURL,CurrentURL);
						System.out.println("Links match as per the requirement for end");
				
						Assert.assertNotEquals("Actual is not Matching with expected", CurrentURL, Old_hub_URL);
						System.out.println("Current URL of the page:" +CurrentURL);
						System.out.println("Old Hub URL:" +Old_hub_URL);
					}
			}
			
			else
				{
				String currenturl="";
				
				cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Cancelbutton);
				System.out.println("English Cancel button Element is present");
			
				String Old_hub_URL = "https://sit33.online.citi.com/US/JRS/portal/menu.do?ID=Support";
				String CurrentURL = SeleniumDriver.getCurrentURL();
			
				if(CurrentURL.contains("&JFP_TOKEN"))
					{
						System.out.println("enter");
						currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
						Assert.assertEquals(CurrentURL,currenturl);
						System.out.println("Links match as per the requirement for enter");
				
						Assert.assertNotEquals("Actual is not Matching with expected", currenturl, Old_hub_URL);
						System.out.println("Current URL of the page:" +currenturl);
						System.out.println("Old Hub URL:" +Old_hub_URL);
				
					}
				else if(CurrentURL.contains("?JFP_TOKEN"))
					{
						System.out.println("enter1");
						currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
						Assert.assertEquals(CurrentURL,currenturl);
						System.out.println("Links match as per the requirement for enter1");
				
						Assert.assertNotEquals("Actual is not Matching with expected", currenturl, Old_hub_URL);
						System.out.println("Current URL of the page:" +currenturl);
						System.out.println("Old Hub URL:" +Old_hub_URL);
					}
				else
					{
						System.out.println("end");
						Assert.assertEquals(CurrentURL,CurrentURL);
						System.out.println("Links match as per the requirement for end");
				
						Assert.assertNotEquals("Actual is not Matching with expected", CurrentURL, Old_hub_URL);
						System.out.println("Current URL of the page:" +CurrentURL);
						System.out.println("Old Hub URL:" +Old_hub_URL);
					}
			}
		}
			
		public void BankingServices()
		{
		
			try 
			{
				Thread.sleep(5000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}	
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.BankingServices);
		}
		
		
		
		public void CreditCardServices()
		{
			try 
			{
				Thread.sleep(5000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Creditcardservices);
			
		}
		
	
		
		public void StatementsandDocuments()
		{
			try
			{
				Thread.sleep(5000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.StatementsandDocuments);
		}
		
	
		
		public void TravelServices()
		{
			try
			{
				Thread.sleep(5000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Travelservice);
		}
		
	
		
		public void Moresettings()
		{
			try
			{
				Thread.sleep(5000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Moresettings);
		}
		
		public void balance_transfer()
		{
			try
			{
				Thread.sleep(5000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Balance_transfer);
		}
		
		
	
		
	/* Banking Service Links UI Validation */
		
		public void LinkFontSizeValidation()
		{
			String LFontvalue = redlinesconfigfilereader.getLFontvalue();
			String lfontSize[] = new String[17];
			
			 lfontSize[0] = iaBrandinglocators.LockCard.getCssValue("font-size");
			 lfontSize[1] = iaBrandinglocators.ReplaceATMcard.getCssValue("font-size");
			 lfontSize[2] = iaBrandinglocators.ChangeATMCard.getCssValue("font-size");
			 lfontSize[3] = iaBrandinglocators.StopCheck.getCssValue("font-size");
			 lfontSize[4] = iaBrandinglocators.Reordercheck.getCssValue("font-size");
			 lfontSize[5] = iaBrandinglocators.Textbank.getCssValue("font-size");
			 lfontSize[6] = iaBrandinglocators.TeleAccesscode.getCssValue("font-size");
			 lfontSize[7] = iaBrandinglocators.ResetATMPIN.getCssValue("font-size");
			 lfontSize[8] = iaBrandinglocators.Linkorunlinkacct.getCssValue("font-size");
			 lfontSize[9] = iaBrandinglocators.LinkorUnlinksaving.getCssValue("font-size");
			 lfontSize[10] = iaBrandinglocators.UpgradeCitiPriority.getCssValue("font-size");
			 lfontSize[11] = iaBrandinglocators.UpgradeCitigold.getCssValue("font-size");
			 lfontSize[12] = iaBrandinglocators.LinkCitiAccount.getCssValue("font-size");
			 lfontSize[13] = iaBrandinglocators.FinanceTool.getCssValue("font-size");
			 lfontSize[14] = iaBrandinglocators.Invest.getCssValue("font-size");
			 lfontSize[15] = iaBrandinglocators.ManageAppt.getCssValue("font-size");
			 lfontSize[16] = iaBrandinglocators.Scheduleappt.getCssValue("font-size");
		
			for (int i=0; i < lfontSize.length-1; i++)
			{
			 Assert.assertEquals(LFontvalue,lfontSize[i]);
			 System.out.println("Font size is matching in the UI Design:" +lfontSize);
			}
		}
		
		public void LFontFamilyValidation()
		{
			String LFontFamily = redlinesconfigfilereader.getLFontfamily();
			String lfontFamily[] = new String[17];
				lfontFamily[0] = iaBrandinglocators.LockCard.getCssValue("font-family");
				lfontFamily[1] = iaBrandinglocators.ReplaceATMcard.getCssValue("font-family");
				lfontFamily[2] = iaBrandinglocators.ChangeATMCard.getCssValue("font-family");
				lfontFamily[3] = iaBrandinglocators.StopCheck.getCssValue("font-family");
				lfontFamily[4] = iaBrandinglocators.Reordercheck.getCssValue("font-family");
				lfontFamily[5] = iaBrandinglocators.Textbank.getCssValue("font-family");
				lfontFamily[6] = iaBrandinglocators.TeleAccesscode.getCssValue("font-family");
				lfontFamily[7] = iaBrandinglocators.ResetATMPIN.getCssValue("font-family");
				lfontFamily[8] = iaBrandinglocators.Linkorunlinkacct.getCssValue("font-family");
				lfontFamily[9] = iaBrandinglocators.LinkorUnlinksaving.getCssValue("font-family");
				lfontFamily[10] = iaBrandinglocators.UpgradeCitiPriority.getCssValue("font-family");
				lfontFamily[11] = iaBrandinglocators.UpgradeCitigold.getCssValue("font-family");
				lfontFamily[12] = iaBrandinglocators.LinkCitiAccount.getCssValue("font-family");
				lfontFamily[13] = iaBrandinglocators.FinanceTool.getCssValue("font-family");
				lfontFamily[14] = iaBrandinglocators.Invest.getCssValue("font-family");
				lfontFamily[15] = iaBrandinglocators.ManageAppt.getCssValue("font-family");
				lfontFamily[16] = iaBrandinglocators.Scheduleappt.getCssValue("font-family");
				
			for (int i=0; i < lfontFamily.length-1; i++)
			{
			 //Trim the Sans-serif from the font family
				String ff=lfontFamily[i];
				String lfontfamily = ff.substring(0, ff.indexOf(","));
			//Compare the trim with the redlines
			 Assert.assertEquals(LFontFamily, lfontfamily);
			 System.out.println("Font Family is matching in the UI Design:" +lfontfamily);
			}
		}
		
		public void LColorvalidation()
		{
			String LColor = redlinesconfigfilereader.getLcolor();
			String lcolor[] = new String[17];
				lcolor[0] = iaBrandinglocators.LockCard.getCssValue("color");
				lcolor[1] = iaBrandinglocators.ReplaceATMcard.getCssValue("color");
				lcolor[2] = iaBrandinglocators.ChangeATMCard.getCssValue("color");
				lcolor[3] = iaBrandinglocators.StopCheck.getCssValue("color");
				lcolor[4] = iaBrandinglocators.Reordercheck.getCssValue("color");
				lcolor[5] = iaBrandinglocators.Textbank.getCssValue("color");
				lcolor[6] = iaBrandinglocators.TeleAccesscode.getCssValue("color");
				lcolor[7] = iaBrandinglocators.ResetATMPIN.getCssValue("color");
				lcolor[8] = iaBrandinglocators.Linkorunlinkacct.getCssValue("color");
				lcolor[9] = iaBrandinglocators.LinkorUnlinksaving.getCssValue("color");
				lcolor[10] = iaBrandinglocators.UpgradeCitiPriority.getCssValue("color");
				lcolor[11] = iaBrandinglocators.UpgradeCitigold.getCssValue("color");
				lcolor[12] = iaBrandinglocators.LinkCitiAccount.getCssValue("color");
				lcolor[13] = iaBrandinglocators.FinanceTool.getCssValue("color");
				lcolor[14] = iaBrandinglocators.Invest.getCssValue("color");
				lcolor[15] = iaBrandinglocators.ManageAppt.getCssValue("color");
				lcolor[16] = iaBrandinglocators.Scheduleappt.getCssValue("color");
				
			for (int i=0; i < lcolor.length-1; i++)
			{
			//Converting the RGB in Hex value
				String hex = Color.fromString(lcolor[i]).asHex();
			//Comparing the converted color into hex with the given value by design team
				Assert.assertEquals(LColor, hex);
				System.out.println("Color is matching in the UI Design:" +hex);
			}	
		}
		
		public void LLineheightvalidation()
		{
			    String LLineheight = redlinesconfigfilereader.getLLineheight();
			   	String llineHeight[] = new String[17];
				 llineHeight[0] = iaBrandinglocators.LockCard.getCssValue("line-height");
				 llineHeight[1] = iaBrandinglocators.ReplaceATMcard.getCssValue("line-height");
				 llineHeight[2] = iaBrandinglocators.ChangeATMCard.getCssValue("line-height");
				 llineHeight[3] = iaBrandinglocators.StopCheck.getCssValue("line-height");
				 llineHeight[4] = iaBrandinglocators.Reordercheck.getCssValue("line-height");
				 llineHeight[5] = iaBrandinglocators.Textbank.getCssValue("line-height");
				 llineHeight[6] = iaBrandinglocators.TeleAccesscode.getCssValue("line-height");
				 llineHeight[7] = iaBrandinglocators.ResetATMPIN.getCssValue("line-height");
				 llineHeight[8] = iaBrandinglocators.Linkorunlinkacct.getCssValue("line-height");
				 llineHeight[9] = iaBrandinglocators.LinkorUnlinksaving.getCssValue("line-height");
				 llineHeight[10] = iaBrandinglocators.UpgradeCitiPriority.getCssValue("line-height");
				 llineHeight[11] = iaBrandinglocators.UpgradeCitigold.getCssValue("line-height");
				 llineHeight[12] = iaBrandinglocators.LinkCitiAccount.getCssValue("line-height");
				 llineHeight[13] = iaBrandinglocators.FinanceTool.getCssValue("line-height");
				 llineHeight[14] = iaBrandinglocators.Invest.getCssValue("line-height");
				 llineHeight[15] = iaBrandinglocators.ManageAppt.getCssValue("line-height");
				 llineHeight[16] = iaBrandinglocators.Scheduleappt.getCssValue("line-height");

				for (int i=0; i < llineHeight.length-1; i++)
				{
				 Assert.assertEquals(LLineheight, llineHeight[i]);
				 System.out.println("Line height is matching in the UI Design:" +llineHeight);
				}
		}
		
		  public void Lletterspacingvalidation()
		  {
			  String Lletterspace = redlinesconfigfilereader.getLletterspace();
			  String lletterspace[] = new String[17];
			  lletterspace[0] = iaBrandinglocators.LockCard.getCssValue("letter-spacing");
			  lletterspace[1] = iaBrandinglocators.ReplaceATMcard.getCssValue("letter-spacing");
			  lletterspace[2] = iaBrandinglocators.ChangeATMCard.getCssValue("letter-spacing");
			  lletterspace[3] = iaBrandinglocators.StopCheck.getCssValue("letter-spacing");
			  lletterspace[4] = iaBrandinglocators.Reordercheck.getCssValue("letter-spacing");
			  lletterspace[5] = iaBrandinglocators.Textbank.getCssValue("letter-spacing");
			  lletterspace[6] = iaBrandinglocators.TeleAccesscode.getCssValue("letter-spacing");
			  lletterspace[7] = iaBrandinglocators.ResetATMPIN.getCssValue("letter-spacing");
			  lletterspace[8] = iaBrandinglocators.Linkorunlinkacct.getCssValue("letter-spacing");
			  lletterspace[9] = iaBrandinglocators.LinkorUnlinksaving.getCssValue("letter-spacing");
			  lletterspace[10] = iaBrandinglocators.UpgradeCitiPriority.getCssValue("letter-spacing");
			  lletterspace[11] = iaBrandinglocators.UpgradeCitigold.getCssValue("letter-spacing");
			  lletterspace[12] = iaBrandinglocators.LinkCitiAccount.getCssValue("letter-spacing");
			  lletterspace[13] = iaBrandinglocators.FinanceTool.getCssValue("letter-spacing");
			  lletterspace[14] = iaBrandinglocators.Invest.getCssValue("letter-spacing");
			  lletterspace[15] = iaBrandinglocators.ManageAppt.getCssValue("letter-spacing");
			  lletterspace[16] = iaBrandinglocators.Scheduleappt.getCssValue("letter-spacing");
			  
			  for (int i=0; i < lletterspace.length-1; i++)
			  {
				  //convert the 0px into normal
				  if (Lletterspace.equals("0px") && lletterspace[i].equals("normal"))
				  {
					  lletterspace[i] = "0px";
				  }
			  
				  Assert.assertEquals(Lletterspace, lletterspace[i]);
				  System.out.println("Link's letter space is matching in the UI Design:" +Lletterspace);
				  System.out.println("Link's letter space is matching in the UI Design:" +lletterspace);
			  }
		  }
	
	/* Statements & Documents Service Links UI Validation */
		
		public void SDLinkFontSizeValidation()
		{
			
				String LFontvalue = redlinesconfigfilereader.getLFontvalue();
				String lfontSize[] = new String[7];
			
					lfontSize[0] = iaBrandinglocators.ViewAcctSnap.getCssValue("font-size");
					lfontSize[1] = iaBrandinglocators.ViewAcctStmt.getCssValue("font-size");
					lfontSize[2] = iaBrandinglocators.ViewEcomm.getCssValue("font-size");
					lfontSize[3] = iaBrandinglocators.ViewSecMsg.getCssValue("font-size");
					lfontSize[4] = iaBrandinglocators.Viewtxdoc.getCssValue("font-size");
					lfontSize[5] = iaBrandinglocators.MangPaperless.getCssValue("font-size");
					lfontSize[6] = iaBrandinglocators.LangPref.getCssValue("font-size");
					
					
						for (int i=0; i < lfontSize.length-1; i++)
						{
							Assert.assertEquals(LFontvalue,lfontSize[i]);
							System.out.println("Font size is matching in the UI Design:" +lfontSize);
						}
					
		}
		
		public void SDLFontFamilyValidation()
		{
			String LFontFamily = redlinesconfigfilereader.getLFontfamily();
			String lfontFamily[] = new String[7];
				lfontFamily[0] = iaBrandinglocators.ViewAcctSnap.getCssValue("font-family");
				lfontFamily[1] = iaBrandinglocators.ViewAcctStmt.getCssValue("font-family");
				lfontFamily[2] = iaBrandinglocators.ViewEcomm.getCssValue("font-family");
				lfontFamily[3] = iaBrandinglocators.ViewSecMsg.getCssValue("font-family");
				lfontFamily[4] = iaBrandinglocators.Viewtxdoc.getCssValue("font-family");
				lfontFamily[5] = iaBrandinglocators.LangPref.getCssValue("font-family");
				lfontFamily[6] = iaBrandinglocators.MangPaperless.getCssValue("font-family");
				
			for (int i=0; i < lfontFamily.length-1; i++)
			{
			 //Trim the Sans-serif from the font family
				String ff=lfontFamily[i];
				String lfontfamily = ff.substring(0, ff.indexOf(","));
			//Compare the trim with the redlines
			 Assert.assertEquals(LFontFamily, lfontfamily);
			 System.out.println("Font Family is matching in the UI Design:" +lfontfamily);
			}
		}
		
		public void SDLColorvalidation()
		{
			String LColor = redlinesconfigfilereader.getLcolor();
			String lcolor[] = new String[7];
				lcolor[0] = iaBrandinglocators.ViewAcctSnap.getCssValue("color");
				lcolor[1] = iaBrandinglocators.ViewAcctStmt.getCssValue("color");
				lcolor[2] = iaBrandinglocators.ViewEcomm.getCssValue("color");
				lcolor[3] = iaBrandinglocators.ViewSecMsg.getCssValue("color");
				lcolor[4] = iaBrandinglocators.Viewtxdoc.getCssValue("color");
				lcolor[5] = iaBrandinglocators.LangPref.getCssValue("color");
				lcolor[6] = iaBrandinglocators.MangPaperless.getCssValue("color");
				
			for (int i=0; i < lcolor.length-1; i++)
			{
			//Converting the RGB in Hex value
				String hex = Color.fromString(lcolor[i]).asHex();
			//Comparing the converted color into hex with the given value by design team
				Assert.assertEquals(LColor, hex);
				System.out.println("Color is matching in the UI Design:" +hex);
			}	
		}
		
		public void SDLLineheightvalidation()
		{
			    String LLineheight = redlinesconfigfilereader.getLLineheight();
			   	String llineHeight[] = new String[7];
				 llineHeight[0] = iaBrandinglocators.ViewAcctSnap.getCssValue("line-height");
				 llineHeight[1] = iaBrandinglocators.ViewAcctStmt.getCssValue("line-height");
				 llineHeight[2] = iaBrandinglocators.ViewEcomm.getCssValue("line-height");
				 llineHeight[3] = iaBrandinglocators.ViewSecMsg.getCssValue("line-height");
				 llineHeight[4] = iaBrandinglocators.Viewtxdoc.getCssValue("line-height");
				 llineHeight[5] = iaBrandinglocators.LangPref.getCssValue("line-height");
				 llineHeight[6] = iaBrandinglocators.MangPaperless.getCssValue("line-height");

				for (int i=0; i < llineHeight.length-1; i++)
				{
				 Assert.assertEquals(LLineheight, llineHeight[i]);
				 System.out.println("Line height is matching in the UI Design:" +llineHeight);
				}
		}
		
		  public void SDLletterspacingvalidation()
		  {
			  String Lletterspace = redlinesconfigfilereader.getLletterspace();
			  String lletterspace[] = new String[7];
			  lletterspace[0] = iaBrandinglocators.ViewAcctSnap.getCssValue("letter-spacing");
			  lletterspace[1] = iaBrandinglocators.ViewAcctStmt.getCssValue("letter-spacing");
			  lletterspace[2] = iaBrandinglocators.ViewEcomm.getCssValue("letter-spacing");
			  lletterspace[3] = iaBrandinglocators.ViewSecMsg.getCssValue("letter-spacing");
			  lletterspace[4] = iaBrandinglocators.Viewtxdoc.getCssValue("letter-spacing");
			  lletterspace[5] = iaBrandinglocators.LangPref.getCssValue("letter-spacing");
			  lletterspace[6] = iaBrandinglocators.MangPaperless.getCssValue("letter-spacing");
			  
			  for (int i=0; i < lletterspace.length-1; i++)
			  {
				  //convert the 0px into normal
				  if (Lletterspace.equals("0px") && lletterspace[i].equals("normal"))
				  {
					  lletterspace[i] = "0px";
				  }
				  Assert.assertEquals(Lletterspace, lletterspace[i]);
				  System.out.println("Link's letter space is matching in the UI Design:" +lletterspace);
			  }
		  }
	
	/* Credit Card Service Links UI Validation */	
		
		public void CRLinkFontSizeValiadation()
		{
				String LFontvalue = redlinesconfigfilereader.getLFontvalue();
				String lfontSize[] = new String[13];
				
				lfontSize[0] = iaBrandinglocators.ActivateCredcard.getCssValue("font-size");
				lfontSize[1] = iaBrandinglocators.Linkunlinkcitiac.getCssValue("font-size");
				lfontSize[2] = iaBrandinglocators.lockcard.getCssValue("font-size");
				lfontSize[3] = iaBrandinglocators.Replacecard.getCssValue("font-size");
				lfontSize[4] = iaBrandinglocators.Requestcreditcard.getCssValue("font-size");
				lfontSize[5] = iaBrandinglocators.Requestcreditlimit.getCssValue("font-size");
				lfontSize[6] = iaBrandinglocators.RequestCardAgree.getCssValue("font-size");
				lfontSize[7] = iaBrandinglocators.Authuser.getCssValue("font-size");
				lfontSize[8] = iaBrandinglocators.Viewrecchar.getCssValue("font-size");
				lfontSize[9] = iaBrandinglocators.Disputecen.getCssValue("font-size");
				lfontSize[10] = iaBrandinglocators.TextBnk.getCssValue("font-size");
				lfontSize[11] = iaBrandinglocators.Virtualacct.getCssValue("font-size");
				lfontSize[12] = iaBrandinglocators.paypal.getCssValue("font-size");
	//			lfontSize[13] = iaBrandinglocators.Citipay.getCssValue("font-size");
				
				for (int i=0; i < lfontSize.length-1; i++)
				{
					Assert.assertEquals(LFontvalue,lfontSize[i]);
					System.out.println("Font size is matching in the UI Design:" +lfontSize);
				}
		}
		
		public void CRLFontFamilyValidation()
		{
			String LFontFamily = redlinesconfigfilereader.getLFontfamily();
			String lfontFamily[] = new String[13];
			
			lfontFamily[0] = iaBrandinglocators.ActivateCredcard.getCssValue("font-family");
			lfontFamily[1] = iaBrandinglocators.Linkunlinkcitiac.getCssValue("font-family");
			lfontFamily[2] = iaBrandinglocators.lockcard.getCssValue("font-family");
			lfontFamily[3] = iaBrandinglocators.Replacecard.getCssValue("font-family");
			lfontFamily[4] = iaBrandinglocators.Requestcreditcard.getCssValue("font-family");
			lfontFamily[5] = iaBrandinglocators.Requestcreditlimit.getCssValue("font-family");
			lfontFamily[6] = iaBrandinglocators.RequestCardAgree.getCssValue("font-family");
			lfontFamily[7] = iaBrandinglocators.Authuser.getCssValue("font-family");
			lfontFamily[8] = iaBrandinglocators.Viewrecchar.getCssValue("font-family");
			lfontFamily[9] = iaBrandinglocators.Disputecen.getCssValue("font-family");
			lfontFamily[10] = iaBrandinglocators.TextBnk.getCssValue("font-family");
			lfontFamily[11] = iaBrandinglocators.Virtualacct.getCssValue("font-family");
			lfontFamily[12] = iaBrandinglocators.paypal.getCssValue("font-family");
		//	lfontFamily[13] = iaBrandinglocators.Citipay.getCssValue("font-family");
			
			for (int i=0; i < lfontFamily.length-1; i++)
			{
			 //Trim the Sans-serif from the font family
				String ff=lfontFamily[i];
				String lfontfamily = ff.substring(0, ff.indexOf(","));
			//Compare the trim with the redlines
			 Assert.assertEquals(LFontFamily, lfontfamily);
			 System.out.println("Font Family is matching in the UI Design:" +lfontfamily);
			}
		}
		
		public void CRLColorvalidation()
		{
			String LColor = redlinesconfigfilereader.getLcolor();
			String lcolor[] = new String[13];
			
			lcolor[0] = iaBrandinglocators.ActivateCredcard.getCssValue("color");
			lcolor[1] = iaBrandinglocators.Linkunlinkcitiac.getCssValue("color");
			lcolor[2] = iaBrandinglocators.lockcard.getCssValue("color");
			lcolor[3] = iaBrandinglocators.Replacecard.getCssValue("color");
			lcolor[4] = iaBrandinglocators.Requestcreditcard.getCssValue("color");
			lcolor[5] = iaBrandinglocators.Requestcreditlimit.getCssValue("color");
			lcolor[6] = iaBrandinglocators.RequestCardAgree.getCssValue("color");
			lcolor[7] = iaBrandinglocators.Authuser.getCssValue("color");
			lcolor[8] = iaBrandinglocators.Viewrecchar.getCssValue("color");
			lcolor[9] = iaBrandinglocators.Disputecen.getCssValue("color");
			lcolor[10] = iaBrandinglocators.TextBnk.getCssValue("color");
			lcolor[11] = iaBrandinglocators.Virtualacct.getCssValue("color");
			lcolor[12] = iaBrandinglocators.paypal.getCssValue("color");
	//		lcolor[13] = iaBrandinglocators.Citipay.getCssValue("color");
			
			for (int i=0; i < lcolor.length-1; i++)
			{
			//Converting the RGB in Hex value
				String hex = Color.fromString(lcolor[i]).asHex();
			//Comparing the converted color into hex with the given value by design team
				Assert.assertEquals(LColor, hex);
				System.out.println("Color is matching in the UI Design:" +hex);
			}	
		}
		
		public void CRLLineheightvalidation()
		{
			String LLineheight = redlinesconfigfilereader.getLLineheight();
			String llineHeight[] = new String[13];
			
			llineHeight[0] = iaBrandinglocators.ActivateCredcard.getCssValue("line-height");
			llineHeight[1] = iaBrandinglocators.Linkunlinkcitiac.getCssValue("line-height");
			llineHeight[2] = iaBrandinglocators.lockcard.getCssValue("line-height");
			llineHeight[3] = iaBrandinglocators.Replacecard.getCssValue("line-height");
			llineHeight[4] = iaBrandinglocators.Requestcreditcard.getCssValue("line-height");
			llineHeight[5] = iaBrandinglocators.Requestcreditlimit.getCssValue("line-height");
			llineHeight[6] = iaBrandinglocators.RequestCardAgree.getCssValue("line-height");
			llineHeight[7] = iaBrandinglocators.Authuser.getCssValue("line-height");
			llineHeight[8] = iaBrandinglocators.Viewrecchar.getCssValue("line-height");
			llineHeight[9] = iaBrandinglocators.Disputecen.getCssValue("line-height");
			llineHeight[10] = iaBrandinglocators.TextBnk.getCssValue("line-height");
			llineHeight[11] = iaBrandinglocators.Virtualacct.getCssValue("line-height");
			llineHeight[12] = iaBrandinglocators.paypal.getCssValue("line-height");
	//		llineHeight[13] = iaBrandinglocators.Citipay.getCssValue("line-height");
			
			for (int i=0; i < llineHeight.length-1; i++)
			{
			 Assert.assertEquals(LLineheight, llineHeight[i]);
			 System.out.println("Line height is matching in the UI Design:" +llineHeight);
			}	
		}
		
		public void CRletterspacingvalidation()
		  {
			  String Lletterspace = redlinesconfigfilereader.getLletterspace();
			  String lletterspace[] = new String[13];
			
			  lletterspace[0] = iaBrandinglocators.ActivateCredcard.getCssValue("letter-spacing");
			  lletterspace[1] = iaBrandinglocators.Linkunlinkcitiac.getCssValue("letter-spacing");
			  lletterspace[2] = iaBrandinglocators.lockcard.getCssValue("letter-spacing");
			  lletterspace[3] = iaBrandinglocators.Replacecard.getCssValue("letter-spacing");
			  lletterspace[4] = iaBrandinglocators.Requestcreditcard.getCssValue("letter-spacing");
			  lletterspace[5] = iaBrandinglocators.Requestcreditlimit.getCssValue("letter-spacing");
			  lletterspace[6] = iaBrandinglocators.RequestCardAgree.getCssValue("letter-spacing");
			  lletterspace[7] = iaBrandinglocators.Authuser.getCssValue("letter-spacing");
			  lletterspace[8] = iaBrandinglocators.Viewrecchar.getCssValue("letter-spacing");
			  lletterspace[9] = iaBrandinglocators.Disputecen.getCssValue("letter-spacing");
			  lletterspace[10] = iaBrandinglocators.TextBnk.getCssValue("letter-spacing");
			  lletterspace[11] = iaBrandinglocators.Virtualacct.getCssValue("letter-spacing");
			  lletterspace[12] = iaBrandinglocators.paypal.getCssValue("letter-spacing");
	//		  lletterspace[13] = iaBrandinglocators.Citipay.getCssValue("letter-spacing");
			  
			  for (int i=0; i < lletterspace.length-1; i++)
			  {
				  //convert the 0px into normal
				  if (Lletterspace.equals("0px") && lletterspace[i].equals("normal"))
				  {
					  lletterspace[i] = "0px";
				  }
				  Assert.assertEquals(Lletterspace, lletterspace[i]);
				  System.out.println("Link's letter space is matching in the UI Design:" +lletterspace);
			  }
		}

		/*Travel Services Links UI Validation */
		
		public void TSLFontSizeValidation()
		{
			String HFontvalue = redlinesconfigfilereader.getHFontvalue();
			String hfontSize[] = new String[1];
			hfontSize[0] = iaBrandinglocators.ManageTravel.getCssValue("font-size");
			for (int i=0; i < hfontSize.length-1; i++)
			{
				Assert.assertEquals(HFontvalue, hfontSize);
				System.out.println("Font Size for the heading is matching in the UI Design:" +hfontSize);	
			}
		}
		
		public void TSLFontFamilyValidation()
		{
			String HFontFamily = redlinesconfigfilereader.getHFontfamily();
			String hfontFamily[] = new String[1];
			hfontFamily[0] = iaBrandinglocators.ManageTravel.getCssValue("font-family");
			for (int i=0; i < hfontFamily.length-1; i++)
			{
				//Trim the Sans-serif from the font family
				String ff=hfontFamily[i];
				String hfontfamily = ff.substring(0, ff.indexOf(","));
				//Compare the trim with the redlines
				Assert.assertEquals(HFontFamily, hfontfamily);
				System.out.println("Font Family for the heading is matching in the UI Design:" +hfontfamily);
			}
		}
				
		public void TSLColorvalidation()
		{
			String HColor = redlinesconfigfilereader.getHcolor();
			String hcolor[] = new String[1];
			hcolor[0] = iaBrandinglocators.ManageTravel.getCssValue("color");
			for (int i=0; i < hcolor.length-1; i++)
			{
				//Converting the RGB in Hex value
				String hex = Color.fromString(hcolor[i]).asHex();
				//Comparing the converted color into hex with the given value by design team
				Assert.assertEquals(HColor, hex);
				System.out.println("Color for the heading is matching in the UI Design:" +hex);
			}
		}
		
			
		public void TSLLineheightvalidation()
		{
			String HLineheight = redlinesconfigfilereader.getHLineheight();
			String hlineHeight[] = new String[1]; 
			hlineHeight[0] = iaBrandinglocators.ManageTravel.getCssValue("line-height");			
			for (int i=0; i < hlineHeight.length-1; i++)
			{
				Assert.assertEquals(HLineheight, hlineHeight);
				System.out.println("Line height for the heading is matching in the UI Design:" +hlineHeight);
			}
		}
		
		public void TSLletterspacingvalidation()
		{
			String Hletterspace = redlinesconfigfilereader.getHletterspace();
			String hletterspace[] = new String[1];
			hletterspace[0] = iaBrandinglocators.ManageTravel.getCssValue("letter-spacing");
			for (int i=0; i < hletterspace.length-1; i++)
			  {
				  //convert the 0px into normal
				  if (Hletterspace.equals("0px") && hletterspace[i].equals("normal"))
				  {
					  hletterspace[i] = "0px";
				  }
				  
				Assert.assertEquals(Hletterspace, hletterspace);
				System.out.println("Opacity for the heading is matching in the UI Design:" +hletterspace);
			  }
		}
	
		
	/* More Settings Links UI Design Validation */
		
		public void MSLFontSizeValidation()
		{
			String HFontvalue = redlinesconfigfilereader.getHFontvalue();
			String hfontSize[] = new String[9];
			hfontSize[0] = iaBrandinglocators.Nickname.getCssValue("font-size");
			hfontSize[1] = iaBrandinglocators.ShoworHideAcct.getCssValue("font-size");
			hfontSize[2] = iaBrandinglocators.StartorHome.getCssValue("font-size");
			hfontSize[3] = iaBrandinglocators.SecuQues.getCssValue("font-size");
			hfontSize[4] = iaBrandinglocators.PINSecques.getCssValue("font-size");
			hfontSize[5] = iaBrandinglocators.Privacy.getCssValue("font-size");
			hfontSize[6] = iaBrandinglocators.twostep.getCssValue("font-size");
			hfontSize[7] = iaBrandinglocators.appacctaccess.getCssValue("font-size");
			hfontSize[8] = iaBrandinglocators.acctaler.getCssValue("font-size");
			
			for (int i=0; i < hfontSize.length-1; i++)
			{
				Assert.assertEquals(HFontvalue, hfontSize);
				System.out.println("Font Size for the heading is matching in the UI Design:" +hfontSize);	
			}
		}
		
		public void MSLFontFamilyValidation()
		{
			String HFontFamily = redlinesconfigfilereader.getHFontfamily();
			String hfontFamily[] = new String[9];

			hfontFamily[0] = iaBrandinglocators.Nickname.getCssValue("font-family");
			hfontFamily[1] = iaBrandinglocators.ShoworHideAcct.getCssValue("font-family");
			hfontFamily[2] = iaBrandinglocators.StartorHome.getCssValue("font-family");
			hfontFamily[3] = iaBrandinglocators.SecuQues.getCssValue("font-family");
			hfontFamily[4] = iaBrandinglocators.PINSecques.getCssValue("font-family");
			hfontFamily[5] = iaBrandinglocators.Privacy.getCssValue("font-family");
			hfontFamily[6] = iaBrandinglocators.twostep.getCssValue("font-family");
			hfontFamily[7] = iaBrandinglocators.appacctaccess.getCssValue("font-family");
			hfontFamily[8] = iaBrandinglocators.acctaler.getCssValue("font-family");
			for (int i=0; i < hfontFamily.length-1; i++)
			{
				//Trim the Sans-serif from the font family
				String ff=hfontFamily[i];
				String hfontfamily = ff.substring(0, ff.indexOf(","));
				//Compare the trim with the redlines
				Assert.assertEquals(HFontFamily, hfontfamily);
				System.out.println("Font Family for the heading is matching in the UI Design:" +hfontfamily);
			}
		}
				
		public void MSLColorvalidation()
		{
			String HColor = redlinesconfigfilereader.getHcolor();
			String hcolor[] = new String[9];
			hcolor[0] = iaBrandinglocators.Nickname.getCssValue("color");
			hcolor[1] = iaBrandinglocators.ShoworHideAcct.getCssValue("color");
			hcolor[2] = iaBrandinglocators.StartorHome.getCssValue("color");
			hcolor[3] = iaBrandinglocators.SecuQues.getCssValue("color");
			hcolor[4] = iaBrandinglocators.PINSecques.getCssValue("color");
			hcolor[5] = iaBrandinglocators.Privacy.getCssValue("color");
			hcolor[6] = iaBrandinglocators.twostep.getCssValue("color");
			hcolor[7] = iaBrandinglocators.appacctaccess.getCssValue("color");
			hcolor[8] = iaBrandinglocators.acctaler.getCssValue("color");
			
			for (int i=0; i < hcolor.length-1; i++)
			{
				//Converting the RGB in Hex value
				String hex = Color.fromString(hcolor[i]).asHex();
				//Comparing the converted color into hex with the given value by design team
				Assert.assertEquals(HColor, hex);
				System.out.println("Color for the heading is matching in the UI Design:" +hex);
			}
		}
		
			
		public void MSLLineheightvalidation()
		{
			String HLineheight = redlinesconfigfilereader.getHLineheight();
			String hlineHeight[] = new String[9]; 
			hlineHeight[0] = iaBrandinglocators.Nickname.getCssValue("line-height");	
			hlineHeight[1] = iaBrandinglocators.ShoworHideAcct.getCssValue("line-height");
			hlineHeight[2] = iaBrandinglocators.StartorHome.getCssValue("line-height");
			hlineHeight[3] = iaBrandinglocators.SecuQues.getCssValue("line-height");
			hlineHeight[4] = iaBrandinglocators.PINSecques.getCssValue("line-height");
			hlineHeight[5] = iaBrandinglocators.Privacy.getCssValue("line-height");
			hlineHeight[6] = iaBrandinglocators.twostep.getCssValue("line-height");
			hlineHeight[7] = iaBrandinglocators.appacctaccess.getCssValue("line-height");
			hlineHeight[8] = iaBrandinglocators.acctaler.getCssValue("line-height");
			
			for (int i=0; i < hlineHeight.length-1; i++)
			{
				Assert.assertEquals(HLineheight, hlineHeight);
				System.out.println("Line height for the heading is matching in the UI Design:" +hlineHeight);
			}
		}
		
		public void MSLletterspacingvalidation()
		{
			String Hletterspace = redlinesconfigfilereader.getHletterspace();
			String hletterspace[] = new String[9];
			hletterspace[0] = iaBrandinglocators.Nickname.getCssValue("letter-spacing");
			hletterspace[1] = iaBrandinglocators.ShoworHideAcct.getCssValue("letter-spacing");
			hletterspace[2] = iaBrandinglocators.StartorHome.getCssValue("letter-spacing");
			hletterspace[3] = iaBrandinglocators.SecuQues.getCssValue("letter-spacing");
			hletterspace[4] = iaBrandinglocators.PINSecques.getCssValue("letter-spacing");
			hletterspace[5] = iaBrandinglocators.Privacy.getCssValue("letter-spacing");
			hletterspace[6] = iaBrandinglocators.twostep.getCssValue("letter-spacing");
			hletterspace[7] = iaBrandinglocators.appacctaccess.getCssValue("letter-spacing");
			hletterspace[8] = iaBrandinglocators.acctaler.getCssValue("letter-spacing");
			
			for (int i=0; i < hletterspace.length-1; i++)
			  {
				  //convert the 0px into normal
				  if (Hletterspace.equals("0px") && hletterspace[i].equals("normal"))
				  {
					  hletterspace[i] = "0px";
				  }
				Assert.assertEquals(Hletterspace, hletterspace);
				System.out.println("Opacity for the heading is matching in the UI Design:" +hletterspace);
			  }
		}

		/* Header UI Design Validation */
		
		public void HeaderFontSizeValidation()
		{
			String HFontvalue = redlinesconfigfilereader.getHFontvalue();
			String hfontSize[] = new String[2];
			hfontSize[0] = iaBrandinglocators.AccessCheck.getCssValue("font-size");
			hfontSize[1] = iaBrandinglocators.Linkacctupgrade.getCssValue("font-size");
			for (int i=0; i < hfontSize.length-1; i++)
			{
				Assert.assertEquals(HFontvalue, hfontSize);
				System.out.println("Font Size for the heading is matching in the UI Design:" +hfontSize);	
			}
		}
		
		public void HFontFamilyValidation()
		{
			String HFontFamily = redlinesconfigfilereader.getHFontfamily();
			String hfontFamily[] = new String[2];
			hfontFamily[0] = iaBrandinglocators.AccessCheck.getCssValue("font-family");
			hfontFamily[1] = iaBrandinglocators.Linkacctupgrade.getCssValue("font-family");
			for (int i=0; i < hfontFamily.length-1; i++)
			{
				//Trim the Sans-serif from the font family
				String ff=hfontFamily[i];
				String hfontfamily = ff.substring(0, ff.indexOf(","));
				//Compare the trim with the redlines
				Assert.assertEquals(HFontFamily, hfontfamily);
				System.out.println("Font Family for the heading is matching in the UI Design:" +hfontfamily);
			}
		}
				
		public void HColorvalidation()
		{
			String HColor = redlinesconfigfilereader.getHcolor();
			String hcolor[] = new String[2];
			hcolor[0] = iaBrandinglocators.AccessCheck.getCssValue("color");
			hcolor[1] = iaBrandinglocators.Linkacctupgrade.getCssValue("color");
			for (int i=0; i < hcolor.length-1; i++)
			{
				//Converting the RGB in Hex value
				String hex = Color.fromString(hcolor[i]).asHex();
				//Comparing the converted color into hex with the given value by design team
				Assert.assertEquals(HColor, hex);
				System.out.println("Color for the heading is matching in the UI Design:" +hex);
			}
		}
		
			
		public void HLineheightvalidation()
		{
			String HLineheight = redlinesconfigfilereader.getHLineheight();
			String hlineHeight[] = new String[2]; 
			hlineHeight[0] = iaBrandinglocators.AccessCheck.getCssValue("line-height");
			hlineHeight[1] = iaBrandinglocators.Linkacctupgrade.getCssValue("line-height");
			for (int i=0; i < hlineHeight.length-1; i++)
			{
				Assert.assertEquals(HLineheight, hlineHeight);
				System.out.println("Line height for the heading is matching in the UI Design:" +hlineHeight);
			}
		}
	
		public void Hheightvalidation()
		{
			String Hheight = redlinesconfigfilereader.getHLineheight();
			String hheight[] = new String[2];
			hheight[0] = iaBrandinglocators.AccessCheck.getCssValue("height");
			hheight[1] = iaBrandinglocators.Linkacctupgrade.getCssValue("height");
			for (int i=0; i < hheight.length-1; i++)
			{
			   Assert.assertEquals(Hheight, hheight);
			   System.out.println("Opacity for the heading is matching in the UI Design:" +hheight);
		    }
		}
		
	public void Hletterspacingvalidation()
		{
			String Hletterspace = redlinesconfigfilereader.getHletterspace();
			String hletterspace[] = new String[2];
			hletterspace[0] = iaBrandinglocators.AccessCheck.getCssValue("letter-spacing");
			hletterspace[1] = iaBrandinglocators.Linkacctupgrade.getCssValue("letter-spacing");
			for (int i=0; i < hletterspace.length-1; i++)
			  {
				Assert.assertEquals(Hletterspace, hletterspace);
				System.out.println("Opacity for the heading is matching in the UI Design:" +hletterspace);
			  }
		}
	
		
 
	   public boolean islinkpresent() 
	   {
		   Boolean isPresent = SeleniumDriver.driver.findElements(By.linkText("Language Preferences")).size() > 0;
		  
		   return isPresent;
		   
	   }
	   
	   public boolean useridislinkpresent() 
	   {
		   Boolean isPresent = SeleniumDriver.driver.findElements(By.linkText("Identificación de Usuario")).size() > 0;
		  
		   return isPresent;
		   
	   }
	   
	   public boolean Cancelislinkpresent() 
	   {
		   Boolean isPresent = SeleniumDriver.driver.findElements(By.linkText("Cancelar")).size() > 0;
		  
		   return isPresent;
		   
	   }
}
