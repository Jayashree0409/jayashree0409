package pageActions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import pageLocators.IABrandingMenusLocators;
import pageLocators.ProductExplorerLocators;
import pageLocators.Medallia_FeedbackLocator;
import utils.SeleniumDriver;



public class Medallia_FeedbackActions {
	
	final Log log = LogFactory.getLog(Medallia_FeedbackActions.class.getName());

	ProductExplorerLocators productexplorerlocators=null;
	CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
	IABrandingMenusLocators iaBrandingMenusLocators=null;
	Medallia_FeedbackLocator medallia_Feedback=null;
	
	//Constructor
		public  Medallia_FeedbackActions()
		{
			this.productexplorerlocators = new ProductExplorerLocators();
			this.iaBrandingMenusLocators = new IABrandingMenusLocators();
			this.medallia_Feedback = new Medallia_FeedbackLocator();
			PageFactory.initElements(SeleniumDriver.getDriver(), productexplorerlocators);
			PageFactory.initElements(SeleniumDriver.getDriver(), iaBrandingMenusLocators);
			PageFactory.initElements(SeleniumDriver.getDriver(), medallia_Feedback);
		}
		
		
		public void medialla()
		{
				
				try {
				Thread.sleep(200);
				} 	catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
			
				cbolCommonActions.CBOLPerformClick(medallia_Feedback.medaillia_form);
			}
			
	
		public void Feedback_icon()
		{
			try {
			Thread.sleep(1000);
			} 	catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
			cbolCommonActions.CBOLPerformClick(medallia_Feedback.Feedback_Icon);
		}
		

		public void wait_screen()
		{
			try {
				Thread.sleep(1000);
				} 	catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
		}
		
		
}
