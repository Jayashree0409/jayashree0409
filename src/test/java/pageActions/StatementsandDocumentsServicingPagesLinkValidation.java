package pageActions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import pageLocators.IABrandingUIDesignLocator;
import utils.SeleniumDriver;
import pageLocators.IABrandingLocators;
import utils.ServiceLinksConfigFileReader;


public class StatementsandDocumentsServicingPagesLinkValidation {
		
		CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
		IABrandingUIDesignLocator iaBrandinguidesignLocators=null;
		ServiceLinksConfigFileReader servicelinksconfigfilereader=null;
		IABrandingLocators iaBrandinglocators=null;
		final Log log = LogFactory.getLog(StatementsandDocumentsServicingPagesLinkValidation.class.getName());
	
		//Constructor
		public  StatementsandDocumentsServicingPagesLinkValidation()
		{
			this.iaBrandinguidesignLocators = new IABrandingUIDesignLocator();
			this.servicelinksconfigfilereader = new ServiceLinksConfigFileReader();
			this.iaBrandinglocators = new IABrandingLocators();
			PageFactory.initElements(SeleniumDriver.getDriver(), iaBrandinguidesignLocators);
			PageFactory.initElements(SeleniumDriver.getDriver(), servicelinksconfigfilereader);
			PageFactory.initElements(SeleniumDriver.getDriver(), iaBrandinglocators);
		}
		
		public void services()
		{
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("/ag")) 
			{
			cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.AGServices);
			}
			else
			{
				cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.Services);
			}
			
		}
		
		
		public void profiles()
		{
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("/ag")) 
			{
			cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.AGprofiles);
			}
			else
			{
				cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.profiles);
			}
			
		}
			
		public void StatementsandDocuments()
		{
			try
			{
				Thread.sleep(2000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.StatementsandDocuments);
		}
		
		public void TravelServices()
		{
			try
			{
				Thread.sleep(2000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Travelservice);
		}
		
		
	/* Statements & documents Service Links Validation */
		
		public void AccountSnapshotValidation()
		{
			String currenturl="";
			String LAccountSnapshot = servicelinksconfigfilereader.getViewAccountSnapshot();
			System.out.println(LAccountSnapshot);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.ViewAcctSnap);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LAccountSnapshot,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LAccountSnapshot,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LAccountSnapshot,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}
	
		}
		
		public void AccountstatementValidation()
		{
			String currenturl="";
			String LAccountstatement = servicelinksconfigfilereader.getViewAccountstatements();
			System.out.println(LAccountstatement);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.ViewAcctStmt);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LAccountstatement,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LAccountstatement,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LAccountstatement,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}

		}
		
		public void EcommunicationsValidation()
		{
			String currenturl="";
			String LEcommunications = servicelinksconfigfilereader.getViewEcommunications();
			System.out.println(LEcommunications);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.ViewEcomm);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LEcommunications,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LEcommunications,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LEcommunications,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}

		}
		
		public void SecureMessageCenterValidation()
		{
			String currenturl="";
			String LSecureMessageCenter = servicelinksconfigfilereader.getViewSecureMessageCenter();
			System.out.println(LSecureMessageCenter);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.ViewSecMsg);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LSecureMessageCenter,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LSecureMessageCenter,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LSecureMessageCenter,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}

		}
		
		public void TaxDocumentsValidation()
		{
			String currenturl="";
			String LTaxDocuments = servicelinksconfigfilereader.getViewTaxDocuments();
			System.out.println(LTaxDocuments);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Viewtxdoc);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LTaxDocuments,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LTaxDocuments,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LTaxDocuments,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}

		}
		
		public void ManagePaperlessSettingsValidation()
		{
			String currenturl="";
			String LManagePaperlessSettings = servicelinksconfigfilereader.getManagePaperlessSettings();
			System.out.println(LManagePaperlessSettings);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.MangPaperless);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LManagePaperlessSettings,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LManagePaperlessSettings,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LManagePaperlessSettings,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}

		}
		
		public void LanguagePreferenceValidation()
		{
			String currenturl="";
			String LLanguagePreference = servicelinksconfigfilereader.getLanguagePreference();
			System.out.println(LLanguagePreference);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.LangPref);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN")) 
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LLanguagePreference,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LLanguagePreference,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LLanguagePreference,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}
			
		}
		
	/*Travel Service Links validation */
		
		public void ManageTravelNoticesValidation()
		{
			String currenturl="";
			String LManageTravelNotices = servicelinksconfigfilereader.getManageTravelNotices();
			System.out.println(LManageTravelNotices);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.ManageTravel);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN")) 
				{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LManageTravelNotices,currenturl);
				System.out.println("Links match as per the requirement for enter");
				}
			else if(CurrentURL.contains("?JFP_TOKEN"))
				{
					System.out.println("enter1");
					currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
					Assert.assertEquals(LManageTravelNotices,currenturl);
					System.out.println("Links match as per the requirement for enter1");
				}
			
			else
			{
				System.out.println("end");
				Assert.assertEquals(LManageTravelNotices,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}

		}
		

	   public boolean islinkpresent() 
	   
	   {
		   Boolean isPresent = SeleniumDriver.driver.findElements(By.linkText("Language Preferences")).size() > 0;
		  
		   return isPresent;
		   
	   }
}
