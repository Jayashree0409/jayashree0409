package pageActions;

import static org.testng.Assert.assertTrue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.Color;
import pageLocators.IABrandingUIDesignLocator;
import utils.SeleniumDriver;
import utils.redlinesConfigFileReader;
import pageLocators.IABrandingLocators;
import pageLocators.ProductExplorerLocators;
//import utils.ExcelRedlinesutils;


public class IABrandingUISpanishDesignActions {
		
		CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
		IABrandingUIDesignLocator iaBrandinguidesignLocators=null;
		redlinesConfigFileReader redlinesconfigfilereader=null;
		IABrandingLocators iaBrandinglocators=null;
		ProductExplorerLocators productexplorerlocators=null;
		final Log log = LogFactory.getLog(IABrandingUISpanishDesignActions.class.getName());
		
		//public XSSFRow row;
		
		//Constructor
		public  IABrandingUISpanishDesignActions()
		{
			this.iaBrandinguidesignLocators = new IABrandingUIDesignLocator();
			this.redlinesconfigfilereader = new redlinesConfigFileReader();
			this.iaBrandinglocators = new IABrandingLocators();
			this.productexplorerlocators = new ProductExplorerLocators();
			PageFactory.initElements(SeleniumDriver.getDriver(), iaBrandinguidesignLocators);
			PageFactory.initElements(SeleniumDriver.getDriver(), redlinesconfigfilereader);
			PageFactory.initElements(SeleniumDriver.getDriver(), iaBrandinglocators);
			PageFactory.initElements(SeleniumDriver.getDriver(), productexplorerlocators);
		}
		
		public void SpanishLanguage()
		{
			   cbolCommonActions.CBOLPerformClick(productexplorerlocators.LanguageSpanish);
		}
		
		public void services()
		{
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("/ag")) 
			{
			cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.AGServices);
			}
			else
			{
				cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.Services);
			}

		}
	
		public void profiles()
		{
			
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("/ag")) 
			{
			cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.AGprofiles);
			}
			else
			{
				cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.profiles);
			}
			
		}
		
		public void BankingServices()
		{
		
			try 
			{
				Thread.sleep(5000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}	
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.SpanishBankingServices);
		}
		
		public void CreditCardServices()
		{
		
			try 
			{
				Thread.sleep(5000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}	
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.SpanishCreditcardservices);
		}
		
		public void TravelServices()
		{
		
			try 
			{
				Thread.sleep(5000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}	
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.SpanishTravelservice);
		}
		
		public void StatementsandDocuments()
		{
			try
			{
				Thread.sleep(5000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.SpanishStatementsandDocuments);
		}
		
		public void MoreSettings()
		{
			try
			{
				Thread.sleep(5000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.SpanishMoresettings);
		}
		
	/*Spanish Banking service Links UI Validation */
		
		public void LinkFontSizeValidation()
		{
			String LFontvalue = redlinesconfigfilereader.getLFontvalue();
			String lfontSize[] = new String[17];
			
			 lfontSize[0] = iaBrandinglocators.SpanishLockCard.getCssValue("font-size");
			 lfontSize[1] = iaBrandinglocators.SpanishReplaceATMcard.getCssValue("font-size");
			 lfontSize[2] = iaBrandinglocators.SpanishChangeATMCard.getCssValue("font-size");
			 lfontSize[3] = iaBrandinglocators.SpanishStopCheck.getCssValue("font-size");
			 lfontSize[4] = iaBrandinglocators.SpanishReordercheck.getCssValue("font-size");
			 lfontSize[5] = iaBrandinglocators.SpanishTextbank.getCssValue("font-size");
			 lfontSize[6] = iaBrandinglocators.SpanishTeleAccesscode.getCssValue("font-size");
			 lfontSize[7] = iaBrandinglocators.SpanishResetATMPIN.getCssValue("font-size");
			 lfontSize[8] = iaBrandinglocators.SpanishLinkorunlinkacct.getCssValue("font-size");
			 lfontSize[9] = iaBrandinglocators.SpanishLinkorUnlinksaving.getCssValue("font-size");
			 lfontSize[10] = iaBrandinglocators.SpanishUpgradeCitiPriority.getCssValue("font-size");
			 lfontSize[11] = iaBrandinglocators.SpanishUpgradeCitigold.getCssValue("font-size");
			 lfontSize[12] = iaBrandinglocators.SpanishLinkCitiAccount.getCssValue("font-size");
			 lfontSize[13] = iaBrandinglocators.SpanishFinanceTool.getCssValue("font-size");
			 lfontSize[14] = iaBrandinglocators.SpanishInvest.getCssValue("font-size");
			 lfontSize[15] = iaBrandinglocators.SpanishManageAppt.getCssValue("font-size");
			 lfontSize[16] = iaBrandinglocators.SpanishScheduleappt.getCssValue("font-size");
		
			for (int i=0; i < lfontSize.length-1; i++)
			{
			 Assert.assertEquals(LFontvalue,lfontSize[i]);
			 System.out.println("Font size is matching in the UI Design:" +lfontSize);
			}
		}
		
		public void LFontFamilyValidation()
		{
			String LFontFamily = redlinesconfigfilereader.getLFontfamily();
			String lfontFamily[] = new String[17];
				lfontFamily[0] = iaBrandinglocators.SpanishLockCard.getCssValue("font-family");
				lfontFamily[1] = iaBrandinglocators.SpanishReplaceATMcard.getCssValue("font-family");
				lfontFamily[2] = iaBrandinglocators.SpanishChangeATMCard.getCssValue("font-family");
				lfontFamily[3] = iaBrandinglocators.SpanishStopCheck.getCssValue("font-family");
				lfontFamily[4] = iaBrandinglocators.SpanishReordercheck.getCssValue("font-family");
				lfontFamily[5] = iaBrandinglocators.SpanishTextbank.getCssValue("font-family");
				lfontFamily[6] = iaBrandinglocators.SpanishTeleAccesscode.getCssValue("font-family");
				lfontFamily[7] = iaBrandinglocators.SpanishResetATMPIN.getCssValue("font-family");
				lfontFamily[8] = iaBrandinglocators.SpanishLinkorunlinkacct.getCssValue("font-family");
				lfontFamily[9] = iaBrandinglocators.SpanishLinkorUnlinksaving.getCssValue("font-family");
				lfontFamily[10] = iaBrandinglocators.SpanishUpgradeCitiPriority.getCssValue("font-family");
				lfontFamily[11] = iaBrandinglocators.SpanishUpgradeCitigold.getCssValue("font-family");
				lfontFamily[12] = iaBrandinglocators.SpanishLinkCitiAccount.getCssValue("font-family");
				lfontFamily[13] = iaBrandinglocators.SpanishFinanceTool.getCssValue("font-family");
				lfontFamily[14] = iaBrandinglocators.SpanishInvest.getCssValue("font-family");
				lfontFamily[15] = iaBrandinglocators.SpanishManageAppt.getCssValue("font-family");
				lfontFamily[16] = iaBrandinglocators.SpanishScheduleappt.getCssValue("font-family");
				
			for (int i=0; i < lfontFamily.length-1; i++)
			{
			 //Trim the Sans-serif from the font family
				String ff=lfontFamily[i];
				String lfontfamily = ff.substring(0, ff.indexOf(","));
			//Compare the trim with the redlines
			 Assert.assertEquals(LFontFamily, lfontfamily);
			 System.out.println("Font Family is matching in the UI Design:" +lfontfamily);
			}
		}
		
		public void LColorvalidation()
		{
			String LColor = redlinesconfigfilereader.getLcolor();
			String lcolor[] = new String[17];
				lcolor[0] = iaBrandinglocators.SpanishLockCard.getCssValue("color");
				lcolor[1] = iaBrandinglocators.SpanishReplaceATMcard.getCssValue("color");
				lcolor[2] = iaBrandinglocators.SpanishChangeATMCard.getCssValue("color");
				lcolor[3] = iaBrandinglocators.SpanishStopCheck.getCssValue("color");
				lcolor[4] = iaBrandinglocators.SpanishReordercheck.getCssValue("color");
				lcolor[5] = iaBrandinglocators.SpanishTextbank.getCssValue("color");
				lcolor[6] = iaBrandinglocators.SpanishTeleAccesscode.getCssValue("color");
				lcolor[7] = iaBrandinglocators.SpanishResetATMPIN.getCssValue("color");
				lcolor[8] = iaBrandinglocators.SpanishLinkorunlinkacct.getCssValue("color");
				lcolor[9] = iaBrandinglocators.SpanishLinkorUnlinksaving.getCssValue("color");
				lcolor[10] = iaBrandinglocators.SpanishUpgradeCitiPriority.getCssValue("color");
				lcolor[11] = iaBrandinglocators.SpanishUpgradeCitigold.getCssValue("color");
				lcolor[12] = iaBrandinglocators.SpanishLinkCitiAccount.getCssValue("color");
				lcolor[13] = iaBrandinglocators.SpanishFinanceTool.getCssValue("color");
				lcolor[14] = iaBrandinglocators.SpanishInvest.getCssValue("color");
				lcolor[15] = iaBrandinglocators.SpanishManageAppt.getCssValue("color");
				lcolor[16] = iaBrandinglocators.SpanishScheduleappt.getCssValue("color");
				
			for (int i=0; i < lcolor.length-1; i++)
			{
			//Converting the RGB in Hex value
				String hex = Color.fromString(lcolor[i]).asHex();
			//Comparing the converted color into hex with the given value by design team
				Assert.assertEquals(LColor, hex);
				System.out.println("Color is matching in the UI Design:" +hex);
			}	
		}
		
		public void LLineheightvalidation()
		{
			    String LLineheight = redlinesconfigfilereader.getLLineheight();
			   	String llineHeight[] = new String[17];
				 llineHeight[0] = iaBrandinglocators.SpanishLockCard.getCssValue("line-height");
				 llineHeight[1] = iaBrandinglocators.SpanishReplaceATMcard.getCssValue("line-height");
				 llineHeight[2] = iaBrandinglocators.SpanishChangeATMCard.getCssValue("line-height");
				 llineHeight[3] = iaBrandinglocators.SpanishStopCheck.getCssValue("line-height");
				 llineHeight[4] = iaBrandinglocators.SpanishReordercheck.getCssValue("line-height");
				 llineHeight[5] = iaBrandinglocators.SpanishTextbank.getCssValue("line-height");
				 llineHeight[6] = iaBrandinglocators.SpanishTeleAccesscode.getCssValue("line-height");
				 llineHeight[7] = iaBrandinglocators.SpanishResetATMPIN.getCssValue("line-height");
				 llineHeight[8] = iaBrandinglocators.SpanishLinkorunlinkacct.getCssValue("line-height");
				 llineHeight[9] = iaBrandinglocators.SpanishLinkorUnlinksaving.getCssValue("line-height");
				 llineHeight[10] = iaBrandinglocators.SpanishUpgradeCitiPriority.getCssValue("line-height");
				 llineHeight[11] = iaBrandinglocators.SpanishUpgradeCitigold.getCssValue("line-height");
				 llineHeight[12] = iaBrandinglocators.SpanishLinkCitiAccount.getCssValue("line-height");
				 llineHeight[13] = iaBrandinglocators.SpanishFinanceTool.getCssValue("line-height");
				 llineHeight[14] = iaBrandinglocators.SpanishInvest.getCssValue("line-height");
				 llineHeight[15] = iaBrandinglocators.SpanishManageAppt.getCssValue("line-height");
				 llineHeight[16] = iaBrandinglocators.SpanishScheduleappt.getCssValue("line-height");

				for (int i=0; i < llineHeight.length-1; i++)
				{
				 Assert.assertEquals(LLineheight, llineHeight[i]);
				 System.out.println("Line height is matching in the UI Design:" +llineHeight);
				}
		}
		
		  public void Lletterspacingvalidation()
		  {
			  String Lletterspace = redlinesconfigfilereader.getLletterspace();
			  String lletterspace[] = new String[17];
			  lletterspace[0] = iaBrandinglocators.SpanishLockCard.getCssValue("letter-space");
			  lletterspace[1] = iaBrandinglocators.SpanishReplaceATMcard.getCssValue("letter-space");
			  lletterspace[2] = iaBrandinglocators.SpanishChangeATMCard.getCssValue("letter-space");
			  lletterspace[3] = iaBrandinglocators.SpanishStopCheck.getCssValue("letter-space");
			  lletterspace[4] = iaBrandinglocators.SpanishReordercheck.getCssValue("letter-space");
			  lletterspace[5] = iaBrandinglocators.SpanishTextbank.getCssValue("letter-space");
			  lletterspace[6] = iaBrandinglocators.SpanishTeleAccesscode.getCssValue("letter-space");
			  lletterspace[7] = iaBrandinglocators.SpanishResetATMPIN.getCssValue("letter-space");
			  lletterspace[8] = iaBrandinglocators.SpanishLinkorunlinkacct.getCssValue("letter-space");
			  lletterspace[9] = iaBrandinglocators.SpanishLinkorUnlinksaving.getCssValue("letter-space");
			  lletterspace[10] = iaBrandinglocators.SpanishUpgradeCitiPriority.getCssValue("letter-space");
			  lletterspace[11] = iaBrandinglocators.SpanishUpgradeCitigold.getCssValue("letter-space");
			  lletterspace[12] = iaBrandinglocators.SpanishLinkCitiAccount.getCssValue("letter-space");
			  lletterspace[13] = iaBrandinglocators.SpanishFinanceTool.getCssValue("letter-space");
			  lletterspace[14] = iaBrandinglocators.SpanishInvest.getCssValue("letter-space");
			  lletterspace[15] = iaBrandinglocators.SpanishManageAppt.getCssValue("letter-space");
			  lletterspace[16] = iaBrandinglocators.SpanishScheduleappt.getCssValue("letter-space");
			  
			  for (int i=0; i < lletterspace.length-1; i++)
			  {
				  Assert.assertEquals(Lletterspace, lletterspace[i]);
				  System.out.println("Link's letter space is matching in the UI Design:" +lletterspace);
			  }
		  }
		  
		  
		  
		/* Statements & Documents Links UI Design Validation */
		
		public void SDLinkFontSizeValidation()
		{
			String LFontvalue = redlinesconfigfilereader.getLFontvalue();
			String lfontSize[] = new String[7];
			
			 lfontSize[0] = iaBrandinglocators.SpanishViewAcctSnap.getCssValue("font-size");
			 lfontSize[1] = iaBrandinglocators.SpanishViewAcctStmt.getCssValue("font-size");
			 lfontSize[2] = iaBrandinglocators.SpanishViewEcomm.getCssValue("font-size");
			 lfontSize[3] = iaBrandinglocators.SpanishViewSecMsg.getCssValue("font-size");
			 lfontSize[4] = iaBrandinglocators.SpanishViewtxdoc.getCssValue("font-size");
			 lfontSize[5] = iaBrandinglocators.SpanishMangPaperless.getCssValue("font-size");
			 lfontSize[6] = iaBrandinglocators.SpanishLangPref.getCssValue("font-size");

		
			for (int i=0; i < lfontSize.length-1; i++)
			{
			 Assert.assertEquals(LFontvalue,lfontSize[i]);
			 System.out.println("Font size is matching in the UI Design:" +lfontSize);
			}
		}
		

		
		public void SDLFontFamilyValidation()
		{
			String LFontFamily = redlinesconfigfilereader.getLFontfamily();
			String lfontFamily[] = new String[7];
			lfontFamily[0] = iaBrandinglocators.SpanishViewAcctSnap.getCssValue("font-family");
			lfontFamily[1] = iaBrandinglocators.SpanishViewAcctStmt.getCssValue("font-family");
			lfontFamily[2] = iaBrandinglocators.SpanishViewEcomm.getCssValue("font-family");
			lfontFamily[3] = iaBrandinglocators.SpanishViewSecMsg.getCssValue("font-family");
			lfontFamily[4] = iaBrandinglocators.SpanishViewtxdoc.getCssValue("font-family");
			lfontFamily[5] = iaBrandinglocators.SpanishMangPaperless.getCssValue("font-family");
			lfontFamily[6] = iaBrandinglocators.SpanishLangPref.getCssValue("font-family");
				
			for (int i=0; i < lfontFamily.length-1; i++)
			{
			 //Trim the Sans-serif from the font family
				String ff=lfontFamily[i];
				String lfontfamily = ff.substring(0, ff.indexOf(","));
			//Compare the trim with the redlines
			 Assert.assertEquals(LFontFamily, lfontfamily);
			 System.out.println("Font Family is matching in the UI Design:" +lfontfamily);
			}
		}
		

		
		public void SDLColorvalidation()
		{
			String LColor = redlinesconfigfilereader.getLcolor();
			String lcolor[] = new String[7];
			lcolor[0] = iaBrandinglocators.SpanishViewAcctSnap.getCssValue("color");
			lcolor[1] = iaBrandinglocators.SpanishViewAcctStmt.getCssValue("color");
			lcolor[2] = iaBrandinglocators.SpanishViewEcomm.getCssValue("color");
			lcolor[3] = iaBrandinglocators.SpanishViewSecMsg.getCssValue("color");
			lcolor[4] = iaBrandinglocators.SpanishViewtxdoc.getCssValue("color");
			lcolor[5] = iaBrandinglocators.SpanishMangPaperless.getCssValue("color");
			lcolor[6] = iaBrandinglocators.SpanishLangPref.getCssValue("color");

			for (int i=0; i < lcolor.length-1; i++)
			{
			//Converting the RGB in Hex value
				String hex = Color.fromString(lcolor[i]).asHex();
			//Comparing the converted color into hex with the given value by design team
				Assert.assertEquals(LColor, hex);
				System.out.println("Color is matching in the UI Design:" +hex);
			}	
		}
		

		
		public void SDLLineheightvalidation()
		{
			    String LLineheight = redlinesconfigfilereader.getLLineheight();
			   	String llineHeight[] = new String[7];
			   	
			   	llineHeight[0] = iaBrandinglocators.SpanishViewAcctSnap.getCssValue("line-height");
			   	llineHeight[1] = iaBrandinglocators.SpanishViewAcctStmt.getCssValue("line-height");
			   	llineHeight[2] = iaBrandinglocators.SpanishViewEcomm.getCssValue("line-height");
			   	llineHeight[3] = iaBrandinglocators.SpanishViewSecMsg.getCssValue("line-height");
			   	llineHeight[4] = iaBrandinglocators.SpanishViewtxdoc.getCssValue("line-height");
			   	llineHeight[5] = iaBrandinglocators.SpanishMangPaperless.getCssValue("line-height");
			   	llineHeight[6] = iaBrandinglocators.SpanishLangPref.getCssValue("line-height");

				for (int i=0; i < llineHeight.length-1; i++)
				{
				 Assert.assertEquals(LLineheight, llineHeight[i]);
				 System.out.println("Line height is matching in the UI Design:" +llineHeight);
				}
		}
		
	  
	  public void SDLletterspacingvalidation()
	  {
		  String Lletterspace = redlinesconfigfilereader.getLletterspace();
		  String lletterspace[] = new String[7];
		  
		  	lletterspace[0] = iaBrandinglocators.SpanishViewAcctSnap.getCssValue("letter-space");
		  	lletterspace[1] = iaBrandinglocators.SpanishViewAcctStmt.getCssValue("letter-space");
		  	lletterspace[2] = iaBrandinglocators.SpanishViewEcomm.getCssValue("letter-space");
			lletterspace[3] = iaBrandinglocators.SpanishViewSecMsg.getCssValue("letter-space");
		   	lletterspace[4] = iaBrandinglocators.SpanishViewtxdoc.getCssValue("letter-space");
		   	lletterspace[5] = iaBrandinglocators.SpanishMangPaperless.getCssValue("letter-space");
		   	lletterspace[6] = iaBrandinglocators.SpanishLangPref.getCssValue("letter-space");
			  
		  for (int i=0; i < lletterspace.length-1; i++)
		  {
			  Assert.assertEquals(Lletterspace, lletterspace[i]);
			  System.out.println("Link's letter space is matching in the UI Design:" +lletterspace);
		  }
	  }

	  /* Credit Card Service Links UI Validation */	
		
		public void CRLinkFontSizeValiadation()
		{
				String LFontvalue = redlinesconfigfilereader.getLFontvalue();
				String lfontSize[] = new String[13];
				
				lfontSize[0] = iaBrandinglocators.SpanishActivateCredcard.getCssValue("font-size");
				lfontSize[1] = iaBrandinglocators.SpanishLinkunlinkcitiac.getCssValue("font-size");
				lfontSize[2] = iaBrandinglocators.Spanishlockcard.getCssValue("font-size");
				lfontSize[3] = iaBrandinglocators.SpanishReplacecard.getCssValue("font-size");
				lfontSize[4] = iaBrandinglocators.SpanishRequestcreditcard.getCssValue("font-size");
				lfontSize[5] = iaBrandinglocators.SpanishRequestcreditlimit.getCssValue("font-size");
				lfontSize[6] = iaBrandinglocators.SpanishRequestCardAgree.getCssValue("font-size");
				lfontSize[7] = iaBrandinglocators.SpanishAuthuser.getCssValue("font-size");
				lfontSize[8] = iaBrandinglocators.SpanishViewrecchar.getCssValue("font-size");
				lfontSize[9] = iaBrandinglocators.SpanishDisputecen.getCssValue("font-size");
				lfontSize[10] = iaBrandinglocators.SpanishTextBnk.getCssValue("font-size");
				lfontSize[11] = iaBrandinglocators.SpanishVirtualacct.getCssValue("font-size");
				lfontSize[12] = iaBrandinglocators.paypal.getCssValue("font-size");
	//			lfontSize[13] = iaBrandinglocators.Citipay.getCssValue("font-size");
				
				for (int i=0; i < lfontSize.length-1; i++)
				{
					Assert.assertEquals(LFontvalue,lfontSize[i]);
					System.out.println("Font size is matching in the UI Design:" +lfontSize);
				}
		}
		
		public void CRLFontFamilyValidation()
		{
			String LFontFamily = redlinesconfigfilereader.getLFontfamily();
			String lfontFamily[] = new String[13];
			
			lfontFamily[0] = iaBrandinglocators.SpanishActivateCredcard.getCssValue("font-family");
			lfontFamily[1] = iaBrandinglocators.SpanishLinkunlinkcitiac.getCssValue("font-family");
			lfontFamily[2] = iaBrandinglocators.Spanishlockcard.getCssValue("font-family");
			lfontFamily[3] = iaBrandinglocators.SpanishReplacecard.getCssValue("font-family");
			lfontFamily[4] = iaBrandinglocators.SpanishRequestcreditcard.getCssValue("font-family");
			lfontFamily[5] = iaBrandinglocators.SpanishRequestcreditlimit.getCssValue("font-family");
			lfontFamily[6] = iaBrandinglocators.SpanishRequestCardAgree.getCssValue("font-family");
			lfontFamily[7] = iaBrandinglocators.SpanishAuthuser.getCssValue("font-family");
			lfontFamily[8] = iaBrandinglocators.SpanishViewrecchar.getCssValue("font-family");
			lfontFamily[9] = iaBrandinglocators.SpanishDisputecen.getCssValue("font-family");
			lfontFamily[10] = iaBrandinglocators.SpanishTextBnk.getCssValue("font-family");
			lfontFamily[11] = iaBrandinglocators.SpanishVirtualacct.getCssValue("font-family");
			lfontFamily[12] = iaBrandinglocators.paypal.getCssValue("font-family");
		//	lfontFamily[13] = iaBrandinglocators.Citipay.getCssValue("font-family");
			
			for (int i=0; i < lfontFamily.length-1; i++)
			{
			 //Trim the Sans-serif from the font family
				String ff=lfontFamily[i];
				String lfontfamily = ff.substring(0, ff.indexOf(","));
			//Compare the trim with the redlines
			 Assert.assertEquals(LFontFamily, lfontfamily);
			 System.out.println("Font Family is matching in the UI Design:" +lfontfamily);
			}
		}
		
		public void CRLColorvalidation()
		{
			String LColor = redlinesconfigfilereader.getLcolor();
			String lcolor[] = new String[13];
			
			lcolor[0] = iaBrandinglocators.SpanishActivateCredcard.getCssValue("color");
			lcolor[1] = iaBrandinglocators.SpanishLinkunlinkcitiac.getCssValue("color");
			lcolor[2] = iaBrandinglocators.Spanishlockcard.getCssValue("color");
			lcolor[3] = iaBrandinglocators.SpanishReplacecard.getCssValue("color");
			lcolor[4] = iaBrandinglocators.SpanishRequestcreditcard.getCssValue("color");
			lcolor[5] = iaBrandinglocators.SpanishRequestcreditlimit.getCssValue("color");
			lcolor[6] = iaBrandinglocators.SpanishRequestCardAgree.getCssValue("color");
			lcolor[7] = iaBrandinglocators.SpanishAuthuser.getCssValue("color");
			lcolor[8] = iaBrandinglocators.SpanishViewrecchar.getCssValue("color");
			lcolor[9] = iaBrandinglocators.SpanishDisputecen.getCssValue("color");
			lcolor[10] = iaBrandinglocators.SpanishTextBnk.getCssValue("color");
			lcolor[11] = iaBrandinglocators.SpanishVirtualacct.getCssValue("color");
			lcolor[12] = iaBrandinglocators.paypal.getCssValue("color");
	//		lcolor[13] = iaBrandinglocators.Citipay.getCssValue("color");
			
			for (int i=0; i < lcolor.length-1; i++)
			{
			//Converting the RGB in Hex value
				String hex = Color.fromString(lcolor[i]).asHex();
			//Comparing the converted color into hex with the given value by design team
				Assert.assertEquals(LColor, hex);
				System.out.println("Color is matching in the UI Design:" +hex);
			}	
		}
		
		public void CRLLineheightvalidation()
		{
			String LLineheight = redlinesconfigfilereader.getLLineheight();
			String llineHeight[] = new String[13];
			
			llineHeight[0] = iaBrandinglocators.SpanishActivateCredcard.getCssValue("line-height");
			llineHeight[1] = iaBrandinglocators.SpanishLinkunlinkcitiac.getCssValue("line-height");
			llineHeight[2] = iaBrandinglocators.Spanishlockcard.getCssValue("line-height");
			llineHeight[3] = iaBrandinglocators.SpanishReplacecard.getCssValue("line-height");
			llineHeight[4] = iaBrandinglocators.SpanishRequestcreditcard.getCssValue("line-height");
			llineHeight[5] = iaBrandinglocators.SpanishRequestcreditlimit.getCssValue("line-height");
			llineHeight[6] = iaBrandinglocators.SpanishRequestCardAgree.getCssValue("line-height");
			llineHeight[7] = iaBrandinglocators.SpanishAuthuser.getCssValue("line-height");
			llineHeight[8] = iaBrandinglocators.SpanishViewrecchar.getCssValue("line-height");
			llineHeight[9] = iaBrandinglocators.SpanishDisputecen.getCssValue("line-height");
			llineHeight[10] = iaBrandinglocators.SpanishTextBnk.getCssValue("line-height");
			llineHeight[11] = iaBrandinglocators.SpanishVirtualacct.getCssValue("line-height");
			llineHeight[12] = iaBrandinglocators.paypal.getCssValue("line-height");
	//		llineHeight[13] = iaBrandinglocators.Citipay.getCssValue("line-height");
			
			for (int i=0; i < llineHeight.length-1; i++)
			{
			 Assert.assertEquals(LLineheight, llineHeight[i]);
			 System.out.println("Line height is matching in the UI Design:" +llineHeight);
			}	
		}
		
		public void CRletterspacingvalidation()
		  {
			  String Lletterspace = redlinesconfigfilereader.getLletterspace();
			  String lletterspace[] = new String[13];
			
			  lletterspace[0] = iaBrandinglocators.SpanishActivateCredcard.getCssValue("letter-spacing");
			  lletterspace[1] = iaBrandinglocators.SpanishLinkunlinkcitiac.getCssValue("letter-spacing");
			  lletterspace[2] = iaBrandinglocators.Spanishlockcard.getCssValue("letter-spacing");
			  lletterspace[3] = iaBrandinglocators.SpanishReplacecard.getCssValue("letter-spacing");
			  lletterspace[4] = iaBrandinglocators.SpanishRequestcreditcard.getCssValue("letter-spacing");
			  lletterspace[5] = iaBrandinglocators.SpanishRequestcreditlimit.getCssValue("letter-spacing");
			  lletterspace[6] = iaBrandinglocators.SpanishRequestCardAgree.getCssValue("letter-spacing");
			  lletterspace[7] = iaBrandinglocators.SpanishAuthuser.getCssValue("letter-spacing");
			  lletterspace[8] = iaBrandinglocators.SpanishViewrecchar.getCssValue("letter-spacing");
			  lletterspace[9] = iaBrandinglocators.SpanishDisputecen.getCssValue("letter-spacing");
			  lletterspace[10] = iaBrandinglocators.SpanishTextBnk.getCssValue("letter-spacing");
			  lletterspace[11] = iaBrandinglocators.SpanishVirtualacct.getCssValue("letter-spacing");
			  lletterspace[12] = iaBrandinglocators.paypal.getCssValue("letter-spacing");
	//		  lletterspace[13] = iaBrandinglocators.Citipay.getCssValue("letter-spacing");
			  
			  for (int i=0; i < lletterspace.length-1; i++)
			  {
				  //convert the 0px into normal
				  if (Lletterspace.equals("0px") && lletterspace[i].equals("normal"))
				  {
					  lletterspace[i] = "0px";
				  }
				  Assert.assertEquals(Lletterspace, lletterspace[i]);
				  System.out.println("Link's letter space is matching in the UI Design:" +lletterspace);
			  }
		}

		/*Travel Services Links UI Validation */
		
		public void TSLFontSizeValidation()
		{
			String HFontvalue = redlinesconfigfilereader.getHFontvalue();
			String hfontSize[] = new String[1];
			hfontSize[0] = iaBrandinglocators.SpanishManageTravel.getCssValue("font-size");
			for (int i=0; i < hfontSize.length-1; i++)
			{
				Assert.assertEquals(HFontvalue, hfontSize);
				System.out.println("Font Size for the heading is matching in the UI Design:" +hfontSize);	
			}
		}
		
		public void TSLFontFamilyValidation()
		{
			String HFontFamily = redlinesconfigfilereader.getHFontfamily();
			String hfontFamily[] = new String[1];
			hfontFamily[0] = iaBrandinglocators.SpanishManageTravel.getCssValue("font-family");
			for (int i=0; i < hfontFamily.length-1; i++)
			{
				//Trim the Sans-serif from the font family
				String ff=hfontFamily[i];
				String hfontfamily = ff.substring(0, ff.indexOf(","));
				//Compare the trim with the redlines
				Assert.assertEquals(HFontFamily, hfontfamily);
				System.out.println("Font Family for the heading is matching in the UI Design:" +hfontfamily);
			}
		}
				
		public void TSLColorvalidation()
		{
			String HColor = redlinesconfigfilereader.getHcolor();
			String hcolor[] = new String[1];
			hcolor[0] = iaBrandinglocators.SpanishManageTravel.getCssValue("color");
			for (int i=0; i < hcolor.length-1; i++)
			{
				//Converting the RGB in Hex value
				String hex = Color.fromString(hcolor[i]).asHex();
				//Comparing the converted color into hex with the given value by design team
				Assert.assertEquals(HColor, hex);
				System.out.println("Color for the heading is matching in the UI Design:" +hex);
			}
		}
		
			
		public void TSLLineheightvalidation()
		{
			String HLineheight = redlinesconfigfilereader.getHLineheight();
			String hlineHeight[] = new String[1]; 
			hlineHeight[0] = iaBrandinglocators.SpanishManageTravel.getCssValue("line-height");			
			for (int i=0; i < hlineHeight.length-1; i++)
			{
				Assert.assertEquals(HLineheight, hlineHeight);
				System.out.println("Line height for the heading is matching in the UI Design:" +hlineHeight);
			}
		}
		
		public void TSLletterspacingvalidation()
		{
			String Hletterspace = redlinesconfigfilereader.getHletterspace();
			String hletterspace[] = new String[1];
			hletterspace[0] = iaBrandinglocators.SpanishManageTravel.getCssValue("letter-spacing");
			for (int i=0; i < hletterspace.length-1; i++)
			  {
				  //convert the 0px into normal
				  if (Hletterspace.equals("0px") && hletterspace[i].equals("normal"))
				  {
					  hletterspace[i] = "0px";
				  }
				  
				Assert.assertEquals(Hletterspace, hletterspace);
				System.out.println("Opacity for the heading is matching in the UI Design:" +hletterspace);
			  }
		}
	
		
	/* More Settings Links UI Design Validation */
		
		public void MSLFontSizeValidation()
		{
			String HFontvalue = redlinesconfigfilereader.getHFontvalue();
			String hfontSize[] = new String[7];
			hfontSize[0] = iaBrandinglocators.SpanishNickname.getCssValue("font-size");
			hfontSize[1] = iaBrandinglocators.SpanishStartorHome.getCssValue("font-size");
			hfontSize[2] = iaBrandinglocators.SpanishPINSecques.getCssValue("font-size");
			hfontSize[3] = iaBrandinglocators.SpanishPrivacy.getCssValue("font-size");
			hfontSize[4] = iaBrandinglocators.Spanishtwostep.getCssValue("font-size");
			hfontSize[5] = iaBrandinglocators.Spanishappacctaccess.getCssValue("font-size");
			hfontSize[6] = iaBrandinglocators.Spanishacctaler.getCssValue("font-size");
			
			for (int i=0; i < hfontSize.length-1; i++)
			{
				Assert.assertEquals(HFontvalue, hfontSize);
				System.out.println("Font Size for the heading is matching in the UI Design:" +hfontSize);	
			}
		}
		
		public void MSLFontFamilyValidation()
		{
			String HFontFamily = redlinesconfigfilereader.getHFontfamily();
			String hfontFamily[] = new String[7];

			hfontFamily[0] = iaBrandinglocators.SpanishNickname.getCssValue("font-family");
			hfontFamily[1] = iaBrandinglocators.SpanishStartorHome.getCssValue("font-family");
			hfontFamily[2] = iaBrandinglocators.SpanishPINSecques.getCssValue("font-family");
			hfontFamily[3] = iaBrandinglocators.SpanishPrivacy.getCssValue("font-family");
			hfontFamily[4] = iaBrandinglocators.Spanishtwostep.getCssValue("font-family");
			hfontFamily[5] = iaBrandinglocators.Spanishappacctaccess.getCssValue("font-family");
			hfontFamily[6] = iaBrandinglocators.Spanishacctaler.getCssValue("font-family");
			for (int i=0; i < hfontFamily.length-1; i++)
			{
				//Trim the Sans-serif from the font family
				String ff=hfontFamily[i];
				String hfontfamily = ff.substring(0, ff.indexOf(","));
				//Compare the trim with the redlines
				Assert.assertEquals(HFontFamily, hfontfamily);
				System.out.println("Font Family for the heading is matching in the UI Design:" +hfontfamily);
			}
		}
				
		public void MSLColorvalidation()
		{
			String HColor = redlinesconfigfilereader.getHcolor();
			String hcolor[] = new String[7];
			hcolor[0] = iaBrandinglocators.SpanishNickname.getCssValue("color");
			hcolor[1] = iaBrandinglocators.SpanishStartorHome.getCssValue("color");
			hcolor[2] = iaBrandinglocators.SpanishPINSecques.getCssValue("color");
			hcolor[3] = iaBrandinglocators.SpanishPrivacy.getCssValue("color");
			hcolor[4] = iaBrandinglocators.Spanishtwostep.getCssValue("color");
			hcolor[5] = iaBrandinglocators.Spanishappacctaccess.getCssValue("color");
			hcolor[6] = iaBrandinglocators.Spanishacctaler.getCssValue("color");
			
			for (int i=0; i < hcolor.length-1; i++)
			{
				//Converting the RGB in Hex value
				String hex = Color.fromString(hcolor[i]).asHex();
				//Comparing the converted color into hex with the given value by design team
				Assert.assertEquals(HColor, hex);
				System.out.println("Color for the heading is matching in the UI Design:" +hex);
			}
		}
		
			
		public void MSLLineheightvalidation()
		{
			String HLineheight = redlinesconfigfilereader.getHLineheight();
			String hlineHeight[] = new String[7]; 
			hlineHeight[0] = iaBrandinglocators.SpanishNickname.getCssValue("line-height");	
			hlineHeight[1] = iaBrandinglocators.SpanishStartorHome.getCssValue("line-height");
			hlineHeight[2] = iaBrandinglocators.SpanishPINSecques.getCssValue("line-height");
			hlineHeight[3] = iaBrandinglocators.SpanishPrivacy.getCssValue("line-height");
			hlineHeight[4] = iaBrandinglocators.Spanishtwostep.getCssValue("line-height");
			hlineHeight[5] = iaBrandinglocators.Spanishappacctaccess.getCssValue("line-height");
			hlineHeight[6] = iaBrandinglocators.Spanishacctaler.getCssValue("line-height");
			
			for (int i=0; i < hlineHeight.length-1; i++)
			{
				Assert.assertEquals(HLineheight, hlineHeight);
				System.out.println("Line height for the heading is matching in the UI Design:" +hlineHeight);
			}
		}
		
		public void MSLletterspacingvalidation()
		{
			String Hletterspace = redlinesconfigfilereader.getHletterspace();
			String hletterspace[] = new String[7];
			hletterspace[0] = iaBrandinglocators.SpanishNickname.getCssValue("letter-spacing");
			hletterspace[1] = iaBrandinglocators.SpanishStartorHome.getCssValue("letter-spacing");
			hletterspace[2] = iaBrandinglocators.SpanishPINSecques.getCssValue("letter-spacing");
			hletterspace[3] = iaBrandinglocators.SpanishPrivacy.getCssValue("letter-spacing");
			hletterspace[4] = iaBrandinglocators.Spanishtwostep.getCssValue("letter-spacing");
			hletterspace[5] = iaBrandinglocators.Spanishappacctaccess.getCssValue("letter-spacing");
			hletterspace[6] = iaBrandinglocators.Spanishacctaler.getCssValue("letter-spacing");
			
			for (int i=0; i < hletterspace.length-1; i++)
			  {
				  //convert the 0px into normal
				  if (Hletterspace.equals("0px") && hletterspace[i].equals("normal"))
				  {
					  hletterspace[i] = "0px";
				  }
				Assert.assertEquals(Hletterspace, hletterspace);
				System.out.println("Opacity for the heading is matching in the UI Design:" +hletterspace);
			  }
		}


}
