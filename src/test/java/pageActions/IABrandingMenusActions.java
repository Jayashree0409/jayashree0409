package pageActions;

import static org.testng.Assert.assertTrue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.support.PageFactory;

import pageLocators.CitiHomePageLocators_test;
//import pageLocators.CardBenefitsLocators;
import pageLocators.IABrandingMenusLocators;
import utils.SeleniumDriver;

import org.openqa.selenium.By;


public class IABrandingMenusActions {
		CitiHomePageLocators_test citiHomePageLocators=null;
	//	CardBenefitsLocators cardBenefitsLocators=null;
		IABrandingMenusLocators iaBrandingMenusLocators=null;
		CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
		final Log log = LogFactory.getLog(CitiHomePageActions_test.class.getName());
		
		//Constructor
		public  IABrandingMenusActions()
		{
			this.citiHomePageLocators = new CitiHomePageLocators_test();
//			this.cardBenefitsLocators = new CardBenefitsLocators();
			this.iaBrandingMenusLocators = new IABrandingMenusLocators();
			PageFactory.initElements(SeleniumDriver.getDriver(), citiHomePageLocators);
//			PageFactory.initElements(SeleniumDriver.getDriver(), cardBenefitsLocators);
			PageFactory.initElements(SeleniumDriver.getDriver(), iaBrandingMenusLocators);
		}
	
	public void VerifyCostoRewardsLinkAngular ()
	{
//		iaBrandingMenusLocators.ServicesAngular.click();
//		iaBrandingMenusLocators.CreditServiceAng.click();
//		iaBrandingMenusLocators.RewardsAngular.click();
//		iaBrandingMenusLocators.CostcoRewardsAngular.click();
//		iaBrandingMenusLocators.CostcoBanner.click();
		//cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.ServicesAngular);
		//cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.CreditServiceAng);
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.RewardsAngular);
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.CostcoRewardsAngular);
		//assertTrue(cbolCommonActions.verifyElementAvailability(iaBrandingMenusLocators.CostcoBanner));
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.CostcoBanner);
	
	}
	
	public void VerifyCostoRewardsLegacy () 
	{

		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.TermsConditionAngular);
		//cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.CreditServiceAng);
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.RewardsLegacy);
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.CostcoRewardsLegacy);
		//assertTrue(cbolCommonActions.verifyElementAvailability(iaBrandingMenusLocators.CostcoBanner));
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.CostcoBanner);
	
	}
	
	public void VerifyDoubleRewardsLegacy () 
	{
		if (isInterStatial ())
		{
			cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.NoThanks);
		}
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.TermsConditionAngular);
		//cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.CreditServiceAng);
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.RewardsLegacy);
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.DoubleRewardsLegacy);
		//assertTrue(cbolCommonActions.verifyElementAvailability(iaBrandingMenusLocators.CostcoBanner));
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.DoubleCashBanner);
	
	}
	public void VerifyDoubleRewardsLinkAngular ()
	{
		
			if (isInterStatial ())
			{
				cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.NoThanks);
			}
			
				
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.RewardsAngular);
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.DoubleRewardsAngular);
		//assertTrue(cbolCommonActions.verifyElementAvailability(iaBrandingMenusLocators.CostcoBanner));
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.DoubleCashBanner);
	
	}
	public boolean isInterStatial()
    {
	
            Boolean isPresent = SeleniumDriver.driver.findElements(By.xpath("//*[@id='cmlink_InterstitialNoThanks']")).size() > 0;
               
        	return isPresent;
    }
	
}
