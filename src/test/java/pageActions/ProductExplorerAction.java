package pageActions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import pageLocators.ProductExplorerLocators;
import pageLocators.IABrandingMenusLocators;
import utils.SeleniumDriver;

public class ProductExplorerAction {
	
	ProductExplorerLocators productexplorerlocators=null;
	CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
	IABrandingMenusLocators iaBrandingMenusLocators=null;
	final Log log = LogFactory.getLog(ProductExplorerAction.class.getName());
	
	//Constructor
	public  ProductExplorerAction()
	{
		this.productexplorerlocators = new ProductExplorerLocators();
		this.iaBrandingMenusLocators = new IABrandingMenusLocators();
		PageFactory.initElements(SeleniumDriver.getDriver(), productexplorerlocators);
		PageFactory.initElements(SeleniumDriver.getDriver(), iaBrandingMenusLocators);
	}
	
	public void ProductExplorer()
	{

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		String CurrentURL = SeleniumDriver.getCurrentURL();
		System.out.println(CurrentURL);
		if(CurrentURL.contains("/ag"))
		{
			cbolCommonActions.CBOLPerformClick(productexplorerlocators.AGProductExplorerIcon);
		}
		else
		{
		cbolCommonActions.CBOLPerformClick(productexplorerlocators.ProductExplorerIcon);
		}
	}
	
	public void productexplorerfontsize()
	{
		String fontSize = productexplorerlocators.ProductExplorerIcon.getCssValue("font-size");
	    System.out.println("Font Size -> "+fontSize);
	}
	
	public  void CreditCard() 
	{	

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		cbolCommonActions.CBOLPerformClick(productexplorerlocators.CreditCards);
	}
	
	
	public  void Lending() 
	{	

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		cbolCommonActions.CBOLPerformClick(productexplorerlocators.Lending);
	}

	public  void citigold() 
	{	

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		cbolCommonActions.CBOLPerformClick(productexplorerlocators.Citigold);
	}
	
	public  void Banking() 
	{	

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		cbolCommonActions.CBOLPerformClick(productexplorerlocators.Banking);
	}
	
	public  void Investing() 
	{	

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		cbolCommonActions.CBOLPerformClick(productexplorerlocators.Investing);
	}
	
	
	public void BankingServices()
		{
		
		if (isSpanishBS ())
			{

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
				cbolCommonActions.CBOLPerformClick(productexplorerlocators.BankingServices);
				System.out.println("English Banking Services Element is present");
			}
	
		else
			{

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
				cbolCommonActions.CBOLPerformClick(productexplorerlocators.SpanishBankingServices);
				System.out.println("Spanish Banking Services Element is present");
			}
	}
	
	
	public void UpgradeCitiPriority()
		{
		
			if (isSpanishUCP ())
				{

				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
					cbolCommonActions.CBOLPerformClick(productexplorerlocators.UpgradeCitiPriority);
					System.out.println("English Upgrade CitiPriority Element is present");
				}

			else
				{

				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
					cbolCommonActions.CBOLPerformClick(productexplorerlocators.SpanishUpgradeCitiPriority);
					System.out.println("Spanish Upgrade CitiPriority Element is present");
				}
		}

			
	public void StatementsandDocuments()
	
	{
		if (isSpanishSD ())
			{

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(productexplorerlocators.StatementsandDocuments);
				System.out.println("English Statements & Documents Element is present");
			}

		else
			{

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(productexplorerlocators.SpanishStatementsandDocuments);
				System.out.println("Spanish Statements & Documents Element is present");
			}
	}
	

	public void PaymentsandTransfers()
	{

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		cbolCommonActions.CBOLPerformClick(productexplorerlocators.PaymentsandTransfers);
	}
	
	public void AnyExternalAccounts()
		{
		if (isSpanishAE ())
			{

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
				cbolCommonActions.CBOLPerformClick(productexplorerlocators.AnyExternalAccount);
				System.out.println("English Any External Account Element is present");
			}

		else
			{

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
				cbolCommonActions.CBOLPerformClick(productexplorerlocators.SpanishAnyExternalAccount);
				System.out.println("Spanish Any External Account Element is present");
			}
	}
	
	public  void OpinionLab() throws InterruptedException
	{

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		cbolCommonActions.CBOLPerformClick(productexplorerlocators.Feedbackicon);
		Thread.sleep(2000);
	}
	
	public void ModelClose()
	{
		//Actions actions = new Actions(SeleniumDriver.getDriver());
		
		if (ismodel ())
			{
			   cbolCommonActions.CBOLPerformClick(productexplorerlocators.closeicon);
			}
		
		else 
		{
			System.out.println("Element not present");
		}
	}
	
	public void RewardsandBenefits()
	{

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		cbolCommonActions.CBOLPerformClick(productexplorerlocators.RewardsandBenefits);
	}
	
	public void CardBenefits()
	{
	
		if (isSpanishCB ())
			{

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}	
				cbolCommonActions.CBOLPerformClick(productexplorerlocators.CardBenefits);
				System.out.println("English Card Benefits Element is present");
			}
	
		else
			{
				

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
				cbolCommonActions.CBOLPerformClick(productexplorerlocators.SpanishCardBenefits);
				System.out.println("Spanish Card Benefits Element is present");
			}
	}
	
	
   public void profile_icon()
   {

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	   cbolCommonActions.CBOLPerformClick(productexplorerlocators.profiles);
   }
   
   public void contact_information()
   {

		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	   cbolCommonActions.CBOLPerformClick(productexplorerlocators.ContactInformation);
   }
		
   public void Email_edit()
   {

		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	   cbolCommonActions.CBOLPerformClick(productexplorerlocators.EmailEdit);
   }
   
   public void Email_Preference()
   {

		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	   cbolCommonActions.CBOLPerformClick(productexplorerlocators.EmailPreference);
   }
   
   public void SpanishLanguage()
   {
	   cbolCommonActions.CBOLPerformClick(productexplorerlocators.LanguageSpanish);
   }
   
   public boolean ismodel() 
   {
	   Boolean isPresent = SeleniumDriver.driver.findElements(By.id("modal-titleid")).size() > 0;
	  
	   return isPresent;
	   
   }
   
	public void VerifyDoubleRewardsLegacy () 
	{
		if (isInterStatial ())
		{
			cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.NoThanks);
		}
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.TermsConditionAngular);
		//cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.CreditServiceAng);
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.RewardsLegacy);
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.DoubleRewardsLegacy);
		//assertTrue(cbolCommonActions.verifyElementAvailability(iaBrandingMenusLocators.CostcoBanner));
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.DoubleCashBanner);
	
	}
	public void VerifyDoubleRewardsLinkAngular ()
	{
		
			if (isInterStatial ())
			{
				cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.NoThanks);
			}
			
				
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.RewardsAngular);
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.DoubleRewardsAngular);
		//assertTrue(cbolCommonActions.verifyElementAvailability(iaBrandingMenusLocators.CostcoBanner));
		cbolCommonActions.CBOLPerformClick(iaBrandingMenusLocators.DoubleCashBanner);
	
	}
	public boolean isInterStatial()
   {
	
           Boolean isPresent = SeleniumDriver.driver.findElements(By.xpath("//*[@id='cmlink_InterstitialNoThanks']")).size() > 0;
              
       	return isPresent;
   }
	
  public boolean isSpanishCB()
  {
	  Boolean isPresent = SeleniumDriver.driver.findElements(By.linkText("Card Benefits")).size() > 0;
  
	  return isPresent;
  }
  
  public boolean isSpanishBS()
  {
	  Boolean isPresent = SeleniumDriver.driver.findElements(By.linkText("Banking Services")).size() > 0;
	  
	  return isPresent;
  }
  
  public boolean isSpanishUCP()
  {
	  Boolean isPresent = SeleniumDriver.driver.findElements(By.linkText("Request Upgrade to Citi Priority")).size() > 0;
	  
	  return isPresent;
  }
  
  public boolean isSpanishSD()
  {
	  Boolean isPresent = SeleniumDriver.driver.findElements(By.linkText("Statements & Documents")).size() > 0;
	  
	  return isPresent;
  }
  
  public boolean isSpanishAE()
  {
	  Boolean isPresent = SeleniumDriver.driver.findElements(By.linkText("Any External Account")).size() > 0;
	  
	  return isPresent;
  }
  
}
