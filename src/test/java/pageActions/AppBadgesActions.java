package pageActions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import pageLocators.AppBadgesLocators;
import pageLocators.IABrandingMenusLocators;
import pageLocators.ProductExplorerLocators;
import utils.SeleniumDriver;

public class AppBadgesActions {
	
	AppBadgesLocators appbadgeslocators=null;
	CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
	final Log log = LogFactory.getLog(ProductExplorerAction.class.getName());

	//Constructor
	public AppBadgesActions()
	{
		this.appbadgeslocators = new AppBadgesLocators();
		PageFactory.initElements(SeleniumDriver.getDriver(), appbadgeslocators);
	}
	
	
	public void iOSAppbadge()
	{
		cbolCommonActions.CBOLPerformClick(appbadgeslocators.iOSAppBadge);
	}
	
	public void PostloginiOSAppbadge()
	{
		cbolCommonActions.CBOLPerformClick(appbadgeslocators.PostLoginiOSAppBadge);
	}
	
	public void AndroidAppbadge()
	{
		cbolCommonActions.CBOLPerformClick(appbadgeslocators.AndroidAppBadge);
	}
	
	public void PostloginAndroidAppbadge()
	{
		cbolCommonActions.CBOLPerformClick(appbadgeslocators.PostLoginAndroidAppBadge);
	}
	
	
	public void Continuebutton() 
	{
		cbolCommonActions.CBOLPerformClick(appbadgeslocators.ContinueButton);
	
	}
	
	public void PostloginContinuebutton() 
	{
		cbolCommonActions.CBOLPerformClick(appbadgeslocators.PostloginContinueButton);
				
	}
	
	public void Speedbump()
	{
		//Actions actions = new Actions(SeleniumDriver.getDriver());
		
		if (isspeedbump())
			{
			   cbolCommonActions.CBOLPerformClick(appbadgeslocators.Speedbump);
			   try {
					Thread.sleep(25000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		
		else 
		{
			System.out.println("Element not present");
			   try {
					Thread.sleep(25000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
	}
	
	public void Speedbumppostlogin()
	{
		//Actions actions = new Actions(SeleniumDriver.getDriver());
		
		if (isspeedbumppostlogin())
			{
			   cbolCommonActions.CBOLPerformClick(appbadgeslocators.PostloginSpeedbump);
			   try {
					Thread.sleep(25000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		
		else 
		{
			System.out.println("Element not present");
			   try {
					Thread.sleep(25000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
	}
	
	public void ATMBranch()
	{
		if (isSpanishATM ())
			{
				cbolCommonActions.CBOLPerformClick(appbadgeslocators.ATMBranch);
				System.out.println("English ATM Branch Element is present");
			}
		else
			{
				cbolCommonActions.CBOLPerformClick(appbadgeslocators.SpanishATMBranch);
				System.out.println("Spanish ATM Branch Element is present");
			}
		 
	}
	
	
	public void OpenAccount()
	{
		if (isSpanishOA ())
			{
				cbolCommonActions.CBOLPerformClick(appbadgeslocators.OpenAccount);
				System.out.println("English Open An Account Element is present");
			}
		else
			{
				cbolCommonActions.CBOLPerformClick(appbadgeslocators.SpanishOpenAccount);
				System.out.println("Spanish Open An Account Element is present");
			}
		 
	}
	
	public void contactus()
	{
		if (isSpanishCT ())
			{
				cbolCommonActions.CBOLPerformClick(appbadgeslocators.ContactUs);
				System.out.println("English Contact us Element is present");
			}
		else
			{
				cbolCommonActions.CBOLPerformClick(appbadgeslocators.SpanishContactUs);
				System.out.println("Spanish Contact us Element is present");
			}
		 
	}

	   public boolean isspeedbump() 
	   {
		   Boolean isPresent = SeleniumDriver.driver.findElements(By.className("modal-content")).size() > 0;
		  
		   return isPresent;
		   
	   }
	   
	   public boolean isspeedbumppostlogin()
	   {
		   Boolean isPresent = SeleniumDriver.driver.findElements(By.className("speedbump-dialog")).size() > 0;
		   
		   return isPresent;
	   }
	
	   public boolean isSpanishATM()
	   {
		   Boolean isPresent = SeleniumDriver.driver.findElements(By.linkText("ATM/Branch")).size() > 0;
		   
		   return isPresent;
	   }
	   
	   public boolean isSpanishOA()
	   {
		   Boolean isPresent = SeleniumDriver.driver.findElements(By.linkText("Open an Account")).size() > 0;
		   
		   return isPresent;
	   }
	   
	   public boolean isSpanishCT()
	   {
		   Boolean isPresent = SeleniumDriver.driver.findElements(By.linkText("Contact Us")).size() > 0;
		   
		   return isPresent;
	   }
}
