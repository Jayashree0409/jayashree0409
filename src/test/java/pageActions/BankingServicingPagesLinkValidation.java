package pageActions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import pageLocators.IABrandingUIDesignLocator;
import utils.SeleniumDriver;
import pageLocators.IABrandingLocators;
import utils.ServiceLinksConfigFileReader;


public class BankingServicingPagesLinkValidation {
		
		CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
		IABrandingUIDesignLocator iaBrandinguidesignLocators=null;
		ServiceLinksConfigFileReader servicelinksconfigfilereader=null;
		IABrandingLocators iaBrandinglocators=null;
		final Log log = LogFactory.getLog(BankingServicingPagesLinkValidation.class.getName());
	
		//Constructor
		public  BankingServicingPagesLinkValidation()
		{
			this.iaBrandinguidesignLocators = new IABrandingUIDesignLocator();
			this.servicelinksconfigfilereader = new ServiceLinksConfigFileReader();
			this.iaBrandinglocators = new IABrandingLocators();
			PageFactory.initElements(SeleniumDriver.getDriver(), iaBrandinguidesignLocators);
			PageFactory.initElements(SeleniumDriver.getDriver(), servicelinksconfigfilereader);
			PageFactory.initElements(SeleniumDriver.getDriver(), iaBrandinglocators);
		}
		
		public void services()
		{
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("/ag")) 
			{
			cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.AGServices);
			}
			else
			{
				cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.Services);
			}
			
		}
		
		
		public void profiles()
		{
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("/ag")) 
			{
			cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.AGprofiles);
			}
			else
			{
				cbolCommonActions.CBOLPerformClick(iaBrandinguidesignLocators.profiles);
			}
			
		}
			
		public void BankingServices()
		{
		
			try 
			{
				Thread.sleep(2000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}	
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.BankingServices);
		}
		
		public void CreditCardServices()
		{
			try 
			{
				Thread.sleep(2000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Creditcardservices);
		}
		
		public void StatementsandDocuments()
		{
			try
			{
				Thread.sleep(2000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.StatementsandDocuments);
		}
		
		public void TravelServices()
		{
			try
			{
				Thread.sleep(2000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Travelservice);
		}
		
		public void Moresettings()
		{
			try
			{
				Thread.sleep(2000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Moresettings);
		}
		
	/* Banking Service Links Validation */
		
		public void ResetATMPINValidation()
		{
			String currenturl="";
			String LResetATMPIN = servicelinksconfigfilereader.getResetATMPIN();
			System.out.println(LResetATMPIN);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.ResetATMPIN);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LResetATMPIN,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LResetATMPIN,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LResetATMPIN,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}
	
		}
		
		public void LockcardValidation()
		{
			String currenturl="";
			String LLockcard = servicelinksconfigfilereader.getLockcard();
			System.out.println(LLockcard);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.LockCard);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LLockcard,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LLockcard,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LLockcard,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}

		}
		
		public void ReplaceATMCardValidation()
		{
			String currenturl="";
			String LReplaceATMCard = servicelinksconfigfilereader.getReplaceATMCard();
			System.out.println(LReplaceATMCard);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.ReplaceATMcard);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LReplaceATMCard,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LReplaceATMCard,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LReplaceATMCard,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}

		}
		
		public void ChangeyourATMCardtoaDebitCardValidation()
		{
			String currenturl="";
			String LChangeyourATMCardtoaDebitCard = servicelinksconfigfilereader.getChangeyourATMCardtoaDebitCard();
			System.out.println(LChangeyourATMCardtoaDebitCard);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.ChangeATMCard);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LChangeyourATMCardtoaDebitCard,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LChangeyourATMCardtoaDebitCard,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LChangeyourATMCardtoaDebitCard,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}

		}
		
		public void StopCheckPaymentValidation()
		{
			String currenturl="";
			String LStopCheckPayment = servicelinksconfigfilereader.getStopCheckPayment();
			System.out.println(LStopCheckPayment);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.StopCheck);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LStopCheckPayment,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LStopCheckPayment,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LStopCheckPayment,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}

		}
		
		public void ReorderPersonalChecksValidation()
		{
			String currenturl="";
			String LReorderPersonalChecks = servicelinksconfigfilereader.getReorderPersonalChecks();
			System.out.println(LReorderPersonalChecks);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Reordercheck);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LReorderPersonalChecks,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LReorderPersonalChecks,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LReorderPersonalChecks,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}

		}
		
		public void TextBankingValidation()
		{
			String currenturl="";
			String LTextBanking = servicelinksconfigfilereader.getTextBanking();
			System.out.println(LTextBanking);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Textbank);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN")) 
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LTextBanking,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LTextBanking,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LTextBanking,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}
			
		}
		
				
		public void TelephoneAccessCodeValidation()
		{
			String currenturl="";
			String LTelephoneAccessCode = servicelinksconfigfilereader.getTelephoneAccessCode();
			System.out.println(LTelephoneAccessCode);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.TeleAccesscode);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN")) 
				{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LTelephoneAccessCode,currenturl);
				System.out.println("Links match as per the requirement for enter");
				}
			else if(CurrentURL.contains("?JFP_TOKEN"))
				{
					System.out.println("enter1");
					currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
					Assert.assertEquals(LTelephoneAccessCode,currenturl);
					System.out.println("Links match as per the requirement for enter1");
				}
			
			else
			{
				System.out.println("end");
				Assert.assertEquals(LTelephoneAccessCode,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}

		}
		
		public void LinkUnlinkCitiAccountsValidation()
		{
			String currenturl="";
			String LLinkUnlinkCitiAccounts = servicelinksconfigfilereader.getLinkUnlinkCitiAccounts();
			System.out.println(LLinkUnlinkCitiAccounts);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Linkunlinkcitiac);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN")) 
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LLinkUnlinkCitiAccounts,currenturl);
				System.out.println("Links match as per the requirement for Enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?")); 
				Assert.assertEquals(LLinkUnlinkCitiAccounts,currenturl);
				System.out.println("Links match as per the requirement for Enter 1");
			}
			else
			{
				System.out.println("End");
				Assert.assertEquals(LLinkUnlinkCitiAccounts,CurrentURL);
				System.out.println("Links match as per the requirement for End");
			}
		}
		
		public void LinkUnlinkSavingsforOverdraftProtectionValidation()
		{
			String currenturl="";
			String LLinkUnlinkSavingsforOverdraftProtection = servicelinksconfigfilereader.getLinkUnlinkSavingsforOverdraftProtection();
			System.out.println(LLinkUnlinkSavingsforOverdraftProtection);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.LinkorUnlinksaving);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LLinkUnlinkSavingsforOverdraftProtection,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LLinkUnlinkSavingsforOverdraftProtection,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LLinkUnlinkSavingsforOverdraftProtection,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}

		}
	
		
		public void RequestUpgradetoCitiPriorityValidation()
		{
			String currenturl="";
			String LRequestUpgradetoCitiPriority = servicelinksconfigfilereader.getRequestUpgradetoCitiPriority();
			System.out.println(LRequestUpgradetoCitiPriority);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.UpgradeCitiPriority);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LRequestUpgradetoCitiPriority,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LRequestUpgradetoCitiPriority,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LRequestUpgradetoCitiPriority,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}

		}
		
		public void RequestUpgradetoCitigoldValidation()
		{
			String currenturl="";
			String LRequestUpgradetoCitigold = servicelinksconfigfilereader.getRequestUpgradetoCitigold();
			System.out.println(LRequestUpgradetoCitigold);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.UpgradeCitigold);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LRequestUpgradetoCitigold,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LRequestUpgradetoCitigold,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LRequestUpgradetoCitigold,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}
		}
		
		public void LinkCitiAccountsfromOtherCountriesValidation()
		{
			String currenturl="";
			String LLinkCitiAccountsfromOtherCountries = servicelinksconfigfilereader.getLinkCitiAccountsfromOtherCountries();
			System.out.println(LLinkCitiAccountsfromOtherCountries);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.LinkCitiAccount);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LLinkCitiAccountsfromOtherCountries,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LLinkCitiAccountsfromOtherCountries,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LLinkCitiAccountsfromOtherCountries,CurrentURL);
				System.out.println("Links match as per the requirement");
			}
		}
		
		public void FinancialToolsValidation()
		{
			String currenturl="";
			String LFinancialTools = servicelinksconfigfilereader.getFinancialTools();
			System.out.println(LFinancialTools);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.FinanceTool);
					
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LFinancialTools,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LFinancialTools,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}

			else
			{
				System.out.println("end");
				Assert.assertEquals(LFinancialTools,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}
		}
		
		public void InvestmentsValidation()
		{
			String currenturl="";
			String LInvestments = servicelinksconfigfilereader.getInvestments();
			System.out.println(LInvestments);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Invest);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LInvestments,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LInvestments,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LInvestments,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}
		}
		
		public void ScheduleanAppointmentValidation()
		{
			String currenturl="";
			String LScheduleanAppointment = servicelinksconfigfilereader.getScheduleanAppointment();
			System.out.println(LScheduleanAppointment);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.Scheduleappt);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LScheduleanAppointment,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LScheduleanAppointment,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LScheduleanAppointment,CurrentURL);
				System.out.println("Links match as per the requirement");
			}
		}
		
		public void ManageAppointmentsValidation()
		{
			String currenturl="";
			String LManageAppointments = servicelinksconfigfilereader.getManageAppointments();
			System.out.println(LManageAppointments);
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			cbolCommonActions.CBOLPerformClick(iaBrandinglocators.ManageAppt);
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			if(CurrentURL.contains("&JFP_TOKEN"))
			{
				System.out.println("enter");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("&"));
				Assert.assertEquals(LManageAppointments,currenturl);
				System.out.println("Links match as per the requirement for enter");
			}
			else if(CurrentURL.contains("?JFP_TOKEN"))
			{
				System.out.println("enter1");
				currenturl = CurrentURL.substring(0, CurrentURL.indexOf("?"));
				Assert.assertEquals(LManageAppointments,currenturl);
				System.out.println("Links match as per the requirement for enter1");
			}
			else
			{
				System.out.println("end");
				Assert.assertEquals(LManageAppointments,CurrentURL);
				System.out.println("Links match as per the requirement for end");
			}
		}
		
		

	   public boolean islinkpresent() 
	   
	   {
		   Boolean isPresent = SeleniumDriver.driver.findElements(By.linkText("Language Preferences")).size() > 0;
		  
		   return isPresent;
		   
	   }
}
