package pageActions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
//import pageLocators.CardBenefitsLocators;
import pageLocators.CitiHomePageLocators_test;
import pageLocators.IABrandingMenusLocators;
import pageLocators.ProductExplorerLocators;
//import pageLocators.IABrandingUIDesignLocator;
import pageLocators.IABrandingLocators;
import utils.SeleniumDriver;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class BrandingMenuNavigation {
	
	CitiHomePageLocators_test citiHomePageLocators=null;
//	CardBenefitsLocators cardBenefitsLocators=null;
	IABrandingMenusLocators iaBrandingMenusLocators=null;
	CBOL_CommonActions_test cbolCommonActions=null;
	ProductExplorerLocators productExplorerLocators=null;
//	IABrandingUIDesignLocator IABrandingUIdesignLocator=null;
	IABrandingLocators IAbrandingLocators=null;
	
	final Log log = LogFactory.getLog(BrandingMenuNavigation.class.getName());
	public List<WebElement> elements;
	public int size;
	
	//Constructor
		public BrandingMenuNavigation()
		{
			this.citiHomePageLocators = new CitiHomePageLocators_test();
			this.iaBrandingMenusLocators = new IABrandingMenusLocators();
	//		this.IABrandingUIdesignLocator = new IABrandingUIDesignLocator();
//			this.cardBenefitsLocators = new CardBenefitsLocators();
			this.cbolCommonActions = new CBOL_CommonActions_test();
			this.productExplorerLocators = new ProductExplorerLocators();
			this.IAbrandingLocators = new IABrandingLocators();
			PageFactory.initElements(SeleniumDriver.getDriver(), citiHomePageLocators);
			PageFactory.initElements(SeleniumDriver.getDriver(), iaBrandingMenusLocators);
//			PageFactory.initElements(SeleniumDriver.getDriver(), IABrandingUIdesignLocator);
			PageFactory.initElements(SeleniumDriver.getDriver(), cbolCommonActions);
			PageFactory.initElements(SeleniumDriver.getDriver(), productExplorerLocators);
			PageFactory.initElements(SeleniumDriver.getDriver(), IAbrandingLocators);
		}
		
					
		//String CurrentURL = SeleniumDriver.getCurrentURL();
		//	System.out.println("CurrentURL");
		
		//	if(CurrentURL.contains("/ag")) 
	//		{
	//			
				
	//		}
	//	}
		
		public void paymentsandtransfer()
		{
			try {
				Thread.sleep(25000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String CurrentURL = SeleniumDriver.getCurrentURL();
			System.out.println(CurrentURL);
			if(CurrentURL.contains("/ag")) 
			{
				cbolCommonActions.CBOLPerformClick(IAbrandingLocators.AGPaymentsandTransfers);
			}
			
			else
			{
				cbolCommonActions.CBOLPerformClick(IAbrandingLocators.PaymentsandTransfers);
			}
		}
	
		
		public void profiles()
		{
			try {
				Thread.sleep(25000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String CurrentURL = SeleniumDriver.getCurrentURL();
			System.out.println(CurrentURL);
			if(CurrentURL.contains("/ag"))
			{
				cbolCommonActions.CBOLPerformClick(IAbrandingLocators.AGprofiles);
			}
			else
				
			{
			cbolCommonActions.CBOLPerformClick(IAbrandingLocators.profiles);
			}
		}
		
		public void Accounts()
		{
			try {
				Thread.sleep(25000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			String CurrentURL = SeleniumDriver.getCurrentURL();
			System.out.println(CurrentURL);
			if(CurrentURL.contains("/ag")) 
			{
				cbolCommonActions.CBOLPerformClick(IAbrandingLocators.AGAccounts);
			}
			else
			{
				cbolCommonActions.CBOLPerformClick(IAbrandingLocators.Accounts);
			}
		}

		public void ServicesBrandingMenu() throws InterruptedException
		{
		//	WebElement Service = SeleniumDriver.driver.findElement(By.id("services"));
		//	WebElement PaymentsandTransfer = SeleniumDriver.driver.findElement(By.id("paymentsandTransfers"));
			
			String CurrentURL = SeleniumDriver.getCurrentURL();
			System.out.println(CurrentURL);
			if(CurrentURL.contains("/ag"))
			{
				cbolCommonActions.CBOLPerformClick(IAbrandingLocators.AGServices);
				Thread.sleep(25000);
				elements = SeleniumDriver.driver.findElements(By.id("servicesMainLI"));
				for (WebElement element : elements) {
					System.out.println(element.getText());
							
				}
			}
			else
			{
			cbolCommonActions.CBOLPerformClick(IAbrandingLocators.Services);
			Thread.sleep(25000);
			elements = SeleniumDriver.driver.findElements(By.id("services"));
			for (WebElement element : elements) {
				System.out.println(element.getText());
						
				}
			}
		}
		
		public void PaymentsandTransferMenu() throws InterruptedException
		{
			String CurrentURL = SeleniumDriver.getCurrentURL();
			System.out.println(CurrentURL);
			if(CurrentURL.contains("/ag"))
			{
				cbolCommonActions.CBOLPerformClick(IAbrandingLocators.AGPaymentsandTransfers);
				Thread.sleep(25000);
				elements = SeleniumDriver.driver.findElements(By.id("pntMainLI"));
				for (WebElement element : elements) {
					System.out.println(element.getText());
				}
			}
			else
			{
			cbolCommonActions.CBOLPerformClick(IAbrandingLocators.PaymentsandTransfers);
			Thread.sleep(25000);
			elements = SeleniumDriver.driver.findElements(By.id("paymentsandTransfers"));
			for (WebElement element : elements) {
				System.out.println(element.getText());
				}
			}
		}	
			
		public void RewardsMenu() throws InterruptedException
		{
			String CurrentURL = SeleniumDriver.getCurrentURL();
			System.out.println(CurrentURL);
			if(CurrentURL.contains("/ag"))
			{
				cbolCommonActions.CBOLPerformClick(IAbrandingLocators.AGRewardsandBenefits);
				Thread.sleep(25000);
				elements = SeleniumDriver.driver.findElements(By.id("rewardsMainLI"));
				for (WebElement element : elements) {
					System.out.println(element.getText());
				}
			}
			else
			{
			cbolCommonActions.CBOLPerformClick(IAbrandingLocators.RewardsandBenefits);
			Thread.sleep(25000);
			elements = SeleniumDriver.driver.findElements(By.id("rewards"));
			for (WebElement element : elements) {
				System.out.println(element.getText());
				}
			}
		}
		

		public void ProfilesMenu() throws InterruptedException
		{
			String CurrentURL = SeleniumDriver.getCurrentURL();
			System.out.println(CurrentURL);
			if(CurrentURL.contains("/ag"))
			{
				cbolCommonActions.CBOLPerformClick(IAbrandingLocators.AGprofiles);
				Thread.sleep(25000);
				elements = SeleniumDriver.driver.findElements(By.id("profilesMainLI"));
				for (WebElement element : elements) {
					System.out.println(element.getText());
				}
			}
			else
			{
			cbolCommonActions.CBOLPerformClick(IAbrandingLocators.profiles);
			Thread.sleep(25000);
			elements = SeleniumDriver.driver.findElements(By.id("profileSettings"));
			for (WebElement element : elements) {
				System.out.println(element.getText());
				}
			}
		}
		
		public void AccountsMenu() throws InterruptedException
		{
			String CurrentURL = SeleniumDriver.getCurrentURL();
			System.out.println(CurrentURL);
			if(CurrentURL.contains("/ag"))
			{
				cbolCommonActions.CBOLPerformClick(IAbrandingLocators.AGAccounts);
				Thread.sleep(25000);
				elements = SeleniumDriver.driver.findElements(By.id("accountsMainLI"));
				for (WebElement element : elements) {
					System.out.println(element.getText());
				}
			}
			else
			{
				
			cbolCommonActions.CBOLPerformClick(IAbrandingLocators.Accounts);
			Thread.sleep(25000);
			elements = SeleniumDriver.driver.findElements(By.id("topNavAccounts"));
			for (WebElement element : elements) {
				System.out.println(element.getText());
			}
		}
	}
		
}
