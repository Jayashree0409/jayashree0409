package pageActions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.support.PageFactory;

import pageLocators.CitiHomePageLocators_test;
import pageLocators.CardBenefitsLocators;
import utils.SeleniumDriver;
import org.openqa.selenium.By;


public class CardBenfitsAction {
		CitiHomePageLocators_test citiHomePageLocators=null;
		CardBenefitsLocators cardBenefitsLocators=null;
		CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
		final Log log = LogFactory.getLog(CitiHomePageActions_test.class.getName());
		
		//Constructor
		public  CardBenfitsAction()
		{
			this.citiHomePageLocators = new CitiHomePageLocators_test();
			this.cardBenefitsLocators = new CardBenefitsLocators();
			PageFactory.initElements(SeleniumDriver.getDriver(), citiHomePageLocators);
			PageFactory.initElements(SeleniumDriver.getDriver(), cardBenefitsLocators);
		}
		
		public void enterUID(String UID, String Pwd)
		{
			log.info(">> Sign On - Start");
		
			//cbolCommonActions.CBOLPerformClick(citiHomePageLocators.loginUIDDiv);
			
			log.info(">> Entering User ID");
			cbolCommonActions.CBOLPerformClick(citiHomePageLocators.loginDiv);
			cbolCommonActions.CBOLPerformClick(citiHomePageLocators.loginUID);
			cbolCommonActions.CBOLPerformSendKeys(citiHomePageLocators.loginUID, UID);

			log.info(">> Entering Password");
			cbolCommonActions.CBOLPerformClick(citiHomePageLocators.loginPWD);
			cbolCommonActions.CBOLPerformSendKeys(citiHomePageLocators.loginPWD, Pwd);;
			
		
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			log.info(">> Clicking Sign on Button");
			cbolCommonActions.CBOLPerformClick(citiHomePageLocators.SignOnBtn);
			
			log.info(">> Sign On - End");
		}
		
		public void SignOnCBOL()
		{
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}


	
	public void ClikCreditAccountProtectionFICOScore ()
	{
		
		cbolCommonActions.CBOLPerformClick(cardBenefitsLocators.CreditAcccountProtection);
		if (isFeedBackPopUpPresent ())
		{
			cbolCommonActions.CBOLPerformClick(cardBenefitsLocators.CancelFeedback);
		}
		cbolCommonActions.CBOLPerformClick(cardBenefitsLocators.FICOscore);
				
	}
	public void ClickSignOnCardfBenefits()
	{
		if (isFeedBackPopUpPresent ())
		{
			cbolCommonActions.CBOLPerformClick(cardBenefitsLocators.CancelFeedback);
		}
		cbolCommonActions.CBOLPerformClick(cardBenefitsLocators.SignOnCardBenefits);
	}
	public void VerifyFicoScore()
	{
		Integer.parseInt(cardBenefitsLocators.FicoScoreCount.getText())	;
		
	}
	
	public boolean isFeedBackPopUpPresent()
    {
	
            Boolean isPresent = SeleniumDriver.driver.findElements(By.xpath("//*[@id='acsMainInvite']/div/a[1]")).size() > 0;
               
        	return isPresent;
    }

	public void ClickEspanol()
	{
		cbolCommonActions.CBOLPerformClick(cardBenefitsLocators.LanguageEspanol);	
	}
	
	
	

}