package stepDef;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pageActions.CBOL_CommonActions_test;
import pageActions.CitiHomePageActions_test;
import pageActions.IABrandingUIDesignActions;
import pageActions.ProductExplorerAction;
//import pageActions.IABrandingUIDesignActions;
//import pageActions.IABrandingUISpanishDesignActions;
import pageActions.BrandingMenuNavigation;
import utils.ConfigFileReader;
import utils.SeleniumDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class BrandingMenu {
	
		final Log log = LogFactory.getLog(BrandingMenu.class.getName());
		CitiHomePageActions_test citiHomePageActions = new CitiHomePageActions_test();
		CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
		BrandingMenuNavigation brandingMenuNavigation = new BrandingMenuNavigation();
		ProductExplorerAction productexploreraction = new ProductExplorerAction(); 
		IABrandingUIDesignActions iaBrandingUIDesignActions = new IABrandingUIDesignActions();
		ConfigFileReader configFileReader;

		@Then("^I see the submenu for services menu$")
		public void i_see_the_submenu_for_services_menu() throws InterruptedException {
			brandingMenuNavigation.ServicesBrandingMenu();
		}
		
		@And("^I click the Payments and Transfer$")
		public void i_click_the_Payments_and_Transfer() {
			brandingMenuNavigation.paymentsandtransfer();
		}
		@Then("^I see the submenu for Payments and Transfer menu$")
		public void i_see_the_submenu_for_Payments_and_Transfer_menu() throws InterruptedException {
			brandingMenuNavigation.PaymentsandTransferMenu();
		}
		
		@And("^I click the Accounts$")
		public void i_click_the_Accounts() {
			brandingMenuNavigation.Accounts();
		}
		
		@Then("^I see the submenu for Accounts menu$")
		public void i_see_the_submenu_for_Accounts_menu() throws InterruptedException {
			brandingMenuNavigation.AccountsMenu();
		}
		
		@And("^Click on profile menu$")
		public void click_on_profile_menu() {
			brandingMenuNavigation.profiles();
		}
		
		@And("^I click the Services$")
		public void I_click_the_Services() {
			iaBrandingUIDesignActions.services();
		}
		
		@Then("^I see the submenu for Rewards and Benefits menu$")
		public void i_see_the_submenu_for_Rewards_and_Benefits_menu() throws InterruptedException {
			brandingMenuNavigation.RewardsMenu();
		}
		
		@Then("^I see the submenu for profiles menu$")
		public void i_see_the_submenu_for_profiles_menu() throws InterruptedException {
			brandingMenuNavigation.ProfilesMenu();
		}
}
