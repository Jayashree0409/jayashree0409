package stepDef;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;


import pageActions.CBOLPostLoginPageActions_test;
import pageActions.CBOL_CommonActions_test;
import pageActions.CitiHomePageActions_test;
import pageActions.IABrandingMenusActions;
import pageActions.IABrandingUIDesignActions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.ConfigFileReader;
import utils.ExcelUtils;
import utils.SeleniumDriver;
import utils.SeleniumHelper;




public class Decommission_acct_mgmt_hub {
	
	final Log log = LogFactory.getLog(IABranding.class.getName());

	IABrandingMenusActions iaBrandingMenusActions = new IABrandingMenusActions();
	CitiHomePageActions_test citiHomePageActions = new CitiHomePageActions_test();
	CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
	IABrandingUIDesignActions iaBrandingUIDesignActions = new IABrandingUIDesignActions();
	ConfigFileReader configFileReader;
	//String tempURL;
	//public XSSFRow row;
	public Decommission_acct_mgmt_hub() {
		
	}
	
	@And("^I click the User ID link$")
	public void I_click_the_User_ID_link() throws Throwable {
		iaBrandingUIDesignActions.user_id();
	    // Write code here that turns the phrase above into concrete actions
	}
	
	@And("^I enter new User ID$")
	public void I_enter_new_User_ID() throws Throwable {
		iaBrandingUIDesignActions.useridinputfield();
	}
	
	@And("^I enter revert User ID$")
	public void I_enter_revert_User_ID() throws Throwable {
		iaBrandingUIDesignActions.revertuseridinputfield();
	}
	
	@And("^I enter Current password$")
	public void I_enter_Current_password() throws Throwable {
		iaBrandingUIDesignActions.passwordinputfield();
	}
	
	@And("^I click Change button$")
	public void I_click_Change_button() throws Throwable {
		iaBrandingUIDesignActions.decommission_acct_mgmt_hub_Change();
	}
	
	@And("^I click Continue button$")
	public void I_click_Continue_button() throws Throwable {
		iaBrandingUIDesignActions.decommission_acct_mgmt_hub_Continue();
	}
	
	@And("^I click Cancel link$")
	public void I_click_Cancel_link() throws Throwable {
		iaBrandingUIDesignActions.decommission_acct_mgmt_hub_Cancel();
	}
	

}
