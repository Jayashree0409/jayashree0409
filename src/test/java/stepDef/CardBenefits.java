package stepDef;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;

import pageActions.CardBenfitsAction;
import pageActions.CBOLPostLoginPageActions_test;
import pageActions.CBOL_CommonActions_test;
import pageActions.CitiHomePageActions_test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.ConfigFileReader;
import utils.ExcelUtils;
import utils.SeleniumDriver;
import utils.SeleniumHelper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pageActions.CBOLPostLoginPageActions_test;
import pageActions.CBOL_CommonActions_test;
import pageActions.CitiHomePageActions_test;
import utils.ConfigFileReader;
import utils.SeleniumDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;


public class CardBenefits {
	
	final Log log = LogFactory.getLog(CardBenefits.class.getName());
	//CitiHomePageActions citiHomePageActions = new CitiHomePageActions();
	CardBenfitsAction cardBenfitsAction = new CardBenfitsAction();
	CitiHomePageActions_test citiHomePageActions = new CitiHomePageActions_test();
	CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
	ConfigFileReader configFileReader;
	//String tempURL;
	//public XSSFRow row;
	public CardBenefits() {
		
	}
	
	@Given("^I am on the Card Benefits Home Page Iteration as new \"([^\"]*)\" and \"([^\"]*)\"$")
	//public void I_am_on_the_CardBenefits_Home_Page_Iteration_as(int Iteration) throws Throwable {
		public void I_am_on_the_CardBenefits_Home_Page_Iteration_as(String url, String lgcag)  {
			configFileReader= new ConfigFileReader();
			log.info(">>> Opening the Benefit Builders  page");
			System.out.println("From POM Excel Type to be used id - "+ System.getProperty("custType"));
		 	System.setProperty("lgcag", lgcag);
			
			if(System.getProperty("custType") == null) {
				System.setProperty("custType", "Blue"); 
			}
			
			//fetch data from excel with # row
			System.setProperty("Data", "");
			System.setProperty("Iteration", "");
			System.out.println("URL - " + url);
			System.setProperty("Data", "URL - "+url+"; ");		
			SeleniumDriver.openPage(url);
	}
	@Then("^User clicks on Credit & Account Protection and then click on FICO Score new$")
	public void User_clicks_on_Credit_Account_Protection_and_then_click_on_FICO_Score() throws Throwable {
		log.info(">>> Navigating to FICO");
	    // Write code here that turns the phrase above into concrete actions
		cardBenfitsAction.ClikCreditAccountProtectionFICOScore();
	}
	
	@Then("^I click on sign on with FICO DeepDrop new$")
	public void I_click_on_sign_on_with_FICO_DeepDrop() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		cardBenfitsAction.ClickSignOnCardfBenefits();
	}
//	@Then("^I enter a valid User ID and Password with FICO deepdropw$")
//	public void I_enter_userID_PWD_FICO_deepdropw() throws Throwable {
//		log.info(">>> Entering User ID and Password");
//		//fetch data from excel with # row
//		log.info(row.getCell(1).toString() + " - " + row.getCell(2).toString());
//		cardBenfitsAction.enterUID(row.getCell(1).toString(), row.getCell(2).toString());
//	    
//	}
	@Then("^Verify The FICO score is showed new$")
	public void Verify_The_FICO_score_is_showed() throws Throwable {
		cardBenfitsAction.VerifyFicoScore();
	    // Write code here that turns the phrase above into concrete actions
	}
	@Then("^Click on language Espanol new$")
	public void Click_on_language_Espanol() throws Throwable {
		cardBenfitsAction.ClickEspanol();
	    // Write code here that turns the phrase above into concrete actions
	}
	

}
