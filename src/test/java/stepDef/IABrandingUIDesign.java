package stepDef;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pageActions.CBOL_CommonActions_test;
import pageActions.CitiHomePageActions_test;
import pageActions.IABrandingUIDesignActions;
import pageActions.IABrandingUISpanishDesignActions;
import pageActions.ProductExplorerAction;
import utils.ConfigFileReader;
import utils.SeleniumDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class IABrandingUIDesign {
	
	final Log log = LogFactory.getLog(IABrandingUIDesign.class.getName());
	CitiHomePageActions_test citiHomePageActions = new CitiHomePageActions_test();
	CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
	IABrandingUIDesignActions iabrandingUIDesignActions = new IABrandingUIDesignActions();
	IABrandingUISpanishDesignActions iabrandingUISpanishDesignActions = new IABrandingUISpanishDesignActions();
	ProductExplorerAction peactions = new ProductExplorerAction();
	ConfigFileReader configFileReader;
	
	public IABrandingUIDesign () {
		
	}

	
	@And("^I click the Profiles$")
	public void i_click_the_Profiles() {
		iabrandingUIDesignActions.profiles();
	}
	
	@And("^I click the Banking service$")
	public void I_click_the_Banking_service() {
		iabrandingUIDesignActions.BankingServices();
	}
	
	@And("^I click the Credit card service$")
	public void i_click_the_Credit_card_service() {
		iabrandingUIDesignActions.CreditCardServices();
	}
	
	@And("^I click the Statements and Documents service$")
	public void i_click_the_Statements_and_Documents_service() {
		iabrandingUIDesignActions.StatementsandDocuments();
	}
	
	@And("^I click the Travel service$")
	public void i_click_the_Travel_service() {
		iabrandingUIDesignActions.TravelServices();
	}
	
	@And("^I click the More settings service$")
	public void i_click_the_More_settings_service() {
		iabrandingUIDesignActions.Moresettings();
	}
	
	@And("^I click the Payments and transfers$")
	public void i_click_the_Payments_and_transfers() {
		peactions.PaymentsandTransfers();
	}
	
	@And("^I click the Balance Transfer service$")
	public void i_click_the_Balance_Transfer_service() {
		iabrandingUIDesignActions.balance_transfer();
	}
	
	@Then("^I validate the Banking service title of page$")
	public void i_validate_the_Banking_service_title_of_page() {
		iabrandingUIDesignActions.Bankingwindowtitle();
	}
	
	@Then("^I validate the Credit card service title of page$")
	public void i_validate_the_Credit_card_service_title_of_page() {
		iabrandingUIDesignActions.CreditCardwindowtitle();
	}
	
	@Then("^I validate the Travel service title of page$")
	public void i_validate_the_Travel_service_title_of_page() {
		iabrandingUIDesignActions.Travelwindowtitle();
	}
	
	@Then("^I validate the Statements and Documents service title of page$")
	public void i_validate_the_Statements_and_Documents_service_title_of_page() {
		iabrandingUIDesignActions.Stmtdocswindowtitle();
	}
	
	@Then("^I validate the More settings service title of page$")
	public void i_validate_the_More_settings_service_title_of_page() {
		iabrandingUIDesignActions.Moresettingswindowtitle();
	}
	
	@Then("^I validate the Balance Transfer service title of page$")
	public void i_validate_the_Balance_Transfer_service_title_of_page() {
		iabrandingUIDesignActions.Balancetransferwindowtitle();
	}
	

	@Then("^I validate the font size for the Banking services heading$")
	public void i_validate_the_font_size_for_the_banking_services_heading() {
		iabrandingUIDesignActions.HeaderFontSizeValidation();
	}
	
	@Then("^I validate the font family for the Banking services heading$")
	public void i_validate_the_font_family_for_the_Banking_services_heading() {
		iabrandingUIDesignActions.HFontFamilyValidation();
	}
	
	@Then("^I validate the color for the Banking services heading$")
	public void i_validate_the_color_for_the_Banking_services_heading() {
		iabrandingUIDesignActions.HColorvalidation();
	}
	
	@Then("^I validate the Line height for the Banking services heading$")
	public void i_validate_the_Line_height_for_the_Banking_services_heading() {
		iabrandingUIDesignActions.HLineheightvalidation();
	}
	

	@Then("^I validate the Letterspace for the Banking services heading$")
	public void i_validate_the_Letterspace_for_the_Banking_services_heading() {
		iabrandingUIDesignActions.Hletterspacingvalidation();
	}
	
	@Then("^I validate the font size for the banking services link$")
	public void i_validate_the_font_size_for_the_banking_services_link() {
		iabrandingUIDesignActions.LinkFontSizeValidation();
	}
	
	@Then("^I validate the font size for the Statements and Documents services link$")
	public void i_validate_the_font_size_for_the_Statements_and_Documents_services_link() {
		iabrandingUIDesignActions.SDLinkFontSizeValidation();
	}
	
	@Then("^I validate the font size for the Credit card services link$")
	public void i_validate_the_font_size_for_the_credit_card_services_link() {
		iabrandingUIDesignActions.CRLinkFontSizeValiadation();
	}
	
	@Then("^I validate the font size for the Travel services link$")
	public void i_validate_the_font_size_for_the_Travel_services_link() {
		iabrandingUIDesignActions.TSLColorvalidation();
	}
	
	@Then("^I validate the font size for the More settings services link$")
	public void i_validate_the_font_size_for_the_More_settings_services_link() {
		iabrandingUIDesignActions.MSLFontSizeValidation();
	}
	
	@Then("^I validate the font family for the banking services link$")
	public void i_validate_the_font_family_for_the_banking_services_link() {
		iabrandingUIDesignActions.LFontFamilyValidation();
	}
	
	@Then("^I validate the font family for the Credit card services link$")
	public void i_validate_the_font_family_for_the_Credit_card_services_link() {
		iabrandingUIDesignActions.CRLFontFamilyValidation();
	}
	
	@Then("^I validate the font family for the Statements and Documents services link$")
	public void i_validate_the_font_family_for_the_Statements_and_Documents_services_link() {
		iabrandingUIDesignActions.SDLFontFamilyValidation();
	}
	
	@Then("^I validate the font family for the Travel services link$")
	public void i_validate_the_font_family_for_the_Travel_services_link() {
		iabrandingUIDesignActions.TSLFontFamilyValidation();
	}
	
	@Then("^I validate the font family for the More settings services link$")
	public void i_validate_the_font_family_for_the_More_settings_services_link() {
		iabrandingUIDesignActions.MSLFontFamilyValidation();
	}
	
	@Then("^I validate the color for the banking services link$")
	public void i_validate_the_color_for_the_banking_services_link() {
		iabrandingUIDesignActions.LColorvalidation();
	}
	
	@Then("^I validate the color for the Statements and Documents services link$")
	public void i_validate_the_color_for_the_Statements_and_Documents_services_link() {
		iabrandingUIDesignActions.SDLColorvalidation();
	}
	
	@Then("^I validate the color for the Credit card services link$")
	public void i_validate_the_color_for_the_Credit_card_services_link() {
		iabrandingUIDesignActions.CRLColorvalidation();
	}
	
	@Then("^I validate the color for the Travel services link$")
	public void i_validate_the_color_for_the_Travel_services_link() {
		iabrandingUIDesignActions.TSLColorvalidation();
	}
	
	@Then("^I validate the color for the More settings services link$")
	public void i_validate_the_color_for_the_More_settings_services_link() {
		iabrandingUIDesignActions.MSLColorvalidation();
	}
	
	@Then("^I validate the Line height for the banking services link$")
	public void i_validate_the_Line_height_for_the_banking_services_link() {
		iabrandingUIDesignActions.LLineheightvalidation();
	}
	
	@Then("^I validate the Line height for the Statements and Documents services link$")
	public void i_validate_the_Line_height_for_the_Statements_and_Documents_services_link() {
		iabrandingUIDesignActions.SDLLineheightvalidation();
	}
	
	@Then("^I validate the Line height for the Credit card services link$")
	public void i_validate_the_Line_height_for_the_Credit_card_services_link() {
		iabrandingUIDesignActions.CRLLineheightvalidation();
	}
	
	@Then("^I validate the Line height for the Travel services link$")
	public void i_validate_the_Line_height_for_the_Travel_services_link() {
		iabrandingUIDesignActions.TSLLineheightvalidation();
	}
	
	@Then("^I validate the Line height for the More settings services link$")
	public void i_validate_the_Line_height_for_the_More_settings_services_link() {
		iabrandingUIDesignActions.MSLLineheightvalidation();
	}
		
	@Then("^I validate the Letterspace for the banking services link$")
	public void i_validate_the_Letterspace_for_the_banking_services_link() {
		iabrandingUIDesignActions.Lletterspacingvalidation();
	}
	
	@Then("^I validate the Letterspace for the Statements and Documents services link$")
	public void i_validate_the_Letterspace_for_the_Statements_and_Documents_services_link() {
		iabrandingUIDesignActions.SDLletterspacingvalidation();
	}
	
	@Then("^I validate the Letterspace for the Credit card services link$")
	public void i_validate_the_Letterspace_for_the_Credit_card_services_link() {
		iabrandingUIDesignActions.CRletterspacingvalidation();
	}
	
	@Then("^I validate the Letterspace for the Travel services link$")
	public void i_validate_the_Letterspace_for_the_Travel_services_link() {
		iabrandingUIDesignActions.TSLletterspacingvalidation();
	}
	
	@Then("^I validate the Letterspace for the More settings services link$")
	public void i_validate_the_Letterspace_for_the_More_settings_services_link() {
		iabrandingUIDesignActions.MSLletterspacingvalidation();
	}
	
	@And("^I click on Espanol$")
	public void i_click_on_Espanol() {
		iabrandingUISpanishDesignActions.SpanishLanguage();
	}
	
	@Then("^I see the page with spanish content$")
	public void i_see_the_page_with_spanish_content() {
	}
	
	@And("^I click the Spanish Banking service$")
	public void i_click_the_Spanish_banking_service() {
		iabrandingUISpanishDesignActions.BankingServices();
	}
	
	@And("^I click the Spanish Credit card service$")
	public void i_click_the_Spanish_Credit_card_service() {
		iabrandingUISpanishDesignActions.CreditCardServices();
	}
	
	@And("^I click the Spanish Travel service$")
	public void i_click_the_Spanish_Travel_service() {
		iabrandingUISpanishDesignActions.TravelServices();
	}
	
	@And("^I click the Spanish Statements and Documents service$")
	public void i_click_the_Spanish_Statements_and_Documents_service() {
		iabrandingUISpanishDesignActions.StatementsandDocuments();
	}
	
	@And("^I click the Spanish More settings service$")
	public void i_click_the_Spanish_More_settings_service() {
		iabrandingUISpanishDesignActions.MoreSettings();
	}
	
	@Then("^I validate the font size for the Spanish banking services link$")
	public void i_validate_the_font_size_for_the_Spanish_banking_services_link() {
		iabrandingUISpanishDesignActions.LinkFontSizeValidation();
	}
	
	@Then("^I validate the font size for the Spanish Credit card services link$")
	public void i_validate_the_font_size_for_the_Spanish_Credit_card_services_link() {
		iabrandingUISpanishDesignActions.CRLinkFontSizeValiadation();
	}
	
	@Then("^I validate the font size for the Spanish Travel services link$")
	public void i_validate_the_font_size_for_the_Spanish_Travel_services_link() {
		iabrandingUISpanishDesignActions.TSLFontSizeValidation();
	}	
	
	@Then("^I validate the font size for the Spanish Statements and Documents services link$")
	public void i_validate_the_font_size_for_the_Spanish_Statements_and_Documents_services_link() {
		iabrandingUISpanishDesignActions.SDLinkFontSizeValidation();
	}
	
	@Then("^I validate the font size for the Spanish More settings services link$")
	public void i_validate_the_font_size_for_the_Spanish_More_settings_services_link() {
		iabrandingUISpanishDesignActions.MSLFontSizeValidation();
	}

	@Then("^I validate the font family for the Spanish banking services link$")
	public void i_validate_the_font_family_for_the_Spanish_banking_services_link() {
		iabrandingUISpanishDesignActions.LFontFamilyValidation();
	}
	
	@Then("^I validate the font family for the Spanish Credit card services link$")
	public void i_validate_the_font_family_for_the_Spanish_Credit_card_services_link() {
		iabrandingUISpanishDesignActions.CRLFontFamilyValidation();
	}
	
	@Then("^I validate the font family for the Spanish Travel services link$")
	public void i_validate_the_font_family_for_the_Spanish_Travel_services_link() {
		iabrandingUISpanishDesignActions.TSLFontFamilyValidation();
	}
	
	@Then("^I validate the font family for the Spanish Statements and Documents services link$")
	public void i_validate_the_font_family_for_the_Spanish_Statements_and_Documents_services_link() {
		iabrandingUISpanishDesignActions.SDLFontFamilyValidation();
	}
	
	@Then("^I validate the font family for the Spanish More settings services link$")
	public void i_validate_the_font_family_for_the_Spanish_More_settings_services_link() {
		iabrandingUISpanishDesignActions.MSLFontFamilyValidation();
	}
	
	@Then("^I validate the color for the Spanish banking services link$")
	public void i_validate_the_color_for_the_Spanish_banking_services_link() {
		iabrandingUISpanishDesignActions.LColorvalidation();
	}
	
	@Then("^I validate the color for the Spanish Credit card services link$")
	public void i_validate_the_color_for_the_Spanish_Credit_card_services_link() {
		iabrandingUISpanishDesignActions.CRLColorvalidation();
	}
	
	@Then("^I validate the color for the Spanish Travel services link$")
	public void i_validate_the_color_for_the_Spanish_Travel_services_link() {
		iabrandingUISpanishDesignActions.TSLColorvalidation();
	}
	
	@Then("^I validate the color for the Spanish Statements and Documents services link$")
	public void i_validate_the_color_for_the_Spanish_Statements_and_Documents_services_link() {
		iabrandingUISpanishDesignActions.SDLColorvalidation();
	}
	
	@Then("^I validate the color for the Spanish More settings services link$")
	public void i_validate_the_color_for_the_Spanish_More_settings_services_link() {
		iabrandingUISpanishDesignActions.MSLColorvalidation();
	}
	
	@Then("^I validate the Line height for the Spanish banking services link$")
	public void i_validate_the_Line_height_for_the_Spanish_banking_services_link() {
		iabrandingUISpanishDesignActions.LLineheightvalidation();
	}
	
	@Then("^I validate the Line height for the Spanish Credit card services link$")
	public void i_validate_the_Line_height_for_the_Spanish_Credit_card_services_link() {
		iabrandingUISpanishDesignActions.CRLLineheightvalidation();
	}
	
	@Then("^I validate the Line height for the Spanish Travel services link$")
	public void i_validate_the_Line_height_for_the_Spanish_Travel_services_link() {
		iabrandingUISpanishDesignActions.TSLLineheightvalidation();
	}
	
	@Then("^I validate the Line height for the Spanish Statements and Documents services link$")
	public void i_validate_the_Line_height_for_the_Spanish_Statements_and_Documents_services_link() {
		iabrandingUISpanishDesignActions.SDLLineheightvalidation();
	}
	
	@Then("^I validate the Line height for the Spanish More settings services link$")
	public void i_validate_the_Line_height_for_the_Spanish_More_settings_services_link() {
		iabrandingUISpanishDesignActions.MSLLineheightvalidation();
	}

	@Then("^I validate the Letterspace for the Spanish banking services link$")
	public void i_validate_the_Letterspace_for_the_Spanish_banking_services_link() {
		iabrandingUISpanishDesignActions.Lletterspacingvalidation();
	}
	
	@Then("^I validate the Letterspace for the Spanish Credit card services link$")
	public void i_validate_the_Letterspace_for_the_Spanish_Credit_card_services_link() {
		iabrandingUISpanishDesignActions.CRletterspacingvalidation();
	}
	
	@Then("^I validate the Letterspace for the Spanish Travel services link$")
	public void i_validate_the_Letterspace_for_the_Spanish_Travel_services_link() {
		iabrandingUISpanishDesignActions.TSLletterspacingvalidation();
	}
	
	@Then("^I validate the Letterspace for the Spanish Statements and Documents services link$")
	public void i_validate_the_Letterspace_for_the_Spanish_Statements_and_Documents_services_link() {
		iabrandingUISpanishDesignActions.SDLletterspacingvalidation();
	}
	
	@Then("^I validate the Letterspace for the Spanish More settings services link$")
	public void i_validate_the_Letterspace_for_the_Spanish_More_settings_services_link() {
		iabrandingUISpanishDesignActions.MSLletterspacingvalidation();
	}
	
}
