package stepDef;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;

//import pageActions.CardBenfitsAction;
import pageActions.CBOLPostLoginPageActions_test;
import pageActions.CBOL_CommonActions_test;
import pageActions.CitiHomePageActions_test;
import pageActions.IABrandingMenusActions;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.ConfigFileReader;
import utils.ExcelUtils;
import utils.SeleniumDriver;
import utils.SeleniumHelper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pageActions.CBOLPostLoginPageActions_test;
import pageActions.CBOL_CommonActions_test;
import pageActions.CitiHomePageActions_test;
import utils.ConfigFileReader;
import utils.SeleniumDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;


public class IABranding {
	
	final Log log = LogFactory.getLog(IABranding.class.getName());
	//CitiHomePageActions citiHomePageActions = new CitiHomePageActions();
//	CardBenfitsAction cardBenfitsAction = new CardBenfitsAction();
	IABrandingMenusActions iaBrandingMenusActions = new IABrandingMenusActions();
	CitiHomePageActions_test citiHomePageActions = new CitiHomePageActions_test();
	CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
	ConfigFileReader configFileReader;
	//String tempURL;
	//public XSSFRow row;
	public IABranding() {
		
	}
	
	@Then("^Move to angular page and verify Costco Cash rewards new link$")
	public void Move_to_angular_page_and_verify_Costco_Rewards_link() throws Throwable {
		
		iaBrandingMenusActions.VerifyCostoRewardsLinkAngular();
	    // Write code here that turns the phrase above into concrete actions
	}
	
//	@Then("^Move to AO page and verify Costco Cash rewards link$")
//	public void Move_to_AO_page_and_verify_Costco_rewards_link() throws Throwable {
//		//iaBrandingMenusActions.VerifyCostoLinkAngular();
//	    // Write code here that turns the phrase above into concrete actions
//	}
	@Then("^Move to legacy page and verify Costco Cash rewards link$")
	public void Move_to_Legacy_page_and_verify_Costco_Rewards_link() throws Throwable {
		iaBrandingMenusActions.VerifyCostoRewardsLegacy();
	    // Write code here that turns the phrase above into concrete actions
	}
//	@Then("^Move to 3rd Party page and verify Costco Cash rewards link$")
//	public void Move_to_3rdParty_page_and_verify_Costco_Rewards_link() throws Throwable {
//	    // Write code here that turns the phrase above into concrete actions
//	}
	
	@Then("^Move to angular page and verify Double Cash rewards new link$")
	public void Move_to_angular_page_and_verify_Double_Rewards_link() throws Throwable {
		
		iaBrandingMenusActions.VerifyDoubleRewardsLinkAngular();
	    // Write code here that turns the phrase above into concrete actions
	}
	
	@Then("^Move to legacy page and verify Double Cash rewards link$")
	public void Move_to_Legacy_page_and_verify_Double_Rewards_link() throws Throwable {
		iaBrandingMenusActions.VerifyDoubleRewardsLegacy();
	    // Write code here that turns the phrase above into concrete actions
	}

	

}
