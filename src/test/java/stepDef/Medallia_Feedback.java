package stepDef;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pageActions.CBOLPostLoginPageActions_test;
import pageActions.CBOL_CommonActions_test;
import pageActions.CitiHomePageActions_test;
import pageActions.Medallia_FeedbackActions;
import utils.ConfigFileReader;
import utils.SeleniumDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class Medallia_Feedback {
	
	final Log log = LogFactory.getLog(Medallia_Feedback.class.getName());
	CitiHomePageActions_test citiHomePageActions = new CitiHomePageActions_test();
	CBOLPostLoginPageActions_test cbolPostLoginPageActions = new CBOLPostLoginPageActions_test();
	CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
	Medallia_FeedbackActions Medallia_FeedbackAction = new Medallia_FeedbackActions();
	ConfigFileReader configFileReader;
	
	public Medallia_Feedback() {

		}
		

@And("^click on feedback icon$")
public void click_on_feedback_icon()  {
	Medallia_FeedbackAction.Feedback_icon();
	}


@Then("^I see medallia page$")
public void I_see_medallia_page() {
	Medallia_FeedbackAction.medialla();
	}

}



	
	

