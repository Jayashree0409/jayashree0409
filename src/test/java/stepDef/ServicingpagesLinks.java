package stepDef;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pageActions.CBOL_CommonActions_test;
import pageActions.CitiHomePageActions_test;
import pageActions.IABrandingUIDesignActions;
import pageActions.IABrandingUISpanishDesignActions;
import pageActions.BankingServicingPagesLinkValidation;
import pageActions.StatementsandDocumentsServicingPagesLinkValidation;
import utils.ConfigFileReader;
import utils.SeleniumDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class ServicingpagesLinks {
	
	final Log log = LogFactory.getLog(ServicingpagesLinks.class.getName());
	CitiHomePageActions_test citiHomePageActions = new CitiHomePageActions_test();
	CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
	BankingServicingPagesLinkValidation servicepageslink = new BankingServicingPagesLinkValidation();
	StatementsandDocumentsServicingPagesLinkValidation docsservicepageslink = new StatementsandDocumentsServicingPagesLinkValidation();
	//IABrandingUIDesignActions iabrandingUIDesignActions = new IABrandingUIDesignActions();
	//IABrandingUISpanishDesignActions iabrandingUISpanishDesignActions = new IABrandingUISpanishDesignActions();
	ConfigFileReader configFileReader;
	
	public ServicingpagesLinks () {
		
	}

	
	@Then("^I validate the Reset ATM link in the banking services$")
	public void I_validate_the_Reset_ATM_link_in_the_banking_services() {
		servicepageslink.ResetATMPINValidation();
	}
	
	@Then("^I validate the Lock card link in the banking services$")
	public void I_validate_the_Lock_card_link_in_the_banking_services() {
		servicepageslink.LockcardValidation();
	}
	
	@Then("^I validate the Replace ATM Card link in the banking services$")
	public void I_validate_the_Replace_ATM_Card_link_in_the_banking_services() {
		servicepageslink.ReplaceATMCardValidation();
	}
	
	@Then("^I validate the Change ATM Card to Debit Card link in the banking services$")
	public void I_validate_the_Change_ATM_Card_to_Debit_Card_link_in_the_banking_services() {
		servicepageslink.ChangeyourATMCardtoaDebitCardValidation();
	}
	
	@Then("^I validate the Stop Check Payment link in the banking services$")
	public void I_validate_the_Stop_Check_Payment_link_in_the_banking_services() {
		servicepageslink.StopCheckPaymentValidation();
	}
	
	@Then("^I validate the Reorder Personal Checks link in the banking services$")
	public void I_validate_the_Reorder_Personal_Checks_link_in_the_banking_services() {
		servicepageslink.ReorderPersonalChecksValidation();
	}
	
	@Then("^I validate the Text Banking link in the banking services$")
	public void I_validate_the_Text_Banking_link_in_the_banking_services() {
		servicepageslink.TextBankingValidation();
	}
	
	@Then("^I validate the Telephone Access Code link in the banking services$")
	public void I_validate_the_Telephone_Access_Code_link_in_the_banking_services() {
		servicepageslink.TelephoneAccessCodeValidation();
	}
	
	@Then("^I validate the Link Unlink Citi Accounts link in the banking services$")
	public void I_validate_the_Link_Unlink_Citi_Accounts_link_in_the_banking_services() {
		servicepageslink.LinkUnlinkCitiAccountsValidation();
	}
	
	@Then("^I validate the Link Unlink Savings for Overdraft Protection link in the banking services$")
	public void I_validate_the_Link_Unlink_Savings_for_Overdraft_Protection_link_in_the_banking_services() {
		servicepageslink.LinkUnlinkSavingsforOverdraftProtectionValidation();
	}
	
	@Then("^I validate the Request Upgrade to CitiPriority link in the banking services$")
	public void I_validate_the_Request_Upgrade_to_CitiPriority_link_in_the_banking_services() {
		servicepageslink.RequestUpgradetoCitiPriorityValidation();
	}
	
	@Then("^I validate the Request Upgrade to Citigold link in the banking services$")
	public void I_validate_the_Request_Upgrade_to_Citigold_link_in_the_banking_services() {
		servicepageslink.RequestUpgradetoCitigoldValidation();
	}
	
	@Then("^I validate the Link Citi Accounts from Other Countries link in the banking services$")
	public void I_validate_the_Link_Citi_Accounts_from_Other_Countries_link_in_the_banking_services() {
		servicepageslink.LinkCitiAccountsfromOtherCountriesValidation();
	}
	
	@Then("^I validate the Financial Tools link in the banking services$")
	public void I_validate_the_Financial_Tools_link_in_the_banking_services() {
		servicepageslink.FinancialToolsValidation();
	}
	
	@Then("^I validate the Investments link in the banking services$")
	public void I_validate_the_Investments_link_in_the_banking_services() {
		servicepageslink.InvestmentsValidation();
	}
	
	@Then("^I validate the Schedule an Appointment link in the banking services$")
	public void I_validate_the_Schedule_an_Appointment_link_in_the_banking_services() {
		servicepageslink.ScheduleanAppointmentValidation();
	}
	
	@Then("^I validate the Manage Appointments link in the banking services$")
	public void I_validate_the_Manage_Appointments_link_in_the_banking_services() {
		servicepageslink.ManageAppointmentsValidation();
	}
	
	@Then("^I validate the Account Snapshot link in the Statements and Documents services$")
	public void I_validate_the_Account_Snapshot_link_in_the_Statements_and_Documents_services() {
		docsservicepageslink.AccountSnapshotValidation();
	}
	
	@Then("^I validate the Account statement link in the Statements and Documents services$")
	public void I_validate_the_Account_statement_link_in_the_Statements_and_Documents_services() {
		docsservicepageslink.AccountstatementValidation();
	}
	
	@Then("^I validate the Ecommunications link in the Statements and Documents services$")
	public void I_validate_the_Ecommunications_link_in_the_Statements_and_Documents_services() {
		docsservicepageslink.EcommunicationsValidation();
	}
	
	@Then("^I validate the Secure Message Center link in the Statements and Documents services$")
	public void I_validate_the_Secure_Message_Center_link_in_the_Statements_and_Documents_services() {
		docsservicepageslink.SecureMessageCenterValidation();
	}
	
	@Then("^I validate the Tax Documents link in the Statements and Documents services$")
	public void I_validate_the_Tax_Documents_link_in_the_Statements_and_Documents_services() {
		docsservicepageslink.TaxDocumentsValidation();
	}
	
	@Then("^I validate the Manage Paperless Settings link in the Statements and Documents services$")
	public void I_validate_the_Manage_Paperless_Settings_link_in_the_Statements_and_Documents_services() {
		docsservicepageslink.ManagePaperlessSettingsValidation();
	}
	
	@Then("^I validate the Language Preference link in the Statements and Documents services$")
	public void I_validate_the_Language_Preference_link_in_the_Statements_and_Documents_services() {
		docsservicepageslink.LanguagePreferenceValidation();
	}
	
	@Then("^I validate the Manage Travel Notices link in the Travel services$")
	public void I_validate_the_Manage_Travel_Notices_link_in_the_Travel_services() {
		docsservicepageslink.ManageTravelNoticesValidation();
	}
	

}
