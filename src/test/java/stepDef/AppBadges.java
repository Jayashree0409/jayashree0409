package stepDef;


import pageActions.CBOL_CommonActions_test;
import pageActions.AppBadgesActions;
import utils.ConfigFileReader;
import utils.SeleniumDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class AppBadges {
	
	final Log log = LogFactory.getLog(AppBadges.class.getName());
	
	CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
	AppBadgesActions appbadgesaction = new AppBadgesActions();
	ConfigFileReader configFileReader;
	
	@Given("^I am on the Home Page with Iteration as \"([^\"]*)\"$")
	public void I_am_on_the_Home_Page_with_Iteration_as(String url)  {
		configFileReader= new ConfigFileReader();
		log.info(">>> Opening the home page");
	//	System.out.println("From POM Excel Type to be used id - "+ System.getProperty("custType"));
	//	System.setProperty("lgcag", lgcag);
		
	//if(System.getProperty("custType") == null) {
	//	System.setProperty("custType", "Blue"); 
	//	}
		
		//fetch data from excel with # row
		System.setProperty("Data", "");
		System.setProperty("Iteration", "");
		System.out.println("URL - " + url);
		System.setProperty("Data", "URL - "+url+"; ");		
		SeleniumDriver.openPage(url);

	}
	
	@When("^User scroll down to the footer of the page$")
	public void user_scroll_down_to_the_footer_of_the_page(){
		// Write into the log file
		log.info(">>> Scrolling down to the footer of the page");
		//Function to be called here
	}


	@And("^I click on Apple App badges$")
	public void i_click_on_Apple_App_badge(){
		//Function to be called here
		appbadgesaction.iOSAppbadge();
	}
	
	@And("^I click on post login Apple App badges$")
	public void i_click_on_post_login_apple_app_badges() {
		appbadgesaction.PostloginiOSAppbadge();
	}
	
	@And("^I see the speedbump display before navigate to app store 3rd party page$")
	public void i_see_the_speedbump_display_before_navigate_to_app_store_3rd_party_page(){
		//Function to be called here
		appbadgesaction.Speedbump();
	}
	
	@And("^I see the post login speedbump display before navigate to app store 3rd party page$")
	public void i_see_the_post_login_speedbump_display_before_navigate_to_app_store_3rd_party_page(){
		//Function to be called here
		appbadgesaction.Speedbumppostlogin();
	}
	
	@And("^I click on continue button to page open in new tab$")
	public void i_click_on_continue_button_to_page_opeb_in_new_tab(){
		//Function to be called here
		appbadgesaction.Continuebutton();
	}
	
	@And("^I click on post login continue button to page open in new tab$")
	public void i_click_on_post_login_continue_button_to_page_opeb_in_new_tab(){
		//Function to be called here
		appbadgesaction.PostloginContinuebutton();
	}
	
	@And("^I click on Google App badges$")
	public void i_click_on_google_app_badges() {
		appbadgesaction.AndroidAppbadge();
	}
	
	@And("^I click on post login Google App badges$")
	public void i_click_on_post_login_google_app_badges() {
		appbadgesaction.PostloginAndroidAppbadge();
	}
	
	@And("^I click on contact us$")
	public void i_click_on_contact_us() {
		appbadgesaction.contactus();
	}
	
	@And("^I click on ATM Branch on the black butler bar$")
	public void i_click_on_atm_branch_on_the_black_butler_bar() {
		appbadgesaction.ATMBranch();
	}
	
	@And("^I click on Open an account on the black butler bar$")
	public void i_click_on_open_an_account_on_the_black_butler_bar() {
		appbadgesaction.OpenAccount();
	}
	
	@Then("^I see the 3rd party page in new tab$")
	public void i_see_the_3rd_party_page_in_new_tab() {
		
	}
	
	@Then("^I see the ATM Branch page$")
	public void i_see_the_atm_branch_page() {
		
	}
}

