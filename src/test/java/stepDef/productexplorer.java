package stepDef;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pageActions.CBOLPostLoginPageActions_test;
import pageActions.CBOL_CommonActions_test;
import pageActions.CitiHomePageActions_test;
import pageActions.ProductExplorerAction;
import utils.ConfigFileReader;
import utils.SeleniumDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class productexplorer {
	
	final Log log = LogFactory.getLog(productexplorer.class.getName());
	CitiHomePageActions_test citiHomePageActions = new CitiHomePageActions_test();
	CBOLPostLoginPageActions_test cbolPostLoginPageActions = new CBOLPostLoginPageActions_test();
	CBOL_CommonActions_test cbolCommonActions = new CBOL_CommonActions_test();
	ProductExplorerAction productexploreraction = new ProductExplorerAction();
	ConfigFileReader configFileReader;
	
	public productexplorer () {
		
	}


	@Given("^I am on the Home Page with Iteration as new \"([^\"]*)\"$")
	public void I_am_on_the_Home_Page_with_Iteration_as_new(String url)  {
		configFileReader= new ConfigFileReader();
		log.info(">>> Opening the home page");
	//	System.out.println("From POM Excel Type to be used id - "+ System.getProperty("custType"));
	//	System.setProperty("lgcag", lgcag);
		
	//if(System.getProperty("custType") == null) {
	//	System.setProperty("custType", "Blue"); 
	//	}
		
		//fetch data from excel with # row
		System.setProperty("Data", "");
		System.setProperty("Iteration", "");
		System.out.println("URL - " + url);
		System.setProperty("Data", "URL - "+url+"; ");		
		SeleniumDriver.openPage(url);

	}

/*	@When("^I enter a valid User ID and Password \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_enter_a_valid_User_ID_and_Password_and(String uid, String pwd)  {
		log.info(">>> Entering User ID and Password");
		System.out.println("Credential - " + uid + " - " + pwd);
		citiHomePageActions.enterUID(uid, pwd);
		System.setProperty("Data", System.getProperty("Data")+"User ID - "+uid+"; Password - "+ pwd + "; ");
	}

	@And("^If I click on Sign on button$")
	public void if_I_click_on_Sign_on_button()  {
		citiHomePageActions.SignOnCBOL();
	}
*/
	@And("^I see CBOL dashboard Page$")
	public void i_see_CBOL_dashboard_Page()  {
	//	configFileReader= new ConfigFileReader();	
	//	System.out.println("Account - " + acct);
	//	System.setProperty("Data", System.getProperty("Data")+"Account to be verified - "+acct+"; ");
	//	cbolPostLoginPageActions.VerifyDashboard(acct.substring(acct.length()-4));
		
	}
	
	@And("click on product explorer icon$")
	public void click_on_product_explorer_icon() {
		productexploreraction.ProductExplorer();
	}
	@Then("I see product explorer dropdown page$")
	public void I_see_product_explorer_dropdown_page() {
		}
	
	@And("click on credit card icon$")
	public void click_on_credit_card_icon() {
		productexploreraction.CreditCard();
	}
	
	@Then("I see credit card marketing page$")
	public void I_see_credit_card_marketing_page() {
		}
	
	@And("click on Lending icon$")
	public void click_on_Lending_icon() {
		productexploreraction.Lending();
	}
	
	@Then("I see Lending marketing page$")
	public void I_see_Lending_marketing_page() {
		}
	
	@And("click on citigold icon$")
	public void click_on_citigold_icon() {
		productexploreraction.citigold();
	}
	
	@Then("I see citigold marketing page$")
	public void I_see_citigold_marketing_page() {
		}
	
	@And("click on Banking icon$")
	public void click_on_Banking_icon() {
		productexploreraction.Banking();
	}
	
	@Then("I see Banking marketing page$")
	public void I_see_Banking_marketing_page() {
		}
	
	@And("click on Investing icon$")
	public void click_on_Investing_icon() {
		productexploreraction.Investing();
	}
	
	@Then("I see Investing marketing page$")
	public void I_see_Investing_marketing_page() {
		}
	

		@And("I see Services drop down menu$")
	public void I_see_Services_drop_down_menu() {
		}
	
	@And("Click on Banking Service menu$")
	public void Click_on_Banking_Service_menu() {
		productexploreraction.BankingServices();
		}
		
	@And("I see Banking Service page$")
	public void I_see_Banking_Service_page() {
		}

	@And("Click on UpgradeCitiPriority link$")
	public void Click_on_UpgradeCitiPriority_link() {
		productexploreraction.UpgradeCitiPriority();
	}
	
	@And("I See UpgradeCitiPriority Page$")
	public void I_See_UpgradeCitiPriority_Page() {
		
	}
	
	@And("Click on Statements and Documents menu$")
	public void Click_on_Statements_and_Documents_menu() {
		productexploreraction.StatementsandDocuments();
	}
	
	@And("I see Statements and Documents page$")
	public void I_see_Statements_and_Documents_page() {
		
	}
	
	@And("Click on Rewards and Benefits menu$")
	public void Click_on_Rewards_and_Benefits_menu() {
		productexploreraction.RewardsandBenefits();
	}
	
	@And("I see Rewards and Benefits drop down menu$")
	public void I_see_Rewards_and_Benefits_drop_down_menu() {
		
	}
	
	@And("Click on Card benefits menu$")
	public void Click_on_Card_benefits_menu() {
		productexploreraction.CardBenefits();
	}
	
	
	@And("I see Card benefits page$")
	public void I_see_Card_benefits_page() {
		
	}
	
	
	@And("click on close button on the popup window$")
	public void click_on_close_button_on_the_popup_window() {
		productexploreraction.ModelClose();
	}
	
	@Then("click the Language to spanish$")
	public void click_the_Language_to_spanish() {
		productexploreraction.SpanishLanguage();
	}
	
	@And("I see the font size of product explorer$")
	public void I_see_the_font_size_of_product_explorer() {
		productexploreraction.productexplorerfontsize();
	}
	
}
