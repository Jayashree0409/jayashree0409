package utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
 
public class redlinesConfigFileReader {
	
	private Properties properties;
	private final String propertyFilePath= System.getProperty("user.dir")+"//configs//Redlines_configuration.properties";

	public redlinesConfigFileReader(){
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Redlines_configuration.properties not found at " + propertyFilePath);
		}		
	}

	public String getMFontvalue(){
		String getMFontvalue = properties.getProperty("MFontvalue");
		if(getMFontvalue!= null) return getMFontvalue;
		else throw new RuntimeException("Menu Font size data is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getMFontfamily(){
		String getMFontfamily = properties.getProperty("MFontfamily");
		if(getMFontfamily!= null) return getMFontfamily;
		else throw new RuntimeException("Menu font family is not specified in the Redlines_configuration.properties file.");		
	}
	
	
	public String getMcolor(){
		String getMcolor = properties.getProperty("Mcolor");
		if(getMcolor!= null) return getMcolor;
		else throw new RuntimeException("Menu color is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getMLineheight(){
		String getMLineheight = properties.getProperty("MLineheight");
		if(getMLineheight!= null) return getMLineheight;
		else throw new RuntimeException("Menu's Lineheight is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getMOpacity(){
		String getMOpacity = properties.getProperty("MOpacity");
		if(getMOpacity!= null) return getMOpacity;
		else throw new RuntimeException("Menu Opacity is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getHFontvalue(){
		String getHFontvalue = properties.getProperty("HFontvalue");
		if(getHFontvalue!= null) return getHFontvalue;
		else throw new RuntimeException("Header Font size data is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getHFontfamily(){
		String getHFontfamily = properties.getProperty("HFontfamily");
		if(getHFontfamily!= null) return getHFontfamily;
		else throw new RuntimeException("Header font family is not specified in the Redlines_configuration.properties file.");		
	}
	
	
	public String getHcolor(){
		String getHcolor = properties.getProperty("Hcolor");
		if(getHcolor!= null) return getHcolor;
		else throw new RuntimeException("Header color is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getHLineheight(){
		String getHLineheight = properties.getProperty("HLineheight");
		if(getHLineheight!= null) return getHLineheight;
		else throw new RuntimeException("Header's Lineheight is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getHOpacity(){
		String getHOpacity = properties.getProperty("HOpacity");
		if(getHOpacity!= null) return getHOpacity;
		else throw new RuntimeException("Header Opacity is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getHletterspace(){
		String getHletterspace = properties.getProperty("HLetterspace");
		if(getHletterspace!= null) return getHletterspace;
		else throw new RuntimeException("Header letter space is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getLFontvalue(){
		String getLFontvalue = properties.getProperty("LFontvalue");
		if(getLFontvalue!= null) return getLFontvalue;
		else throw new RuntimeException("Link Font size data is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getLFontfamily(){
		String getLFontfamily = properties.getProperty("LFontfamily");
		if(getLFontfamily!= null) return getLFontfamily;
		else throw new RuntimeException("Link font family is not specified in the Redlines_configuration.properties file.");		
	}
	
	
	public String getLcolor(){
		String getLcolor = properties.getProperty("Lcolor");
		if(getLcolor!= null) return getLcolor;
		else throw new RuntimeException("Link color is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getLLineheight(){
		String getLLineheight = properties.getProperty("LLineheight");
		if(getLLineheight!= null) return getLLineheight;
		else throw new RuntimeException("Link's Lineheight is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getLOpacity(){
		String getLOpacity = properties.getProperty("LOpacity");
		if(getLOpacity!= null) return getLOpacity;
		else throw new RuntimeException("Link Opacity is not specified in the Redlines_configuration.properties file.");		
	}
		
	public String getLheight(){
		String getLheight = properties.getProperty("Lheight");
		if(getLheight!= null) return getLheight;
		else throw new RuntimeException("Link height is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getLwidth(){
		String getLwidth = properties.getProperty("Lwidth");
		if(getLwidth!= null) return getLwidth;
		else throw new RuntimeException("Link width is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getLoutershadow(){
		String getLoutershadow = properties.getProperty("LOutershadow");
		if(getLoutershadow!= null) return getLoutershadow;
		else throw new RuntimeException("Link outershadow is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getLFillscolor(){
		String getLFillscolor = properties.getProperty("LFillscolor");
		if(getLFillscolor!= null) return getLFillscolor;
		else throw new RuntimeException("Link Fill color is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getLbordercolor(){
		String getLbordercolor = properties.getProperty("Lbordercolor");
		if(getLbordercolor!= null) return getLbordercolor;
		else throw new RuntimeException("Link border color is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getborderradius(){
		String getborderradius = properties.getProperty("borderradius");
		if(getborderradius!= null) return getborderradius;
		else throw new RuntimeException("Border radius is not specified in the Redlines_configuration.properties file.");		
	}
	
	public String getLletterspace(){
		String getLletterspace = properties.getProperty("Lletterspace");
		if(getLletterspace!= null) return getLletterspace;
		else throw new RuntimeException("Link letter space is not specified in the Redlines_configuration.properties file.");		
	}
	
 
}