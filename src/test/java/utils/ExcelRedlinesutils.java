package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import utils.ConfigFileReader;

public class ExcelRedlinesutils {
	
	 //Main Directory of the project
    public static final String currentDir = System.getProperty("user.dir");
	final static Log log = LogFactory.getLog(ExcelUtils.class.getName());
    
    public static ConfigFileReader configFileReader;

    //Location of Test data excel file
    public static String testDataExcelPath = null;

    //Excel WorkBook
    private static XSSFWorkbook excelWBook;

    //Excel Sheet
    private static XSSFSheet excelWSheet;

    //Excel cell
    private static XSSFCell cell;

    //Excel row
    private static XSSFRow row;

    //Row Number
    public static int rowNumber;

    //Column Number
    public static int columnNumber;

    public static String testDataExcelFileName = "";
    
    public static String testDateExcelsheetName = ""; 
    
    //Setter and Getters of row and columns
    public static void setRowNumber(int pRowNumber) {
        rowNumber = pRowNumber;
    }

    public static int getRowNumber() {
        return rowNumber;
    }

    public static void setColumnNumber(int pColumnNumber) {
        columnNumber = pColumnNumber;
    }

    public static int getColumnNumber() {
        return columnNumber;
    }

    public static void setExcelFileSheet(String sheetName) {
    	
    	configFileReader = new ConfigFileReader();
    	System.out.println("From POM Excel Type to be used id - "+ System.getProperty("custType"));
    	testDataExcelFileName = "";
    	testDataExcelFileName = configFileReader.getExcelName();
    	testDateExcelsheetName = "Demo";
    	testDataExcelPath = currentDir + configFileReader.getExcelPath();
    	log.info(testDataExcelPath + testDataExcelFileName);
        FileInputStream ExcelFile = null;
		try {
			ExcelFile = new FileInputStream(testDataExcelPath + testDataExcelFileName);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
        try {
			excelWBook = new XSSFWorkbook(ExcelFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
        excelWSheet = excelWBook.getSheet(sheetName);
    }

    public static String getCellData(int RowNum, int ColNum) {
    	log.info(RowNum + " - "+ ColNum);
        cell = excelWSheet.getRow(RowNum).getCell(ColNum);
        DataFormatter formatter = new DataFormatter();
        String cellData = formatter.formatCellValue(cell);
        return cellData;
    }

    public static XSSFRow getRowData(int RowNum) throws Exception {
    	log.info("Iteration is " + RowNum);
    	   try {
               row = excelWSheet.getRow(RowNum);
               return row;
           } catch (Exception e) {
               throw (e);
           }
    }

    public static void setCellData(String value, int RowNum, int ColNum) {
        row = excelWSheet.getRow(RowNum);
        cell = row.getCell(ColNum);
        if (cell == null) {
            cell = row.createCell(ColNum);
            cell.setCellValue(value);
        } else {
            cell.setCellValue(value);
        }
        FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream(testDataExcelPath + testDataExcelFileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        try {
			excelWBook.write(fileOut);
		} catch (IOException e) {
			e.printStackTrace();
		}
        try {
			fileOut.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
        try {
			fileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

}
