package utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
 
public class ServiceLinksConfigFileReader {
	
	private Properties properties;
	private final String propertyFilePath= System.getProperty("user.dir")+"//configs//Servicing_links.properties";

	public ServiceLinksConfigFileReader(){
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Servicing_Links_configuration.properties not found at " + propertyFilePath);
		}		
	}

	public String getResetATMPIN(){
		String getResetATMPIN = properties.getProperty("ResetATM/DebitCardPIN");
		if(getResetATMPIN!= null) return getResetATMPIN;
		else throw new RuntimeException("ResetATM/DebitCardPIN Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getLockcard(){
		String getLockcard = properties.getProperty("Lockcard");
		if(getLockcard!= null) return getLockcard;
		else throw new RuntimeException("Lockcard Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	
	public String getReplaceATMCard(){
		String getReplaceATMCard = properties.getProperty("ReplaceATM/DebitCard");
		if(getReplaceATMCard!= null) return getReplaceATMCard;
		else throw new RuntimeException("ReplaceATM/DebitCard Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getChangeyourATMCardtoaDebitCard(){
		String getChangeyourATMCardtoaDebitCard = properties.getProperty("ChangeyourATMCardtoaDebitCard");
		if(getChangeyourATMCardtoaDebitCard!= null) return getChangeyourATMCardtoaDebitCard;
		else throw new RuntimeException("ChangeyourATMCardtoaDebitCard Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getStopCheckPayment(){
		String getStopCheckPayment = properties.getProperty("StopCheckPayment");
		if(getStopCheckPayment!= null) return getStopCheckPayment;
		else throw new RuntimeException("StopCheckPayment Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getReorderPersonalChecks(){
		String getReorderPersonalChecks = properties.getProperty("ReorderPersonalChecks");
		if(getReorderPersonalChecks!= null) return getReorderPersonalChecks;
		else throw new RuntimeException("ReorderPersonalChecks Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getTextBanking(){
		String getTextBanking = properties.getProperty("TextBanking");
		if(getTextBanking!= null) return getTextBanking;
		else throw new RuntimeException("TextBanking Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
		
	public String getTelephoneAccessCode(){
		String getTelephoneAccessCode = properties.getProperty("TelephoneAccessCode");
		if(getTelephoneAccessCode!= null) return getTelephoneAccessCode;
		else throw new RuntimeException("TelephoneAccessCode Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getLinkUnlinkCitiAccounts(){
		String getLinkUnlinkCitiAccounts = properties.getProperty("Link/UnlinkCitiAccounts");
		if(getLinkUnlinkCitiAccounts!= null) return getLinkUnlinkCitiAccounts;
		else throw new RuntimeException("Link/UnlinkCitiAccounts Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getLinkUnlinkSavingsforOverdraftProtection(){
		String getLinkUnlinkSavingsforOverdraftProtection = properties.getProperty("Link/UnlinkSavingsforOverdraftProtection");
		if(getLinkUnlinkSavingsforOverdraftProtection!= null) return getLinkUnlinkSavingsforOverdraftProtection;
		else throw new RuntimeException("Link/UnlinkSavingsforOverdraftProtection Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getRequestUpgradetoCitiPriority(){
		String getRequestUpgradetoCitiPriority = properties.getProperty("RequestUpgradetoCitiPriority");
		if(getRequestUpgradetoCitiPriority!= null) return getRequestUpgradetoCitiPriority;
		else throw new RuntimeException("RequestUpgradetoCitiPriority Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getRequestUpgradetoCitigold(){
		String getRequestUpgradetoCitigold = properties.getProperty("RequestUpgradetoCitigold");
		if(getRequestUpgradetoCitigold!= null) return getRequestUpgradetoCitigold;
		else throw new RuntimeException("RequestUpgradetoCitigold Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getLinkCitiAccountsfromOtherCountries(){
		String getLFontfamily = properties.getProperty("LinkCitiAccountsfromOtherCountries");
		if(getLFontfamily!= null) return getLFontfamily;
		else throw new RuntimeException("LinkCitiAccountsfromOtherCountries Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
		
	public String getFinancialTools(){
		String getFinancialTools = properties.getProperty("FinancialTools");
		if(getFinancialTools!= null) return getFinancialTools;
		else throw new RuntimeException("FinancialTools Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getInvestments(){
		String getInvestments = properties.getProperty("Investments");
		if(getInvestments!= null) return getInvestments;
		else throw new RuntimeException("Investments Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getScheduleanAppointment(){
		String getScheduleanAppointment = properties.getProperty("ScheduleanAppointment");
		if(getScheduleanAppointment!= null) return getScheduleanAppointment;
		else throw new RuntimeException("ScheduleanAppointment Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
		
	public String getManageAppointments(){
		String getManageAppointments = properties.getProperty("ManageAppointments");
		if(getManageAppointments!= null) return getManageAppointments;
		else throw new RuntimeException("ManageAppointments Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getViewAccountSnapshot(){
		String getViewAccountSnapshot = properties.getProperty("ViewAccountSnapshot");
		if(getViewAccountSnapshot!= null) return getViewAccountSnapshot;
		else throw new RuntimeException("ViewAccountSnapshot Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getViewAccountstatements(){
		String getViewAccountstatements = properties.getProperty("ViewAccountstatements");
		if(getViewAccountstatements!= null) return getViewAccountstatements;
		else throw new RuntimeException("ViewAccountstatements Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getViewEcommunications(){
		String getViewEcommunications = properties.getProperty("ViewEcommunications");
		if(getViewEcommunications!= null) return getViewEcommunications;
		else throw new RuntimeException("ViewEcommunications Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getViewSecureMessageCenter(){
		String getViewSecureMessageCenter = properties.getProperty("ViewSecureMessageCenter");
		if(getViewSecureMessageCenter!= null) return getViewSecureMessageCenter;
		else throw new RuntimeException("ViewSecureMessageCenter Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getViewTaxDocuments(){
		String getViewTaxDocuments = properties.getProperty("ViewTaxDocuments");
		if(getViewTaxDocuments!= null) return getViewTaxDocuments;
		else throw new RuntimeException("ViewTaxDocuments Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getManagePaperlessSettings(){
		String getManagePaperlessSettings = properties.getProperty("ManagePaperlessSettings");
		if(getManagePaperlessSettings!= null) return getManagePaperlessSettings;
		else throw new RuntimeException("ManagePaperlessSettings Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getLanguagePreference(){
		String getLanguagePreference = properties.getProperty("LanguagePreference");
		if(getLanguagePreference!= null) return getLanguagePreference;
		else throw new RuntimeException("LanguagePreference Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
	
	public String getManageTravelNotices(){
		String getManageTravelNotices = properties.getProperty("ManageTravelNotices");
		if(getManageTravelNotices!= null) return getManageTravelNotices;
		else throw new RuntimeException("ManageTravelNotices Link not displayed as per in the servicing_links_configuration.properties file.");		
	}
}