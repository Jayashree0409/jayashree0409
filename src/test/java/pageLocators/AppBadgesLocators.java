package pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AppBadgesLocators {

	    
    @FindBy(how=How.ID,using="desktopAppStoreBadge")
    public WebElement iOSAppBadge;
    
    @FindBy(how=How.ID,using="appStoreNonResponsive")
    public WebElement PostLoginiOSAppBadge;
    
    @FindBy(how=How.ID,using="desktopGooglePlayBadge")
    public WebElement AndroidAppBadge;
    
    @FindBy(how=How.ID,using="googlePlayNonResponsive")
    public WebElement PostLoginAndroidAppBadge;
    
    @FindBy(how=How.CLASS_NAME,using="modal-content")
    public WebElement Speedbump;
    
    @FindBy(how=How.CLASS_NAME,using="speedbump-dialog")
    public WebElement PostloginSpeedbump;
    
    @FindBy(how=How.ID,using="overlayBtn")
    public WebElement ContinueButton;
    
    @FindBy(how=How.ID,using="dialogContinueBtn")
    public WebElement PostloginContinueButton;
    
    @FindBy(how=How.CLASS_NAME,using="close")
    public WebElement CloseSpeedbump;
    
    @FindBy(how=How.CLASS_NAME,using="dialog-closeBtn")
    public WebElement PostloginCloseSpeedbump;
    
    @FindBy(how=How.LINK_TEXT,using="ATM/Branch")
    public WebElement ATMBranch;
    
    @FindBy(how=How.LINK_TEXT,using="ATM/Sucursal")
    public WebElement SpanishATMBranch;
    
    @FindBy(how=How.LINK_TEXT,using="Open an Account")
    public WebElement OpenAccount;
    
    @FindBy(how=How.LINK_TEXT,using="Abrir una Cuenta")
    public WebElement SpanishOpenAccount;
    
    @FindBy(how=How.LINK_TEXT,using="Contact Us")
    public WebElement ContactUs;
    
    @FindBy(how=How.LINK_TEXT,using="Contáctanos")
    public WebElement SpanishContactUs;
        
  }
