package pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;



public class IABrandingLocators {
	

	
	@FindBy(how=How.ID,using="Inspect")
	public WebElement Inspects;
	
	@FindBy(how=How.ID,using="topNavAccounts")
	public WebElement Accounts;
	
	@FindBy(how=How.ID,using="accountsMainLI")
	public WebElement AGAccounts;
	
	/*Payments & Transfer Mega Menu */
    
    @FindBy(how=How.ID,using="paymentsandTransfers")
    public WebElement PaymentsandTransfers;
    
    @FindBy(how=How.ID,using="pntMainLI")
    public WebElement AGPaymentsandTransfers;
 
    /*Payments & Transfers Page Links Locators under the Payments Heading*/
    
      		
    		
    @FindBy(how=How.LINK_TEXT,using="Activity")
    public WebElement Activity;
    
    @FindBy(how=How.LINK_TEXT,using="Pay Bills")
    public WebElement Paybills;
    
    @FindBy(how=How.LINK_TEXT,using="Manage Payees")
    public WebElement Managepay;
    
    /*Payments & Transfers Page Links Locators under the Transfers Heading*/
    
    @FindBy(how=How.LINK_TEXT,using="Your Citi Accounts")
    public WebElement Yourcitiacct;
    
    @FindBy(how=How.LINK_TEXT,using="Your External Accounts")
    public WebElement Yourextacct;
    
    @FindBy(how=How.LINK_TEXT,using="Any Citi Accounts")
    public WebElement Anycitiacct;

    @FindBy(how=How.LINK_TEXT,using="Any External Account")
    public WebElement AnyExternalAccount;
    
    @FindBy(how=How.LINK_TEXT,using="Cualquier Cuenta Externa")
    public WebElement SpanishAnyExternalAccount;
    
    /* send with zelle doesn;t have link text in code */
		
    
    
    /*Services Mega Menu */
    
    @FindBy(how=How.ID,using="services")
    public WebElement Services;
    
    @FindBy(how=How.ID,using="servicesMainLI")
    public WebElement AGServices;
    
    /*Banking Services Page Links Locators for Access, Checks & Mobile Heading*/
    
    @FindBy(how=How.LINK_TEXT,using="Banking Services")
    public WebElement BankingServices;
    
    @FindBy(how=How.LINK_TEXT,using="Servicios Bancarios")
    public WebElement SpanishBankingServices;
    
    @FindBy(how=How.CLASS_NAME,using="accessChecksAndMobileCards")
    public WebElement AccessCheck;
    
    @FindBy(how=How.LINK_TEXT,using="Activate ATM/Debit Card")
    public WebElement ActivateATM;
    
    @FindBy(how=How.LINK_TEXT,using="Reset ATM/Debit Card PIN")
    public WebElement ResetATMPIN;
    
    @FindBy(how=How.LINK_TEXT,using="Restablecer el PIN de la Tarjeta de Débito/ ATM")
    public WebElement SpanishResetATMPIN;
    
    @FindBy(how=How.LINK_TEXT,using="Lock Card")
    public WebElement LockCard;
    
    @FindBy(how=How.LINK_TEXT,using="Bloquear Tarjeta")
    public WebElement SpanishLockCard;
    
    @FindBy(how=How.LINK_TEXT,using="Replace ATM/Debit Card")
    public WebElement ReplaceATMcard;
    
    @FindBy(how=How.LINK_TEXT,using="Reemplazar Tarjeta de ATM/Débito")
    public WebElement SpanishReplaceATMcard;
    
    @FindBy(how=How.LINK_TEXT,using="Change Your ATM Card to a Debit Card")
    public WebElement ChangeATMCard;
    
    @FindBy(how=How.LINK_TEXT,using="Cambiar Tu Tarjeta ATM a una Tarjeta de Débito")
    public WebElement SpanishChangeATMCard;
    
    @FindBy(how=How.LINK_TEXT,using="Stop Check Payment")
    public WebElement StopCheck;
    
    @FindBy(how=How.LINK_TEXT,using="Suspender Pago con Cheque")
    public WebElement SpanishStopCheck;
    
    @FindBy(how=How.LINK_TEXT,using="Reorder Personal Checks")
    public WebElement Reordercheck;
    
    @FindBy(how=How.LINK_TEXT,using="Volver a Ordenar Cheques Personales")
    public WebElement SpanishReordercheck;
    
    @FindBy(how=How.LINK_TEXT,using="Text Banking")
    public WebElement Textbank;
    
    @FindBy(how=How.LINK_TEXT,using="Banca por Texto")
    public WebElement SpanishTextbank;
    
    @FindBy(how=How.LINK_TEXT,using="Telephone Access Code")
    public WebElement TeleAccesscode;
    
    @FindBy(how=How.LINK_TEXT,using="Código de Acceso Telefónico")
    public WebElement SpanishTeleAccesscode;
    
    /*Banking Services Page Links Locators for Link Accounts, Upgrades & More Heading*/
    
    @FindBy(how=How.CLASS_NAME,using="linkAccountsCards")
    public WebElement Linkacctupgrade;
    
    @FindBy(how=How.LINK_TEXT,using="Link/Unlink Citi Accounts")
    public WebElement Linkorunlinkacct;
    
    @FindBy(how=How.LINK_TEXT,using="Vincular/Desvincular Cuentas Citi")
    public WebElement SpanishLinkorunlinkacct;
    
    @FindBy(how=How.LINK_TEXT,using="Manage Overdraft Protection")
    public WebElement LinkorUnlinksaving;
    
    @FindBy(how=How.LINK_TEXT,using="Administrar Protección Sobregiros")
    public WebElement SpanishLinkorUnlinksaving;
    
    @FindBy(how=How.LINK_TEXT,using="Link Citi Accounts from Other Countries")
    public WebElement LinkCitiAccount;
    
    @FindBy(how=How.LINK_TEXT,using="Vincular Cuentas Citi de Otros Países")
    public WebElement SpanishLinkCitiAccount;
    
    @FindBy(how=How.CLASS_NAME,using="upgradesAndServicesCards")
    public WebElement UpgradeServices;
    
    @FindBy(how=How.LINK_TEXT,using="Request Upgrade to Citi Priority")
    public WebElement UpgradeCitiPriority;
        
    @FindBy(how=How.LINK_TEXT,using="Solicitar Ascenso a Citi Priority")
    public WebElement SpanishUpgradeCitiPriority;
    
    @FindBy(how=How.LINK_TEXT,using="Request Upgrade to Citigold")
    public WebElement UpgradeCitigold;
    
    @FindBy(how=How.LINK_TEXT,using="Solicitar Ascenso a Citigold")
    public WebElement SpanishUpgradeCitigold;
        
    @FindBy(how=How.LINK_TEXT,using="Financial Tools")
    public WebElement FinanceTool;
    
    @FindBy(how=How.LINK_TEXT,using="Herramientas Financieras")
    public WebElement SpanishFinanceTool;
    
    @FindBy(how=How.LINK_TEXT,using="Investments")
    public WebElement Invest;
    
    @FindBy(how=How.LINK_TEXT,using="Inversiones")
    public WebElement SpanishInvest;
    
    @FindBy(how=How.LINK_TEXT,using="Schedule an Appointment")
    public WebElement Scheduleappt;
    
    @FindBy(how=How.LINK_TEXT,using="Programar una Cita")
    public WebElement SpanishScheduleappt;
    
    @FindBy(how=How.LINK_TEXT,using="Manage Appointments")
    public WebElement ManageAppt;
    
    @FindBy(how=How.LINK_TEXT,using="Administrar Citas")
    public WebElement SpanishManageAppt;
       
    
    /*Credit Card Services Page Links for Card Management */
    
    @FindBy(how=How.LINK_TEXT,using="Credit Card Services")
    public WebElement Creditcardservices;
  
    @FindBy(how=How.LINK_TEXT,using="Activate Credit Card")
    public WebElement ActivateCredcard;
    
    @FindBy(how=How.LINK_TEXT,using="Link/Unlink Citi Accounts")
    public WebElement Linkunlinkcitiac;
    
    @FindBy(how=How.LINK_TEXT,using="Lock Card")
    public WebElement lockcard;
    
    @FindBy(how=How.LINK_TEXT,using="Replace Lost/Stolen/Damaged Card")
    public WebElement Replacecard;
    
    @FindBy(how=How.LINK_TEXT,using="Request/Cancel Credit Card PIN")
    public WebElement Requestcreditcard;
    
    @FindBy(how=How.LINK_TEXT,using="Request a Credit Limit Increase")
    public WebElement Requestcreditlimit;
    
    @FindBy(how=How.LINK_TEXT,using="Request a Card Agreement")
    public WebElement RequestCardAgree;
    
    @FindBy(how=How.LINK_TEXT,using="Authorized Users")
    public WebElement Authuser;
    
    @FindBy(how=How.LINK_TEXT,using="Servicios de Tarjeta de Crédito")
    public WebElement SpanishCreditcardservices;
    
    @FindBy(how=How.LINK_TEXT,using="Activar Tarjeta de Crédito")
    public WebElement SpanishActivateCredcard;
    
    @FindBy(how=How.LINK_TEXT,using="Vincular/Desvincular Cuentas Citi")
    public WebElement SpanishLinkunlinkcitiac;
    
    @FindBy(how=How.LINK_TEXT,using="Bloquear Tarjeta")
    public WebElement Spanishlockcard;
    
    @FindBy(how=How.LINK_TEXT,using="Reemplazar Tarjeta/Tarjeta Dañada/Nunca robado Mi Tarjeta")
    public WebElement SpanishReplacecard;
    
    @FindBy(how=How.LINK_TEXT,using="Solicitar/Cancelar PIN de Tarjeta de Crédito")
    public WebElement SpanishRequestcreditcard;
    
    @FindBy(how=How.LINK_TEXT,using="Solicitar un Aumento de la límite de Crédito")
    public WebElement SpanishRequestcreditlimit;
    
    @FindBy(how=How.LINK_TEXT,using="Solicitar el Contrato de Tarjeta")
    public WebElement SpanishRequestCardAgree;
    
    @FindBy(how=How.LINK_TEXT,using="Agregar/Administrar Usuarios Autorizados")
    public WebElement SpanishAuthuser;
    
    
    /*Credit Card Services Page Links for Charges, Refunds & Disputes */
    
    @FindBy(how=How.LINK_TEXT,using="View Recurring Charges")
    public WebElement Viewrecchar;
    
    @FindBy(how=How.LINK_TEXT,using="Dispute Center")
    public WebElement Disputecen;
    
    @FindBy(how=How.LINK_TEXT,using="Ver Cargos Recurrentes")
    public WebElement SpanishViewrecchar;
    
    @FindBy(how=How.LINK_TEXT,using="Centro de Disputas")
    public WebElement SpanishDisputecen;
    
    /*Credit Card Services Page Links for Mobile & Digital Wallet */
    
    @FindBy(how=How.LINK_TEXT,using="Text Banking")
    public WebElement TextBnk;
    
    @FindBy(how=How.LINK_TEXT,using="Virtual Account Number")
    public WebElement Virtualacct;
    
    @FindBy(how=How.LINK_TEXT,using="Citi Pay'℠'")
    public WebElement Citipay;
      
    @FindBy(how=How.LINK_TEXT,using="PayPal")
    public WebElement paypal;
    
    @FindBy(how=How.LINK_TEXT,using="Banca por Texto")
    public WebElement SpanishTextBnk;
    
    @FindBy(how=How.LINK_TEXT,using="Número de Cuenta Virtual")
    public WebElement SpanishVirtualacct;
    
    /*Travel Services */
    
    @FindBy(how=How.LINK_TEXT,using="Travel Services")
    public WebElement Travelservice;
 
    @FindBy(how=How.LINK_TEXT,using="Set Up/Manage Travel Notices")
    public WebElement ManageTravel;
    
    @FindBy(how=How.LINK_TEXT,using="Servicios de Viaje")
    public WebElement SpanishTravelservice;
 
    @FindBy(how=How.LINK_TEXT,using="Configurar/Administrar Notificaciones de Viaje")
    public WebElement SpanishManageTravel;
       
    /* Statements & Documents Services Page Links */
    
    @FindBy(how=How.LINK_TEXT,using="Statements & Documents")
    public WebElement StatementsandDocuments;
    
    @FindBy(how=How.LINK_TEXT,using="Estados de Cuenta y Documentos")
    public WebElement SpanishStatementsandDocuments;
    
    @FindBy(how=How.LINK_TEXT,using="Snapshot")
    public WebElement ViewAcctSnap;
    
    @FindBy(how=How.LINK_TEXT,using="Ver Sinopsis de la Cuenta")
    public WebElement SpanishViewAcctSnap;
    
    @FindBy(how=How.LINK_TEXT,using="Statements")
    public WebElement ViewAcctStmt;
    
    @FindBy(how=How.LINK_TEXT,using="Ver Estados de Cuenta")
    public WebElement SpanishViewAcctStmt;
    
    @FindBy(how=How.LINK_TEXT,using="View E-Communications")
    public WebElement ViewEcomm;
    
    @FindBy(how=How.LINK_TEXT,using="Ver E-Communications")
    public WebElement SpanishViewEcomm;
    
    @FindBy(how=How.LINK_TEXT,using="Secure Message Center")
    public WebElement ViewSecMsg;
    
    @FindBy(how=How.LINK_TEXT,using="Ver el Centro de Mensajes Seguros")
    public WebElement SpanishViewSecMsg;
    
    @FindBy(how=How.LINK_TEXT,using="Tax Documents")
    public WebElement Viewtxdoc;
    
    @FindBy(how=How.LINK_TEXT,using="Ver Documentos de Impuestos")
    public WebElement SpanishViewtxdoc;
    
    @FindBy(how=How.LINK_TEXT,using="Manage Paperless Settings")
    public WebElement MangPaperless;
    
    @FindBy(how=How.LINK_TEXT,using="Administrar la Configuración del Programa Paperless")
    public WebElement SpanishMangPaperless;
    
    @FindBy(how=How.LINK_TEXT,using="Language Preferences")
    public WebElement LangPref;
    
    @FindBy(how=How.LINK_TEXT,using="Preferencias del Idioma")
    public WebElement SpanishLangPref;
    
    /*Rewards & Benefits Mega Menu */
    
	@FindBy(how=How.ID,using="rewards")
	public WebElement RewardsandBenefits;
    
    
	@FindBy(how=How.ID,using="rewardsMainLI")
	public WebElement AGRewardsandBenefits;
	
    /*Rewards Page Links Locators */
    

	@FindBy(how=How.LINK_TEXT,using="ThankYou Rewards")
    public WebElement Thankurew;
    
    @FindBy(how=How.LINK_TEXT,using="Citi Private Pass")
    public WebElement Citipripass;
    
    @FindBy(how=How.LINK_TEXT,using="Special Offers")
    public WebElement Sploff;
    
	@FindBy(how=How.LINK_TEXT,using="Card Benefits")
	public WebElement CardBenefits;
	
	@FindBy(how=How.LINK_TEXT,using="Beneficios de la Tarjeta")
	public WebElement SpanishCardBenefits;

	@FindBy(how=How.LINK_TEXT,using="Español")
	public WebElement LanguageSpanish;
	
	@FindBy(how=How.ID,using="closebutton")
	public WebElement closeicon;
	
	 /*Profiles Mega Menu */
	
	@FindBy(how=How.ID,using="profileSettings")
	public WebElement profiles;
	
	@FindBy(how=How.ID,using="profilesMainLI")
	public WebElement AGprofiles;
	
	@FindBy(how=How.LINK_TEXT,using="User ID")
	public WebElement Userid;
	
	@FindBy(how=How.LINK_TEXT,using="Password")
	public WebElement Passwd;
	
	@FindBy(how=How.LINK_TEXT,using="Income Information")
	public WebElement IncomeInf;
	
	@FindBy(how=How.LINK_TEXT,using="Account Alerts")
	public WebElement AcctAlrt;
	
	@FindBy(how=How.LINK_TEXT,using="Identificación de Usuario")
	public WebElement SpanishUserid;
	
//	Contraseña
	
//	Información de Contacto
	
//	Información sobre Ingresos
	
//	Alertas de Cuenta
	
	/*More Settings service pages under the Accounts Heading*/
	
	@FindBy(how=How.LINK_TEXT,using="More Settings")
	public WebElement Moresettings;
	
	@FindBy(how=How.LINK_TEXT,using="Nickname Bank Accounts")
	public WebElement Nickname;
	
	@FindBy(how=How.LINK_TEXT,using="Show/Hide Account Numbers")
	public WebElement ShoworHideAcct;
	
	@FindBy(how=How.LINK_TEXT,using="Start/Home Page")
	public WebElement StartorHome;
	
	@FindBy(how=How.LINK_TEXT,using="Authorized Users")
	public WebElement Authusers;
		
	@FindBy(how=How.LINK_TEXT,using="Default Credit Card & Nicknames")
	public WebElement DefaCards;
	
	@FindBy(how=How.LINK_TEXT,using="Manage Payment Due Date")
	public WebElement Managepaymt;
	
		
	@FindBy(how=How.LINK_TEXT,using="Configuraciones")
	public WebElement SpanishMoresettings;
	
	@FindBy(how=How.LINK_TEXT,using="Tarjeta de Crédito Predeterminada y Apodos")
	public WebElement SpanishNickname;
	
//	@FindBy(how=How.LINK_TEXT,using="Show/Hide Account Numbers")
//	public WebElement ShoworHideAcct;
	
	@FindBy(how=How.LINK_TEXT,using="Fecha de Vencimiento de Pago")
	public WebElement SpanishStartorHome;

	//Apodos de Cuentas
	
	//Mostrar/Ocultar Números de Cuenta
	
	//Agregar/Administrar Usuarios Autorizados
	
	//Seleccionar Página de Inicio
	
	
	
	
	/*More Settings service pages under the Security Heading*/
	
	@FindBy(how=How.LINK_TEXT,using="Security Questions")
	public WebElement SecuQues;
	
	@FindBy(how=How.LINK_TEXT,using="Security Word")
	public WebElement Secuword;
	
	@FindBy(how=How.LINK_TEXT,using="PIN Security Option")
	public WebElement PINSecques; 
	
	@FindBy(how=How.LINK_TEXT,using="Privacy Options")
	public WebElement Privacy;
	
	@FindBy(how=How.LINK_TEXT,using="2-Step Authentication")
	public WebElement twostep;
	
	@FindBy(how=How.LINK_TEXT,using="Apps With Account Access")
	public WebElement appacctaccess;
	
	@FindBy(how=How.LINK_TEXT,using="Preguntas de Seguridad")
	public WebElement SpanishSecuQues;
	
	@FindBy(how=How.LINK_TEXT,using="Palabras de Seguridad")
	public WebElement SpanishPrivacy;
	
	@FindBy(how=How.LINK_TEXT,using="Autenticación de 2 Pasos")
	public WebElement Spanishtwostep;
	
	@FindBy(how=How.LINK_TEXT,using="Aplicaciones con Acceso a tu Cuenta")
	public WebElement Spanishappacctaccess;
	
	@FindBy(how=How.LINK_TEXT,using="Opción de Seguridad PIN")
	public WebElement SpanishPINSecques;
	
	//Opciones de Privacidad
	
	
	/*More Settings service pages under the Account Alerts Heading*/
	
	@FindBy(how=How.LINK_TEXT,using="Account Alerts")
	public WebElement acctaler;
	
	@FindBy(how=How.LINK_TEXT,using="Alertas de Cuenta")
	public WebElement Spanishacctaler;
	
	/*Balance Transfer Service Pages*/
	
	@FindBy(how=How.LINK_TEXT,using="Balance Transfers")
	public WebElement Balance_transfer;
	
	@FindBy(how=How.LINK_TEXT,using="Explore Balance Transfer Options")
	public WebElement Balancetransferoffer;
	
	@FindBy(how=How.LINK_TEXT,using="View Balance Transfer Status")
	public WebElement Balancetransferstatus;
	
	@FindBy(how=How.ID,using="username")
	public WebElement NewUsername;
	
	@FindBy(how=How.ID,using="usernameMasked")
	public WebElement NewUsernamemasked;
	
	@FindBy(how=How.NAME,using="password")
	public WebElement CurrPassword;
	
	@FindBy(how=How.ID,using="link_lkChUIDContinue")
	public WebElement ChangeButton;
	
	@FindBy(how=How.LINK_TEXT,using="Cancel")
	public WebElement Cancelbutton;
	
	@FindBy(how=How.LINK_TEXT,using="Cancelar")
	public WebElement SpanishCancelbutton;
	
	@FindBy(how=How.ID,using="link_lkChUidCnfrmContinue")
	public WebElement Continuebutton;
	
}
