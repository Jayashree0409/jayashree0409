package pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class IABrandingUIDesignLocator {
	
    @FindBy(how=How.ID,using="services")
    public WebElement Services;
    
    @FindBy(how=How.ID,using="servicesMainLI")
    public WebElement AGServices;
    
	@FindBy(how=How.ID,using="profileSettings")
	public WebElement profiles;
	
	@FindBy(how=How.ID,using="profilesMainLI")
	public WebElement AGprofiles;


}
