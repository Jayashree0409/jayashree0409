package pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Medallia_FeedbackLocator {

	
	@FindBy(how=How.ID,using="nebula_div_btn")
	public WebElement Feedback_Icon;
	
	@FindBy(how=How.CLASS_NAME,using="live-form-content")
	public WebElement medaillia_form;
	
	@FindBy(how=How.ID,using="Close")
	public WebElement close_button;
	
	@FindBy(how=How.ID,using="Exit")
	public WebElement Exit_button;
	


}
