package pageLocators;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utils.SeleniumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


public class CardBenefitsLocators {

	
	@FindBy(how=How.XPATH,using="//*[@id='main']/div[2]/div/section[1]/section/nav/ul/li[4]/a")
	public WebElement CreditAcccountProtection;

	@FindBy(how=How.XPATH, using="//*[@id='main']/div[3]/div/section[1]/section/nav/ul/li[4]/ul/li[1]/a")
	public WebElement FICOscore;
	
	@FindBy(how=How.XPATH, using="//*[@id='main']/div[2]/div/div[1]/div/section/div/div/div/a")
	public WebElement SignOnCardBenefits;
	
	@FindBy(how=How.XPATH, using="//*[@id='navUtility']/div/div/ul/li[3]/a")
	public WebElement LanguageEspanol;
	
	@FindBy(how=How.XPATH, using="//*[@id='fico-score-number-countup']")
	public WebElement FicoScoreCount;
	
	@FindBy(how=How.XPATH, using="//*[@id='acsMainInvite']/div/a[1]")
	public WebElement CancelFeedback;
	
	
	@FindBy(how=How.XPATH,using="//*[@id='usernameMasked']") 
	//@FindBy(how=How.XPATH,using="//*[@id='username']") 
	public WebElement loginUID;
	
	//@FindBy(how=How.XPATH,using="//*[@id='usernameMasked']") 
	@FindBy(how=How.XPATH,using="//*[@id='username']") 
	public WebElement loginUID2;
	
	@FindBy(how=How.XPATH,using="//*[@id='password']")
	public WebElement loginPWD;
	
	@FindBy(how=How.XPATH,using="//*[@id='signInBtn']")
	public WebElement SignOnBtn;
	
	
}
