package pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utils.SeleniumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class IABrandingMenusLocators {
	
	
	@FindBy(how=How.XPATH,using="//*[@id='services']/button")
	public WebElement ServicesAngular;
	
	@FindBy(how=How.XPATH, using="//*[@id='services']/a")
	public WebElement ServicesLegacy;
	
	@FindBy(how=How.XPATH, using="//*[@id='rewards']/ul/li[1]/a")
	public WebElement CostcoRewardsLegacy;
	
	
	@FindBy(how=How.XPATH, using="//*[@id='rewards']/ul/li[1]/a")
	public WebElement DoubleRewardsLegacy;
		
	@FindBy(how=How.XPATH, using="//*[@id='rewardsMainLI']/div[3]/ul[1]/li[1]/citi-cta/a")
	public WebElement CostcoRewardsAngular;
	@FindBy(how=How.XPATH, using="//*[@id='rewardsMainLI']/div[3]/ul[1]/li[3]/citi-cta/a")
	public WebElement DoubleRewardsAngular;
	
	
	@FindBy(how=How.XPATH, using="//*[@id='rewards']/button")
	public WebElement RewardsAngular;
	
	@FindBy(how=How.XPATH, using="//*[@id='rewards']/a")
	public WebElement RewardsLegacy;
	
	@FindBy(how=How.XPATH, using="//*[@id='cardSelector']/div/div[1]/div[1]/img")
	public WebElement CostcoBanner;
	
	@FindBy(how=How.XPATH, using="//*[@id='cardAccount']")
	public WebElement DoubleCashBanner;

	@FindBy(how=How.XPATH, using="//*[@id='servicesMainLI']/div[3]/ul[1]/li[1]/citi-cta/a")
	public WebElement CreditServiceAng;
	
	@FindBy(how=How.XPATH, using="//*[@id='services']/ul/li[1]/a")
	public WebElement CreditServiceLeg;
	@FindBy(how=How.XPATH, using="/html/body/app-root/cbol-core/citi-parent-layout/div/citi-footer/div/citi-footer-sub-navigation/div/div/ul/li[1]/citi-cta/a")
	public WebElement TermsConditionAngular;
	
	
	@FindBy(how=How.XPATH, using="//*[@id='cmlink_InterstitialNoThanks']")
	public WebElement NoThanks;
	
	
}
