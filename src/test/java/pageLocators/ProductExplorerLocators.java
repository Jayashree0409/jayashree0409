package pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;



public class ProductExplorerLocators {
		
	@FindBy(how=How.ID,using="productExplorer")
	public WebElement ProductExplorerIcon;
	
	@FindBy(how=How.ID,using="productExplorerMainLI")
	public WebElement AGProductExplorerIcon;
	
   @FindBy(how=How.CLASS_NAME,using="creditCardsIcon")
   public WebElement CreditCards;

   	@FindBy(how=How.CLASS_NAME,using="lendingIcon")
    public WebElement Lending;
	
    @FindBy(how=How.CLASS_NAME,using="bankingIcon")
	public WebElement Banking;
   	
   	@FindBy(how=How.CLASS_NAME,using="investingIcon")
	public WebElement Investing;
	
   	@FindBy(how=How.CLASS_NAME,using="citigoldIcon")
	public WebElement Citigold;
  
    @FindBy(how=How.ID,using="services")
    public WebElement Services;
    
    @FindBy(how=How.LINK_TEXT,using="Banking Services")
    public WebElement BankingServices;
    
    @FindBy(how=How.LINK_TEXT,using="Servicios Bancarios")
    public WebElement SpanishBankingServices;
        
    @FindBy(how=How.LINK_TEXT,using="Request Upgrade to Citi Priority")
    public WebElement UpgradeCitiPriority;
    
    @FindBy(how=How.LINK_TEXT,using="Solicitar Ascenso a Citi Priority")
    public WebElement SpanishUpgradeCitiPriority;
    
    @FindBy(how=How.LINK_TEXT,using="Statements & Documents")
    public WebElement StatementsandDocuments;
    
    @FindBy(how=How.LINK_TEXT,using="Estados de Cuenta y Documentos")
    public WebElement SpanishStatementsandDocuments;
    
    @FindBy(how=How.ID,using="paymentsandTransfers")
    public WebElement PaymentsandTransfers;

    @FindBy(how=How.LINK_TEXT,using="Any External Account")
    public WebElement AnyExternalAccount;
    
    @FindBy(how=How.LINK_TEXT,using="Cualquier Cuenta Externa")
    public WebElement SpanishAnyExternalAccount;
    
	@FindBy(how=How.XPATH,using="//*[@id=\'footer\']/div[2]/div/img")
	public WebElement Feedbackicon;
	
	@FindBy(how=How.ID,using="rewards")
	public WebElement RewardsandBenefits;
	
	@FindBy(how=How.LINK_TEXT,using="Card Benefits")
	public WebElement CardBenefits;
	
	@FindBy(how=How.LINK_TEXT,using="Beneficios de la Tarjeta")
	public WebElement SpanishCardBenefits;
	
	@FindBy(how=How.LINK_TEXT,using="Español")
	public WebElement LanguageSpanish;
	
	@FindBy(how=How.ID,using="closebutton")
	public WebElement closeicon;
	
	//@FindBy(how=How.ID,using="modal-titleid")
	//public WebElement Popupwindow;
	
	@FindBy(how=How.XPATH,using="//*[@id=\"profileSettings\"]/a/span[1]")
	public WebElement profiles;
	
	@FindBy(how=How.XPATH, using="//*[@id=\"profileSettings\"]/ul/li[3]/a")
	public WebElement ContactInformation;
	
	@FindBy(how=How.XPATH, using="/html/body/app-root/cbol-core/citi-parent-layout/div/div/div/ng-component/citi-simple-layout/div/citi-row/div/citi-column[2]/div/div/div/citi-row[3]/div/emailview/citi-column/div/div/div/citi-cta/button")
	public WebElement EmailEdit;
	
	@FindBy(how=How.XPATH, using="/html/body/app-root/cbol-core/citi-parent-layout/div/div/div/ng-component/citi-simple-layout/div/citi-row/div/citi-column[2]/div/div/div/citi-row[3]/div/emailview/citi-column/div/div/citi-row[3]/div/citi-column[1]/div/div/a")
	public WebElement EmailPreference;

}
